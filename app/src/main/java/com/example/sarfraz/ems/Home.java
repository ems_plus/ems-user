package com.example.sarfraz.ems;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import Module.DirectionFinder;
import Module.DirectionFinderListener;
import Module.Route;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static java.lang.Thread.sleep;

public class Home extends AppCompatActivity
        implements GestureDetector.OnGestureListener,NavigationView.OnNavigationItemSelectedListener,GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback , GoogleApiClient.ConnectionCallbacks,DirectionFinderListener, View.OnClickListener {


    ImageView clearSearch, curLocationIcon;
    ImageView clearSearchTo, nav_header_image;
    SharedPreferences sp;
    private GoogleApiClient googleApiClient;
    Button proceed_arrow;
    ImageView emergency_btn;
    ImageView imageViewRound;
    Animation animation;
    AlertClass ac;
    LinearLayout button_layout;
    Typeface raleway_light, raleway_medium, raleway_italic;
    RelativeLayout map_fragment, vehicle_text_layout, down_layout;


    private TextView nav_header_name, vehicle_text;
    NavigationView navigationView;
    RoundedImageView userImg;

    boolean location_flag;

    int CALL_PHONE_CODE = 98;

    GifImageView loader_image;
    boolean directionFound = false;
    boolean locationFound = false;

    MediaPlayer mp;


    RelativeLayout home_booked_layout, input_layout, call_relative_layout;
    ImageView home_booked_driver_image;
    TextView home_booked_driver_name, home_booked_driver_phone, home_booked_driver_vehicle_number;
    Button home_booked_cancel_button;

    private GoogleMap mMap;
    private LocationManager locationManager;
    private LocationListener locationListener;
    LatLng myPlace, startDirection;
    LatLng toPlace, endDirection;
    String curLocationName;
    MarkerOptions marker;
    float[] final_distance = new float[1];
    long duration;
    float distance;
    TextView nav_header_text;
    Bundle bundle;
    OkHttpClient client;


    boolean doubleBackToExitPressedOnce = false;

    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();




    //google places var declaration
    private static final String LOG_TAG = "MainActivity";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private AutoCompleteTextView mAutocompleteTextView;
    private AutoCompleteTextView mAutocompleteTextViewTo;
    //private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));



    CheckBox future_booking_checkbox, select_service_1, select_service_2, select_service_3 , select_service_4, select_service_5, select_service_6, select_service_7;
    boolean fare_flag;
    LinearLayout tmsView, bmsView, smsView, morgueView, eventView, service_layout, book_now_info, date_time_layout, date_time_layout_event;
    TextView booking_fare, ride_sub_header_text;
    Button bookNow_btn, datebtn, timebtn, datebtn_event, timebtn_event;
    private Calendar calendar;
    private int year, month, day,mHour, mMinute;
    ImageView back_booking, info_booking;
    String am_pm, vehicle_selected, from_address, to_address, from_lat_lng, to_lat_lng, event_type;
    FareCalculator fc;
    EmsDatabase db;
    float total_fare;
    boolean[] services_selected = {false, false, false, false, false, false, false};
    RelativeLayout down_relative_layout, down_image_layout, event_layout;
    GifImageView down_image;
    ImageView menu;
    GestureDetectorCompat gestureDetector;
    EditText description_text;
    Boolean event_flag;
    HorizontalScrollView horizontalScrollView;
    ProgressClass pc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ac = new AlertClass(this);
        pc = new ProgressClass(this);

        if(!MyFunctions.isInternetAvailable(this)){
            ac.showOkDialog("Please Connect to Internet !!","");
        }

        this.gestureDetector = new GestureDetectorCompat(this, this);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer , R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        if(!MyFunctions.isInternetAvailable(this)){
            ac.showOkDialog("Please Connect to Internet !!","");
        } else {
            new FareCalculator(this).execute();
        }


        init();

        registerForNotification();

       new waitForLocation().execute();



        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mMap.clear();
                Log.i("Called :","On Location");
                myPlace = new LatLng(location.getLatitude(), location.getLongitude());
                curLocationName = getLocationNameFromCoordinate(myPlace);

                loader_image.setVisibility(View.GONE);
                locationFound = true;
                if(!location_flag){
                    curLocationIcon.performClick();
                    location_flag = true;
                }


                //mAutocompleteTextView.setText(from);

                marker = new MarkerOptions().position(myPlace).title("You Here");
                mMap.addMarker(marker);
                //mMap.moveCamera(CameraUpdateFactory.newLatLng(myPlace),12.0f);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPlace, 16.0f));
                String cur_loc = getLocationNameFromCoordinate(myPlace);
                //if(cur_loc!=null) mAutocompleteTextView.setText(cur_loc);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

               // switchGPS();

            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{
                        android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.INTERNET
                }, 10);




                return;
            }else{
                donow();
            }
        }else{
            donow();
        }

    }

    private void registerForNotification() {

        SharedPreferences spp  =  getSharedPreferences("NOTIF", Context.MODE_PRIVATE);
        String token = spp.getString("FCM_TOKEN","");


        //Toast.makeText(this, token, Toast.LENGTH_SHORT).show();

        SharedPreferences sp1 = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        String id = sp1.getString(ConstantsLocal.USER_ID,"");

        Log.d("TOken in fun - - - ", token+" "+id);
        if(MyFunctions.isInternetAvailable(this))
            new TokenRegister(id, token).execute();
        else
            MyFunctions.showToast(this, "No internet");

    }

    @Override
    public boolean onDown(MotionEvent e) {
        Log.d("OnDown -- ", "asfasfadf");
        slideToBottom(down_relative_layout);
        slideToBottom(service_layout);
        clearAllSelection();
        input_layout.setVisibility(View.VISIBLE);
        proceed_arrow.setVisibility(View.GONE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) down_relative_layout.getLayoutParams();
        params.height = (int) convertDpToPixel(210, getApplicationContext());
        params.topMargin = (int) -convertDpToPixel(22, getApplicationContext());
        //Log.i("params height -- ", params.height + "");
        down_relative_layout.setVisibility(View.VISIBLE);
        vehicle_text_layout.setVisibility(View.GONE);
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        Log.d("onShowPress -- ", "asfasfadf");

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        Log.d("onSingTapUp -- ", "asfasfadf");
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        Log.d("onScroll -- ", "asfasfadf");
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        Log.d("onLongPress -- ", "asfasfadf");

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        Log.d("onFling -- ", "asfasfadf");
        return false;
    }

    public class TokenRegister extends AsyncTask<String, String, String>{

        String id;
        String token;
        String url = "http://emsplus.co.in/admin/android_notif/insert.php";
        String res;
        public TokenRegister(String id, String token){
            this.id = id;
            this.token = token;
        }
        @Override
        protected String doInBackground(String... params) {

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("insert","yes");
            map.put("type","user");
            map.put("id",id);
            map.put("token",token);

            RequestBody rb = RequestServer.LoginBody(map);
            try {
                res = RequestServer.POST(client, url, rb);

                //Log.d("NOTIF - - - -  ",res);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return res;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Toast.makeText(Home.this, "resp - "+s, Toast.LENGTH_SHORT).show();
        }
    }

    public class waitForLocation extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... params) {
            try {
                sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(!locationFound){
                loader_image.setVisibility(View.GONE);
                //ac.showOkDialog("Unable to fetch current location", "");
                MyFunctions.showToast(Home.this,"Unable to fetch current location");
            }
        }
    }

    private void switchGPS(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Home.this);
        alertDialogBuilder.setMessage("Location access is required. Turn on GPS");
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton("ALLOW",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        //startActivity(intent);
                        startActivityForResult(intent, 23);
                    }
                });

        alertDialogBuilder.setNegativeButton("DENY",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                makeToast("GPS is required!");
                finish();
            }
        });

        alertDialogBuilder.setCancelable(false);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.logout) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Do you really want to logout?");
            alertDialogBuilder.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            logOut();
                        }
                    });

            alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else if (id == R.id.nav_profile) {

            if(bundle == null){
                bundle = new Bundle();
            }
            startActivity(new Intent(Home.this, Profile.class).putExtras(bundle));

        }/* else if (id == R.id.check) {

            if(bundle == null){
                bundle = new Bundle();
            }

            if(myPlace!=null){
                bundle.putString("lat",myPlace.latitude+"");
                bundle.putString("long",myPlace.longitude+"");
                startActivity(new Intent(Home.this, AroundMe.class).putExtras(bundle));
            }



        }*/ else if (id == R.id.nav_settings) {

            //
            startActivity(new Intent(Home.this, SettingActivity.class));

        } else if (id == R.id.nav_journey) {
            //startActivity(new Intent(Home.this, MyJourney.class));
            if(!MyFunctions.isInternetAvailable(this)){
                ac.showOkDialog("Please check your internet connection", "");
                return false;
            }
            pc = new ProgressClass(Home.this);
            if(!pc.isActive())
                pc.startProgress("Fetching transaction data. Please wait...");

            final OkHttpClient client = new OkHttpClient();
            sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, MODE_PRIVATE);
            if(bundle == null){
                bundle = new Bundle();
            } else {
                bundle = getIntent().getExtras();
            }


            //getting transaction history

            final Map<String, String> param = new HashMap<String, String>();
            param.put("USER_ID", sp.getString(ConstantsLocal.USER_ID, ""));

            new AsyncTask<String, String, String>(){

                String response;
                JSONArray jsonArray = null;

                @Override
                protected String doInBackground(String... params) {
                    try {
                        response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                        //Log.i("response --  -- ", response);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return response;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    if(s != null) {

                        if (pc != null && pc.isActive())
                            pc.stopProgress();
                        //Log.i("server res ",s);
                        //ac.showOkDialog(s, "");
                        if (bundle == null) {
                            bundle = new Bundle();
                        }
                        bundle.putString("RESPONSE", s);
                        startActivity(new Intent(Home.this, MyJourney.class).putExtras(bundle));
                    }
                }
            }.execute(ConstantsLocal.GET_JOURNEY_URL);
        } else if (id == R.id.transactions) {
            if(!MyFunctions.isInternetAvailable(this)){
                ac.showOkDialog("Please check your internet connection", "");
                return false;
            }

            pc = new ProgressClass(this);

            if(!pc.isActive())
                pc.startProgress("Fetching transaction data. Please wait...");

            final OkHttpClient client = new OkHttpClient();
            sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, MODE_PRIVATE);
            if(bundle == null){
                bundle = new Bundle();
            } else {
                bundle = getIntent().getExtras();
            }

            //getting transaction history

            final Map<String, String> param = new HashMap<String, String>();
            param.put("USER_ID", sp.getString(ConstantsLocal.USER_ID, ""));

            new AsyncTask<String, String, String>(){

                String response;
                JSONArray jsonArray = null;

                @Override
                protected String doInBackground(String... params) {
                    try {
                        response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                        //Log.i("response --  -- ", response);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return response;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);

                    if(s != null) {

                        if (pc != null && pc.isActive())
                            pc.stopProgress();
                        Log.i("server res ", s);
                        //ac.showOkDialog(s, "");
                        if (bundle == null) {
                            bundle = new Bundle();
                        }
                        bundle.putString("RESPONSE", s);
                        startActivity(new Intent(Home.this, TransactionHistory.class).putExtras(bundle));
                    }

                }
            }.execute(ConstantsLocal.GET_TRANSACTION_HISTORY_URL);
        } /*else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void init() {
        String pid = "";
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
        clearSearch = (ImageView) findViewById(R.id.clear_search);
        curLocationIcon = (ImageView) findViewById(R.id.cur_location_icon);
        clearSearchTo = (ImageView) findViewById(R.id.clear_searchTo);
        proceed_arrow = (Button)findViewById(R.id.proceed_arrow);
        proceed_arrow.setClickable(false);

        MediaPlayer mp = MediaPlayer.create(this, R.raw.doraemon);

        vehicle_text = (TextView) findViewById(R.id.vehicle_text);
        vehicle_text_layout = (RelativeLayout) findViewById(R.id.vehicle_text_layout);
        vehicle_text_layout.setVisibility(View.GONE);

        event_layout = (RelativeLayout) findViewById(R.id.event_layout);
        description_text = (EditText) findViewById(R.id.description_text);
        datebtn_event = (Button) findViewById(R.id.datebtn_event);
        timebtn_event = (Button) findViewById(R.id.timebtn_event);
        event_layout.setVisibility(View.GONE);

        datebtn_event.setOnClickListener(this);
        timebtn_event.setOnClickListener(this);

        raleway_light = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        raleway_medium = Typeface.createFromAsset(getAssets(), "Raleway-Medium.ttf");
        raleway_italic = Typeface.createFromAsset(getAssets(), "Raleway-MediumItalic.ttf");


        vehicle_text.setTypeface(raleway_italic);

        //emergency_btn = (ImageView) findViewById(R.id.emergency_button);
        button_layout = (LinearLayout) findViewById(R.id.button_layout);
        input_layout = (RelativeLayout) findViewById(R.id.input_layout);
        call_relative_layout = (RelativeLayout) findViewById(R.id.call_relative_layout);
        button_layout.setBackgroundColor(Color.TRANSPARENT);
        /*imageViewRound=(ImageView)findViewById(R.id.profile);
        Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.divya);
        imageViewRound.setImageBitmap(icon);*/

        client = new OkHttpClient();

        View header_view = navigationView.inflateHeaderView(R.layout.nav_header_home);
        nav_header_name = (TextView) header_view.findViewById(R.id.nav_header_name);
        userImg = (RoundedImageView) header_view.findViewById(R.id.nav_header_image);

        nav_header_name.setTypeface(raleway_medium);

        sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        nav_header_name.setText(sp.getString(ConstantsLocal.USER_NAME, ""));

        /*if(sp.getString(ConstantsLocal.USER_GENDER,"").equals("MALE")){
            userImg.setImageResource(R.drawable.man);
        }else{
            userImg.setImageResource(R.drawable.woman);
        }*/

        String pic = MyFunctions.getUserPic(this);
        Log.d("user pic - - -",pic);
        if(pic.isEmpty() || pic.equalsIgnoreCase("path") || pic.equalsIgnoreCase("null")){
            if(sp.getString(ConstantsLocal.USER_GENDER,"").equals("MALE")){
                userImg.setImageResource(R.drawable.man);
            }else{
                userImg.setImageResource(R.drawable.woman);
            }
        }else{
             userImg.setImageBitmap(MyFunctions.Decodebitmap(MyFunctions.getUserPic(this)));
        }




        map_fragment = (RelativeLayout) findViewById(R.id.map_fragment);

        home_booked_layout = (RelativeLayout) findViewById(R.id.home_booked_layout);
        home_booked_driver_image = (ImageView) findViewById(R.id.home_booked_driver_image);
        home_booked_driver_name = (TextView) findViewById(R.id.home_booked_driver_name);
        home_booked_driver_phone = (TextView) findViewById(R.id.home_booked_driver_phone);
        home_booked_driver_vehicle_number = (TextView) findViewById(R.id.home_booked_driver_vehicle_number);
        home_booked_cancel_button = (Button) findViewById(R.id.home_booked_cancel_button);

        raleway_light = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        raleway_medium = Typeface.createFromAsset(getAssets(), "Raleway-SemiBold.ttf");

        home_booked_driver_name.setTypeface(raleway_medium);
        home_booked_driver_phone.setTypeface(raleway_light);
        home_booked_driver_vehicle_number.setTypeface(raleway_light);

        loader_image = (GifImageView) findViewById(R.id.loader);
        loader_image.setGifImageResource(R.drawable.find_driver);


        call_relative_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                if(isPermissionGranted())
                    callAction();


            }
        });

        sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, MODE_PRIVATE);

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(Home.this).enableAutoManage(Home.this, Home.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();



        clearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutocompleteTextView.setText("");
            }
        });

        curLocationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutocompleteTextView.setText(curLocationName);
            }
        });

        clearSearchTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutocompleteTextViewTo.setText("");
            }
        });

        //google places
        /*mGoogleApiClient = new GoogleApiClient.Builder(Home.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();*/

        //from input
        mAutocompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        mAutocompleteTextView.setThreshold(3);
        mAutocompleteTextView.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1, new LatLngBounds(new LatLng(23.63936, 68.14712), new LatLng(28.20453, 97.34466)), null);
        mAutocompleteTextView.setAdapter(mPlaceArrayAdapter);


        //to input
        mAutocompleteTextViewTo = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewTo);
        mAutocompleteTextViewTo.setThreshold(3);
        mAutocompleteTextViewTo.setOnItemClickListener(mAutocompleteClickListenerTo);
        mAutocompleteTextViewTo.setAdapter(mPlaceArrayAdapter);


        //for current location
        mAutocompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                down_relative_layout.setVisibility(View.GONE);
                service_layout.setVisibility(View.GONE);
                proceed_arrow.setVisibility(View.GONE);
                clearAllSelection();
                if(mAutocompleteTextView.getText().toString().trim().isEmpty()){
                    curLocationIcon.setVisibility(View.VISIBLE);
                    clearSearch.setVisibility(View.GONE);
                }else{
                    curLocationIcon.setVisibility(View.GONE);
                    clearSearch.setVisibility(View.VISIBLE);
                }
            }
        });

        mAutocompleteTextViewTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                down_relative_layout.setVisibility(View.GONE);
                service_layout.setVisibility(View.GONE);
                proceed_arrow.setVisibility(View.GONE);
                clearAllSelection();
                if(mAutocompleteTextViewTo.getText().toString().trim().isEmpty()){
                    clearSearchTo.setVisibility(View.GONE);
                }else {
                    clearSearchTo.setVisibility(View.VISIBLE);
                }
            }
        });

        //emergency_btn.startAnimation(animation);

        /*emergency_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this, EmergencyBooking.class));
            }
        });*/

        //coming from paymentStatus - successfull order

        down_relative_layout = (RelativeLayout) findViewById(R.id.down_relative_layout);
        down_relative_layout.setVisibility(View.GONE);
        down_image_layout = (RelativeLayout) findViewById(R.id.down_image_layout);
        down_image_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);

        horizontalScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        ride_sub_header_text = (TextView) findViewById(R.id.ride_sub_header_text);

        ride_sub_header_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        bundle = getIntent().getExtras();
        if(bundle != null && bundle.getBoolean("ORDER_SUCCESSFULL")){

            mp = MediaPlayer.create(this, R.raw.doraemon);
            mp.stop();
            mp.start();

            loader_image.setVisibility(View.GONE);
            locationFound = true;
            pid = bundle.getString("PID");
            input_layout.setVisibility(View.GONE);
            button_layout.setVisibility(View.GONE);
            home_booked_layout.setVisibility(View.VISIBLE);
            home_booked_driver_name.setText(bundle.getString("driver_name"));
            home_booked_driver_phone.setText(bundle.getString("driver_phone"));
            home_booked_driver_vehicle_number.setText(bundle.getString("driver_vehicle_number"));

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) map_fragment.getLayoutParams();
            params.addRule(RelativeLayout.ABOVE, R.id.home_booked_layout);


            down_relative_layout.setVisibility(View.GONE);
            down_image_layout.setVisibility(View.GONE);

            //ac.showOkDialog(bundle.getString("from_address") + " -- " + bundle.getString("to_address"), "");

            sendPathRequest2(bundle.getString("from_address"), bundle.getString("to_address"));
            checkCompletion();

        }

        if(bundle != null && bundle.getBoolean("JOURNEY_COMPLETED")){
            ac.showOkDialog("Your journey has been completed successfully.", "Thank you for using EMS+");
        }


        /*proceed_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!directionFound){
                    makeToast("Please enter the correct address..");
                    return;
                }

                /*Location.distanceBetween(startDirection.latitude, startDirection.longitude,
                        endDirection.latitude, endDirection.longitude, final_distance);
                if(mAutocompleteTextView.getText().toString().trim().isEmpty()){
                    makeToast("Please enter source!");
                    return;
                }

                if(mAutocompleteTextViewTo.getText().toString().trim().isEmpty()){
                    //ac.showOkDialog("Please enter destination!","");
                    makeToast("Please enter destination!");
                   return;
                }
                from_address = mAutocompleteTextView.getText().toString();
                to_address = mAutocompleteTextViewTo.getText().toString();
                //makeToast("from_address - " + from_address + ", to_address - " + to_address);
                Intent intent = new Intent(Home.this, Booking1.class);
                bundle = new Bundle();
                bundle.putDouble("DISTANCE", distance/1000.0);
                bundle.putString("FROM_ADDRESS", from_address);
                bundle.putString("TO_ADDRESS", to_address);
                bundle.putString("FROM_LAT_LNG", startDirection.toString());
                bundle.putString("TO_LAT_LNG", endDirection.toString());
                bundle.putLong("DURATION", duration);
                //makeToast("startLatLng - " + startDirection.toString() + ", endLatLng - " + endDirection.toString() );
                Log.i("location --", startDirection+"  "+endDirection+"  "+final_distance[0]+"  "+distance);
                startActivity(intent.putExtras(bundle));
            }
        });*/

        final String finalPid = pid;
        home_booked_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Map<String, String> param = new HashMap<String, String>();
                param.put("pid", finalPid);

                pc = new ProgressClass(Home.this);
                if(!pc.isActive())
                    pc.startProgress("Cancelling order. Please wait...");

                new AsyncTask<String, String, String>(){

                    String response;
                    JSONArray jsonArray = null;

                    @Override
                    protected String doInBackground(String... params) {
                        try {
                            response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                            //Log.i("response --  -- ", response);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return response;
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);

                        if(s != null) {

                            Log.i("server res ", s);
                            if (pc != null && pc.isActive())
                                pc.stopProgress();
                            try {
                                JSONObject parentObject = new JSONObject(s);
                                JSONObject obj = parentObject.getJSONObject("data");

                                String status = obj.get("status").toString();

                                if (status.equals("success")) {

                                    bundle = new Bundle();
                                    input_layout.setVisibility(View.VISIBLE);
                                    button_layout.setVisibility(View.VISIBLE);
                                    home_booked_layout.setVisibility(View.GONE);
                                    finish();
                                    Intent intent = new Intent(Home.this, Home.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);

                                } else {


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.execute(ConstantsLocal.CANCEL_ORDER);
            }
        });


        /*changeLayoutEnabled(mAutocompleteTextView);
        changeLayoutDisabled(mAutocompleteTextViewTo);*/

        menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(this);


        fare_flag = true;
        tmsView = (LinearLayout) findViewById(R.id.tms_image_view);
        bmsView = (LinearLayout) findViewById(R.id.bms_image_view);
        smsView = (LinearLayout) findViewById(R.id.sms_image_view);
        morgueView = (LinearLayout) findViewById(R.id.morgue_image_view);
        eventView = (LinearLayout) findViewById(R.id.event_image_view);
        //bookNow_btn = (Button) findViewById(R.id.book_now_btn);
        //bookNow_btn.setTransformationMethod(null);
        future_booking_checkbox = (CheckBox) findViewById(R.id.future_booking_checkbox);
        date_time_layout = (LinearLayout) findViewById(R.id.date_time_layout);
        //back_booking = (ImageView) findViewById(R.id.back_booking);
        //info_booking = (ImageView) findViewById(R.id.info_booking);
        booking_fare = (TextView) findViewById(R.id.booking_fare);


        select_service_1 = (CheckBox) findViewById(R.id.select_service_1);
        select_service_2 = (CheckBox) findViewById(R.id.select_service_2);
        select_service_3 = (CheckBox) findViewById(R.id.select_service_3);
        select_service_4 = (CheckBox) findViewById(R.id.select_service_4);
        select_service_5 = (CheckBox) findViewById(R.id.select_service_5);
        select_service_6 = (CheckBox) findViewById(R.id.select_service_6);
        select_service_7 = (CheckBox) findViewById(R.id.select_service_7);

        //time picker and date picker

        date_time_layout.setVisibility(View.GONE);
        datebtn = (Button) findViewById(R.id.datebtn);
        timebtn = (Button) findViewById(R.id.timebtn);

        datebtn.setOnClickListener(this);
        timebtn.setOnClickListener(this);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinute = calendar.get(Calendar.MINUTE);

        //info_booking.setVisibility(View.GONE);


        service_layout = (LinearLayout) findViewById(R.id.services_layout);
        service_layout.setVisibility(View.GONE);
        //bookNow_btn.setVisibility(View.GONE);
        //book_now_info = (LinearLayout) findViewById(R.id.book_now_info);

        tmsView.setOnClickListener(this);
        bmsView.setOnClickListener(this);
        smsView.setOnClickListener(this);
        morgueView.setOnClickListener(this);
        eventView.setOnClickListener(this);
        //bookNow_btn.setOnClickListener(this);

        future_booking_checkbox.setOnClickListener(this);

        proceed_arrow.setVisibility(View.GONE);
        proceed_arrow.setOnClickListener(this);

        down_image = (GifImageView) findViewById(R.id.down_image);
        down_image.setGifImageResource(R.drawable.down_arrow);
        //down_image_layout.setVisibility(View.GONE);

        down_image.setOnClickListener(this);

        event_type = "";

        down_layout = (RelativeLayout) findViewById(R.id.down_layout);

    }

    public void checkCompletion(){
        new checkJourneyComplete().execute();
    }

    public class checkJourneyComplete extends AsyncTask<String, String, String>{

        String id = MyFunctions.getUserId(Home.this);
        String res="";
        String url= ConstantsLocal.HOME_URL+"check_complete_order.php";

        @Override
        protected String doInBackground(String... params) {

            Map<String, String> map = new HashMap<String, String>();
            map.put("user_id",id);

            try {
                res = RequestServer.POST(client, url, RequestServer.LoginBody(map));
            } catch (IOException e) {
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String s) {

            if(s != null) {

                try {
                    JSONObject obj = new JSONObject(s);

                    if (obj.getString("status").equalsIgnoreCase("success")) {
                        if (obj.getString("journey_status").trim().equalsIgnoreCase("completed")) {
                            Bundle bundle = new Bundle();
                            bundle.putString("upid", obj.getString("upid").trim());
                            NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            notifManager.cancelAll();
                            startActivity(new Intent(Home.this, Completed.class).putExtras(bundle));
                        } else {
                            Log.d("Sending", "again");
                            int timeout = 5000; // make the activity visible for 4 seconds

                            Timer timer = new Timer();
                            timer.schedule(new TimerTask() {

                                @Override
                                public void run() {
                                    new checkJourneyComplete().execute();
                                }
                            }, timeout);

                        }
                    } else {
                        Log.d("Status ", "Not success");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            super.onPostExecute(s);
        }
    }



    private void getBasic(float distance, long duration, String vehicle_type){
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) down_relative_layout.getLayoutParams();
        params.height = MATCH_PARENT;
        down_relative_layout.setBackgroundColor(Color.WHITE);
        //params.topMargin = (int) -convertPixelsToDp(900, getApplicationContext());
        //Log.i("params -- ", params.topMargin + "");
        down_relative_layout.setLayoutParams(params);
        Log.d("database - ", distance + "  " + duration + "  " + vehicle_type);
        if(MyFunctions.isInternetAvailable(this)){
            fc = new FareCalculator(distance+"", "", duration, vehicle_type, this);
            db = new EmsDatabase(this);

            total_fare = fc.getFare().getTotal_fare();

            fare_flag = true;
        } else {
            MyFunctions.showToast(this, "No internet");
        }


    }

    public class UploadEvent extends AsyncTask<String, String, String>{


        Map<String, String> param = new HashMap<String, String>();
        String res;
        public UploadEvent(String from_address, String to_address, String from_lat_lng, String to_lat_lng, String datetime, String description, String event_type){
            param.put("user_id",sp.getString(ConstantsLocal.USER_ID, ""));
            param.put("from_address", from_address);
            param.put("to_address", to_address);
            param.put("from_lat_lng", from_lat_lng);
            param.put("to_lat_lng", to_lat_lng);
            param.put("datetime", datetime);
            param.put("description", description);
            param.put("type", event_type);

            pc = new ProgressClass(Home.this);
            if(pc != null && !pc.isActive())
                pc.startProgress("Please wait...");

            Log.d("param", param.toString());
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                res = RequestServer.POST(client, ConstantsLocal.EVENT_UPLOAD_URL, RequestServer.LoginBody(param));
            } catch (IOException e) {
                e.printStackTrace();
            }


            return res;
        }

        @Override
        protected void onPostExecute(String s) {


            if(s != null) {

                if (pc != null && pc.isActive())
                    pc.stopProgress();

                try {
                    if (pc != null && pc.isActive())
                        pc.stopProgress();
                    Log.d("res--", s);
                    JSONObject obj = new JSONObject(s);

                    if (obj.getString("status").equalsIgnoreCase("success")) {
                        makeToast("success");
                        Intent intent = new Intent(Home.this, EventSuccess.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        makeToast("There was some error!");
                        down_image.performClick();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {

        /*if(v.getId() == R.id.book_now_btn){
            clickedBookNow();
        }*/

        if(v.getId() == R.id.tms_image_view || v.getId() == R.id.bms_image_view || v.getId() == R.id.sms_image_view || v.getId() == R.id.morgue_image_view || v.getId() == R.id.event_image_view){
            service_layout.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) down_relative_layout.getLayoutParams();
            params.height = (int) convertDpToPixel(210, getApplicationContext());
            params.topMargin = (int) convertDpToPixel(130, getApplicationContext());
            //book_now_info.setVisibility(View.GONE);
            //bookNow_btn.setVisibility(View.VISIBLE);
            //info_booking.setVisibility(View.VISIBLE);
            clearAllSelection();
        }

        if(v.getId() == R.id.proceed_arrow){

            from_address = mAutocompleteTextView.getText().toString();
            to_address = mAutocompleteTextViewTo.getText().toString();

            from_lat_lng = startDirection.toString();
            to_lat_lng = endDirection.toString();

            if(event_flag){

                String description = description_text.getText().toString();
                String date = datebtn_event.getText().toString();
                String time = timebtn_event.getText().toString();
                String datetime = date + " " + time;

                if(!description.equals("") && !date.equals("date") && !time.equals("time")){

                    new UploadEvent(from_address, to_address, from_lat_lng, to_lat_lng, datetime, description, event_type).execute();

                } else {
                    ac.showOkDialog("Description, time & date are required.", "");
                }


            } else {

                String services = "";
                String[] total_services = {"VENTILATOR", "STRETCHER", "PARAMEDIC", "OXYGEN", "WHEELCHAIR", "HELPER", "DOCTOR"};

                if(services_selected != null){
                    for(int i=0;i<7;i++){
                        if(services_selected[i]){
                            services += total_services[i] + ", ";
                        }
                    }

                    if(services.length() > 0){
                        services = services.substring(0, services.length() - 2);
                        services = services.trim();
                    }

                }

                //ac.showConfirmDialog("total fare = " + total_fare + "\n\nservices = " + services + "\n\nfrom address = " + from_address + "\n\nto address = " + to_address + "\n\nfrom lat lng = " + from_lat_lng + "\n\nto lat lng = " + to_lat_lng + "\n\ndistance = " + distance + "\n\nvehicle selected = " + vehicle_selected);
                if(bundle == null){
                    bundle = new Bundle();
                }

                Bundle bundle = new Bundle();
                bundle.putDouble("TOTAL_FARE", total_fare);
                bundle.putString("SERVICES", services);
                bundle.putString("VEHICLE_TYPE", vehicle_selected);
                bundle.putDouble("DISTANCE", distance);
                bundle.putString("FROM_ADDRESS", from_address);
                bundle.putString("TO_ADDRESS", to_address);
                bundle.putString("FROM_LAT_LNG", from_lat_lng);
                bundle.putString("TO_LAT_LNG", to_lat_lng);

                if(vehicle_selected.equalsIgnoreCase("ALS")){
                    HashMap<String, String> map = new HashMap<>();
                    map.put("name", sp.getString(ConstantsLocal.USER_NAME, ""));
                    map.put("email", sp.getString(ConstantsLocal.USER_EMAIL, ""));
                    map.put("phone", sp.getString(ConstantsLocal.USER_PHONE, ""));

                    new sendAlsDetails(map).execute();

                }

                startActivity(new Intent(Home.this, DriverDetails.class).putExtras(bundle));

            }

        }

        if(v.getId() == R.id.down_image){
            slideToBottom(down_relative_layout);
            slideToBottom(service_layout);
            clearAllSelection();
            input_layout.setVisibility(View.VISIBLE);
            proceed_arrow.setVisibility(View.GONE);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) down_relative_layout.getLayoutParams();
            params.height = (int) convertDpToPixel(210, getApplicationContext());
            params.topMargin = (int) -convertDpToPixel(22, getApplicationContext());
            //Log.i("params height -- ", params.height + "");
            down_relative_layout.setVisibility(View.VISIBLE);
            vehicle_text_layout.setVisibility(View.GONE);
        }

        if(v.getId() == R.id.menu){
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.openDrawer(GravityCompat.START);
        }

        if(fare_flag) {
            switch (v.getId()) {
                case R.id.tms_image_view:
                    proceed_arrow.setVisibility(View.VISIBLE);
                    tmsView.setBackgroundResource(R.drawable.layout_border);
                    vehicle_selected = "TLS";
                    vehicle_text.setText(R.string.tls_text);
                    vehicle_text_layout.setVisibility(View.VISIBLE);
                    //hiding services

                    enableCheckbox();
                    select_service_1.setVisibility(View.GONE);
                    select_service_4.setVisibility(View.GONE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //select_service_1.setChecked(true);
                            select_service_2.setChecked(true);
                            //select_service_3.setChecked(true);
                            //select_service_4.setChecked(true);
                            //select_service_5.setChecked(true);
                            //select_service_6.setChecked(true);
                            //select_service_7.setChecked(true);
                        }
                    }, 300);
                    select_service_2.setEnabled(false);
                    services_selected[1] = true;
                    services_selected[3] = false;
                    services_selected[0] = false;
                    services_selected[2] = false;
                    services_selected[4] = false;
                    services_selected[5] = false;
                    services_selected[6] = false;

                    service_layout.setVisibility(View.VISIBLE);
                    event_layout.setVisibility(View.GONE);
                    event_flag = false;
                    getBasic(distance, duration, "TLS");
                    calculateFare();
                    break;
                case R.id.bms_image_view:
                    proceed_arrow.setVisibility(View.VISIBLE);
                    bmsView.setBackgroundResource(R.drawable.layout_border);
                    vehicle_selected = "BLS";
                    vehicle_text.setText(R.string.bls_text);

                    //hiding services

                    enableCheckbox();
                    select_service_1.setVisibility(View.GONE);
                    select_service_4.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //select_service_1.setChecked(true);
                            select_service_2.setChecked(true);
                            //select_service_3.setChecked(true);
                            select_service_4.setChecked(true);
                            //select_service_5.setChecked(true);
                            //select_service_6.setChecked(true);
                            //select_service_7.setChecked(true);
                        }
                    }, 300);
                    select_service_2.setEnabled(false);
                    select_service_4.setEnabled(false);
                    services_selected[1] = true;
                    services_selected[3] = true;
                    services_selected[0] = false;
                    services_selected[2] = false;
                    services_selected[4] = false;
                    services_selected[5] = false;
                    services_selected[6] = false;

                    vehicle_text_layout.setVisibility(View.VISIBLE);
                    service_layout.setVisibility(View.VISIBLE);
                    event_layout.setVisibility(View.GONE);
                    event_flag = false;
                    getBasic(distance, duration, "BLS");

                    if(distance <= 40) {
                        total_fare += Float.parseFloat(db.getServiceFare("OXYGEN_40"));
                    } else {
                        total_fare += Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));
                    }


                    calculateFare();
                    break;
                case R.id.sms_image_view:
                    proceed_arrow.setVisibility(View.VISIBLE);
                    smsView.setBackgroundResource(R.drawable.layout_border);
                    vehicle_selected = "ALS";
                    vehicle_text.setText(R.string.als_text);
                    vehicle_text_layout.setVisibility(View.VISIBLE);

                    //hiding services

                    enableCheckbox();
                    select_service_1.setVisibility(View.VISIBLE);
                    select_service_4.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            select_service_1.setChecked(true);
                            select_service_2.setChecked(true);
                            select_service_3.setChecked(true);
                            select_service_4.setChecked(true);
                            select_service_5.setChecked(true);
                            select_service_6.setChecked(true);
                            select_service_7.setChecked(true);
                        }
                    }, 300);
                    select_service_1.setEnabled(false);
                    select_service_2.setEnabled(false);
                    select_service_3.setEnabled(false);
                    select_service_4.setEnabled(false);
                    select_service_5.setEnabled(false);
                    select_service_6.setEnabled(false);
                    select_service_7.setEnabled(false);
                    services_selected[0] = true;
                    services_selected[1] = true;
                    services_selected[2] = true;
                    services_selected[3] = true;
                    services_selected[4] = true;
                    services_selected[5] = true;
                    services_selected[6] = true;

                    service_layout.setVisibility(View.VISIBLE);
                    event_layout.setVisibility(View.GONE);
                    event_flag = false;
                    getBasic(distance, duration, "ALS");
                    calculateFare();
                    break;
                case R.id.morgue_image_view:
                    //proceed_arrow.setVisibility(View.VISIBLE);
                    morgueView.setBackgroundResource(R.drawable.layout_border);
                    vehicle_selected = "MORGUE";
                    RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) down_relative_layout.getLayoutParams();
                    param.height = MATCH_PARENT;
                    down_relative_layout.setBackgroundColor(Color.WHITE);
                    //params.topMargin = (int) -convertPixelsToDp(900, getApplicationContext());
                    //Log.i("params -- ", params.topMargin + "");
                    down_relative_layout.setLayoutParams(param);
                    String text = "Vehicle to provide facilites for mortuary." + "\n*Your request will be transferred to admin. After this you'll will be contacted for the same.";
                    vehicle_text.setText(text);
                    vehicle_text_layout.setVisibility(View.VISIBLE);
                    service_layout.setVisibility(View.GONE);
                    event_layout.setVisibility(View.VISIBLE);
                    button_layout.setVisibility(View.VISIBLE);
                    proceed_arrow.setVisibility(View.VISIBLE);
                    event_flag = true;
                    event_type = "MORGUE";
                    //calculateFare();
                    break;
                case R.id.event_image_view:
                    //proceed_arrow.setVisibility(View.VISIBLE);
                    eventView.setBackgroundResource(R.drawable.layout_border);
                    vehicle_selected = "EVENT";
                    param = (RelativeLayout.LayoutParams) down_relative_layout.getLayoutParams();
                    param.height = MATCH_PARENT;
                    down_relative_layout.setBackgroundColor(Color.WHITE);
                    //params.topMargin = (int) -convertPixelsToDp(900, getApplicationContext());
                    //Log.i("params -- ", params.topMargin + "");
                    down_relative_layout.setLayoutParams(param);
                    text = "Vehicle for any kind of medical event." + "\n*Your request will be transferred to admin. After this you'll will be contacted for the same.";
                    vehicle_text.setText(text);
                    vehicle_text_layout.setVisibility(View.VISIBLE);
                    service_layout.setVisibility(View.GONE);
                    event_layout.setVisibility(View.VISIBLE);
                    button_layout.setVisibility(View.VISIBLE);
                    proceed_arrow.setVisibility(View.VISIBLE);
                    event_flag = true;
                    event_type = "EVENT";
                    //calculateFare();
                    break;
                case R.id.datebtn:
                    showDialog(999);
                    break;
                case R.id.timebtn:
                    showDialog(998);
                    break;
                case R.id.datebtn_event:
                    showDialog(997);
                    break;
                case R.id.timebtn_event:
                    showDialog(996);
                    break;
                case R.id.future_booking_checkbox:
                    if (future_booking_checkbox.isChecked()) {
                        date_time_layout.setVisibility(View.VISIBLE);
                    } else {
                        date_time_layout.setVisibility(View.GONE);
                    }
                default:
                    break;
            }

        } else {
            ac.showOkDialog("There was some error.", "Please try again later!");
        }
    }

    private void enableCheckbox(){

        select_service_1.setEnabled(true);
        select_service_2.setEnabled(true);
        select_service_3.setEnabled(true);
        select_service_4.setEnabled(true);
        select_service_5.setEnabled(true);
        select_service_6.setEnabled(true);
        select_service_7.setEnabled(true);
    }

    private void clickedBookNow() {

        /*int radioButtonID = payment_menu.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton)payment_menu.findViewById(radioButtonID);
        int selectedIndex = payment_menu.indexOfChild(radioButton);

        String payment_option = "";
        if(selectedIndex==0)  payment_option = "ONLINE";
        else if(selectedIndex==1) payment_option = "CASH";*/

        String services = "";
        String[] total_services = {"VENTILATOR", "STRETCHER", "PARAMEDIC", "OXYGEN", "WHEELCHAIR", "HELPER", "DOCTOR"};

        if(services_selected != null){
            for(int i=0;i<7;i++){
                if(services_selected[i]){
                    services += total_services[i] + ", ";
                }
            }

            if(services.length() > 0){
                services = services.substring(0, services.length() - 2);
                services = services.trim();
            }

        }


        //Log.i("FINAL STATUS - ", "COST = " + total_fare + ", SERVICES = " + services + ", PAYMENT = " + payment_option);

        /*Bundle bundle = new Bundle();
        bundle.putDouble("TOTAL_FARE", total_fare);
        bundle.putString("SERVICES", services);
        //bundle.putString("PAYMENT", payment_option);
        bundle.putString("VEHICLE_TYPE", vehicle_selected);
        bundle.putDouble("DISTANCE", (final_distance));
        bundle.putString("FROM_ADDRESS", from_address);
        bundle.putString("TO_ADDRESS", to_address);
        bundle.putString("FROM_LAT_LNG", from_lat_lng);
        bundle.putString("TO_LAT_LNG", to_lat_lng);

        //startActivity(new Intent(Booking1.this, DriverDetails.class).putExtras(bundle));*/

    }


    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        if(id == 998){
            return new TimePickerDialog(this,myTimeListener, mHour, mMinute, false);
        }
        if (id == 997) {
            return new DatePickerDialog(this, myDateListener1, year, month, day);
        }
        if(id == 996){
            return new TimePickerDialog(this,myTimeListener1, mHour, mMinute, false);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            String month = "";
            switch (arg2){
                case 1 : month = "Jan"; break;
                case 2 : month = "Feb"; break;
                case 3 : month = "Mar"; break;
                case 4 : month = "Apr"; break;
                case 5 : month = "May"; break;
                case 6 : month = "Jun"; break;
                case 7 : month = "July"; break;
                case 8 : month = "Aug"; break;
                case 9 : month = "Sept"; break;
                case 10 : month = "Oct"; break;
                case 11 : month = "Nov"; break;
                case 12 : month = "Dec"; break;
            }
            String date = month+" "+arg3+", "+arg1;
            datebtn.setText(date);
        }
    };

    private TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener(){
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if(hourOfDay < 12){
                am_pm = "AM";
            } else {
                am_pm = "PM";
                hourOfDay = (hourOfDay == 12) ? hourOfDay : (hourOfDay - 12);
            }
            String time = hourOfDay+":"+minute+" "+am_pm;
            timebtn.setText(time);
        }
    };

    private DatePickerDialog.OnDateSetListener myDateListener1 = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            String month = "";
            switch (arg2){
                case 1 : month = "Jan"; break;
                case 2 : month = "Feb"; break;
                case 3 : month = "Mar"; break;
                case 4 : month = "Apr"; break;
                case 5 : month = "May"; break;
                case 6 : month = "Jun"; break;
                case 7 : month = "July"; break;
                case 8 : month = "Aug"; break;
                case 9 : month = "Sept"; break;
                case 10 : month = "Oct"; break;
                case 11 : month = "Nov"; break;
                case 12 : month = "Dec"; break;
            }
            String date = month+" "+arg3+", "+arg1;
            datebtn_event.setText(date);
        }
    };

    private TimePickerDialog.OnTimeSetListener myTimeListener1 = new TimePickerDialog.OnTimeSetListener(){
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if(hourOfDay < 12){
                am_pm = "AM";
            } else {
                am_pm = "PM";
                hourOfDay = (hourOfDay == 12) ? hourOfDay : (hourOfDay - 12);
            }
            String time = hourOfDay+":"+minute+" "+am_pm;
            timebtn_event.setText(time);
        }
    };

    private void clearAllSelection() {
        tmsView.setBackground(null);
        bmsView.setBackground(null);
        smsView.setBackground(null);
        morgueView.setBackground(null);
        eventView.setBackground(null);
    }

    private void calculateFare(){

        total_fare = Float.parseFloat(String.format("%.2f", total_fare));
        final float[] total_fare_up = {Float.parseFloat(String.format("%.2f", (total_fare - 17.39)))};
        final float[] total_fare_down = {Float.parseFloat(String.format("%.2f", (total_fare + 27.45)))};
        booking_fare.setText("₹" + total_fare_up[0] + " ~ " + "₹" + total_fare_down[0]);

        unCheckAll();

        select_service_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_1.isChecked()){

                    Log.i("total fare - ",total_fare+"");
                    services_selected[0] = true;
                } else {

                    Log.i("total fare - ", total_fare+"");
                    services_selected[0] = false;
                }
            }
        });

        select_service_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_2.isChecked()){

                    Log.i("total fare - ",total_fare+"");
                    services_selected[1] = true;
                } else {

                    Log.i("total fare - ", total_fare+"");
                    services_selected[1] = false;
                }
            }
        });

        select_service_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_3.isChecked()){
                    total_fare += Float.parseFloat(db.getServiceFare("PARAMEDIC"));
                    total_fare_up[0] += Float.parseFloat(db.getServiceFare("PARAMEDIC"));
                    total_fare_down[0] += Float.parseFloat(db.getServiceFare("PARAMEDIC"));
                    Log.i("total fare - ",total_fare+"");
                    services_selected[2] = true;
                    booking_fare.setText("₹" + String.format("%.2f", total_fare_up[0]) + " ~ " + "₹" + String.format("%.2f", total_fare_down[0]));
                } else {
                    total_fare -= Float.parseFloat(db.getServiceFare("PARAMEDIC"));
                    total_fare_up[0] -= Float.parseFloat(db.getServiceFare("PARAMEDIC"));
                    total_fare_down[0] -= Float.parseFloat(db.getServiceFare("PARAMEDIC"));
                    Log.i("total fare - ", total_fare+"");
                    services_selected[2] = false;
                    booking_fare.setText("₹" + String.format("%.2f", total_fare_up[0]) + " ~ " + "₹" + String.format("%.2f", total_fare_down[0]));
                }
            }
        });

        select_service_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_4.isChecked()){
                    if(distance <= 40) {
                        total_fare += Float.parseFloat(db.getServiceFare("OXYGEN_40"));
                        total_fare_up[0] += Float.parseFloat(db.getServiceFare("OXYGEN_40"));
                        total_fare_down[0] += Float.parseFloat(db.getServiceFare("OXYGEN_40"));
                    } else {
                        total_fare += Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));
                        total_fare_up[0] += Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));
                        total_fare_down[0] += Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));
                    }
                    Log.i("total fare - ",total_fare+"");
                    services_selected[3] = true;
                    booking_fare.setText("₹" + String.format("%.2f", total_fare_up[0]) + " ~ " + "₹" + String.format("%.2f", total_fare_down[0]));
                } else {
                    if(distance <= 40) {
                        total_fare -= Float.parseFloat(db.getServiceFare("OXYGEN_40"));
                        total_fare_up[0] -= Float.parseFloat(db.getServiceFare("OXYGEN_40"));
                        total_fare_down[0] -= Float.parseFloat(db.getServiceFare("OXYGEN_40"));
                    } else {
                        total_fare -= Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));
                        total_fare_up[0] -= Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));
                        total_fare_down[0] -= Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));
                    }
                    Log.i("total fare - ", total_fare+"");
                    services_selected[3] = false;
                    booking_fare.setText("₹" + String.format("%.2f", total_fare_up[0]) + " ~ " + "₹" + String.format("%.2f", total_fare_down[0]));
                }
            }
        });

        select_service_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_5.isChecked()){
                    total_fare +=  Float.parseFloat(db.getServiceFare("WHEELCHAIR"));;
                    total_fare_up[0] += Float.parseFloat(db.getServiceFare("WHEELCHAIR"));
                    total_fare_down[0] += Float.parseFloat(db.getServiceFare("WHEELCHAIR"));
                    Log.i("total fare - ",total_fare+"");
                    services_selected[4] = true;
                    booking_fare.setText("₹" + String.format("%.2f", total_fare_up[0]) + " ~ " + "₹" + String.format("%.2f", total_fare_down[0]));
                } else {
                    total_fare -=  Float.parseFloat(db.getServiceFare("WHEELCHAIR"));
                    total_fare_up[0] -= Float.parseFloat(db.getServiceFare("WHEELCHAIR"));
                    total_fare_down[0] -= Float.parseFloat(db.getServiceFare("WHEELCHAIR"));
                    Log.i("total fare - ", total_fare+"");
                    services_selected[4] = false;
                    booking_fare.setText("₹" + String.format("%.2f", total_fare_up[0]) + " ~ " + "₹" + String.format("%.2f", total_fare_down[0]));
                }
            }
        });

        select_service_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_6.isChecked()){
                    total_fare +=  Float.parseFloat(db.getServiceFare("HELPER"));
                    total_fare_up[0] += Float.parseFloat(db.getServiceFare("HELPER"));
                    total_fare_down[0] += Float.parseFloat(db.getServiceFare("HELPER"));
                    Log.i("total fare - ",total_fare+"");
                    services_selected[5] = true;
                    booking_fare.setText("₹" + String.format("%.2f", total_fare_up[0]) + " ~ " + "₹" + String.format("%.2f", total_fare_down[0]));
                } else {
                    total_fare -=  Float.parseFloat(db.getServiceFare("HELPER"));
                    total_fare_up[0] -= Float.parseFloat(db.getServiceFare("HELPER"));
                    total_fare_down[0] -= Float.parseFloat(db.getServiceFare("HELPER"));
                    Log.i("total fare - ", total_fare+"");
                    services_selected[5] = false;
                    booking_fare.setText("₹" + String.format("%.2f", total_fare_up[0]) + " ~ " + "₹" + String.format("%.2f", total_fare_down[0]));
                }
            }
        });

        select_service_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_7.isChecked()){
                    total_fare +=  Float.parseFloat(db.getServiceFare("DOCTOR"));
                    total_fare_up[0] += Float.parseFloat(db.getServiceFare("DOCTOR"));
                    total_fare_down[0] += Float.parseFloat(db.getServiceFare("DOCTOR"));
                    Log.i("total fare - ",total_fare+"");
                    services_selected[6] = true;
                    booking_fare.setText("₹" + String.format("%.2f", total_fare_up[0]) + " ~ " + "₹" + String.format("%.2f", total_fare_down[0]));
                } else {
                    total_fare -=  Float.parseFloat(db.getServiceFare("DOCTOR"));
                    total_fare_up[0] -= Float.parseFloat(db.getServiceFare("DOCTOR"));
                    total_fare_down[0] -= Float.parseFloat(db.getServiceFare("DOCTOR"));
                    Log.i("total fare - ", total_fare+"");
                    services_selected[6] = false;
                    booking_fare.setText("₹" + String.format("%.2f", total_fare_up[0]) + " ~ " + "₹" + String.format("%.2f", total_fare_down[0]));
                }
            }
        });
    }

    public void unCheckAll(){
        select_service_1.setChecked(false);
        //select_service_2.setChecked(false);
        //select_service_4.setChecked(false);
        select_service_3.setChecked(false);
        select_service_5.setChecked(false);
        select_service_6.setChecked(false);
        select_service_7.setChecked(false);
    }


    private boolean isPermissionGranted(){
        if(Build.VERSION.SDK_INT >= 23){
            if(checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED){
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PHONE_CODE);
                return false;
            }
        } else {

            return true;
        }

    }

    private void callAction() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        makeToast(home_booked_driver_phone.getText().toString());
        callIntent.setData(Uri.parse("tel:+91" + home_booked_driver_phone.getText().toString()));
        makeToast("calling...");

        startActivity(callIntent);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 10: if(grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED)
                donow();
                break;
            case 98: if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                            callAction();
                        } else {
                            makeToast("Permission denied!");
                        }
                        return;
            default:
                Log.i("Request Code ",requestCode+"");

                return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 23 :
                if ( !locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                    //switchGPS();
                }
                break;
            default:
                break;
        }
    }

    private void donow() {
        if(bundle != null && bundle.getBoolean("ORDER_SUCCESSFULL")){
            return;
        }
        if(mMap != null){
            mMap.setMyLocationEnabled(true);
            //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPlace, 16.0f));
            //mMap.addMarker(marker);
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(myPlace));
        }
        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, locationListener);
        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000,0,locationListener);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //myPlace = new LatLng(28.7041, 77.1025);      // Delhi Coordinates
        //marker = new MarkerOptions().position(myPlace).title("You Here");
        //noinspection MissingPermission
        //mMap.setMyLocationEnabled(true);
        //mMap.addMarker(marker);
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(myPlace));
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(googleApiClient);
        //Log.i(LOG_TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        //Log.e(LOG_TAG, "Google Places API connection suspended.");
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /*Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();*/
    }
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(googleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
           // Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();
            locationManager.removeUpdates(locationListener);
            hideSoftKeyboard();
            //setMarker(Html.fromHtml(place.getName().toString()).toString());

            myPlace = getCoordinate(place.getName().toString());
            setMarker();



           /* mNameTextView.setText(Html.fromHtml(place.getName() + ""));
            mAddressTextView.setText(Html.fromHtml(place.getAddress() + ""));
            mIdTextView.setText(Html.fromHtml(place.getId() + ""));
            mPhoneTextView.setText(Html.fromHtml(place.getPhoneNumber() + ""));
            mWebTextView.setText(place.getWebsiteUri() + "");
            if (attributions != null) {
                mAttTextView.setText(Html.fromHtml(attributions.toString()));
            }*/
        }
    };
    private AdapterView.OnItemClickListener mAutocompleteClickListenerTo
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {



            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(googleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallbackTo);
            // Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallbackTo
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();
            locationManager.removeUpdates(locationListener);
            hideSoftKeyboard();

            toPlace = getCoordinate(place.getName().toString());
            setMarker();


           /* mNameTextView.setText(Html.fromHtml(place.getName() + ""));
            mAddressTextView.setText(Html.fromHtml(place.getAddress() + ""));
            mIdTextView.setText(Html.fromHtml(place.getId() + ""));
            mPhoneTextView.setText(Html.fromHtml(place.getPhoneNumber() + ""));
            mWebTextView.setText(place.getWebsiteUri() + "");
            if (attributions != null) {
                mAttTextView.setText(Html.fromHtml(attributions.toString()));
            }*/
        }
    };

    //Function to get lat, long from selected places
    public void setMarker(){

        mMap.clear();

        if(myPlace != null){
            marker = new MarkerOptions().position(myPlace).title("You Here");
            mMap.addMarker(marker);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPlace, 16.0f));
        }

        if(toPlace !=null){
            marker = new MarkerOptions().position(toPlace).title("Destination");
            mMap.addMarker(marker);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(toPlace, 16.0f));
        }

        String origin = mAutocompleteTextView.getText().toString();
        String destination = mAutocompleteTextViewTo.getText().toString();
        sendPathRequest(origin, destination);

       

    }
    public LatLng getCoordinate(String strAddress){
        Log.i("Set Marker called ",strAddress);
        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null || address.size()<=0) {
                return null;
            }
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            return new LatLng( location.getLatitude(),  location.getLongitude());


        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }
    private void logOut() {

        if(MyFunctions.getLoginType(this).equals(ConstantsLocal.LOGIN_TYPE_NORMAL)){

        }else if(MyFunctions.getLoginType(this).equals(ConstantsLocal.LOGIN_TYPE_GOOGLE)){
            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    Toast.makeText(Home.this, "Successfully Logged Out",Toast.LENGTH_LONG).show();
                }
            });
        }else if(MyFunctions.getLoginType(this).equals(ConstantsLocal.LOGIN_TYPE_FACEBOOK)){

        }

        MyFunctions.clearPreferenceFile(this);

        Intent intent = new Intent(Home.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }
    public void changeLayoutDisabled(View view){
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = 50;
        view.setLayoutParams(params);
        if(view.getId() == R.id.autoCompleteTextView){
            clearSearch.setVisibility(View.GONE);
            clearSearchTo.setVisibility(View.VISIBLE);
        }else{
            clearSearch.setVisibility(View.VISIBLE);
            clearSearchTo.setVisibility(View.GONE);
        }
    }
    public void changeLayoutEnabled(View view){
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = 100;
        view.setLayoutParams(params);
        if(view.getId() == R.id.autoCompleteTextView){
            clearSearch.setVisibility(View.VISIBLE);
            clearSearchTo.setVisibility(View.GONE);
        }else{
            clearSearch.setVisibility(View.GONE);
            clearSearchTo.setVisibility(View.VISIBLE);
        }
    }
    private void sendPathRequest(String origin, String destination) {
        if (origin.isEmpty()) {
            Toast.makeText(this, "Please enter origin address!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (destination.isEmpty()) {
            Toast.makeText(this, "Please enter destination address!", Toast.LENGTH_SHORT).show();
            return;
        }

        //ac.showConfirmDialog(mMap.toString());
        mMap.clear();
        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        }
    }

    private void sendPathRequest2(String origin, String destination) {
        //locationManager.removeUpdates(locationListener);

        hideSoftKeyboard();
        if (origin.isEmpty()) {
            Toast.makeText(this, "Please enter origin address!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (destination.isEmpty()) {
            Toast.makeText(this, "Please enter destination address!", Toast.LENGTH_SHORT).show();
            return;
        }

        proceed_arrow.setClickable(true);

        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onDirectionFinderStart() {

        pc = new ProgressClass(Home.this);
        if(!pc.isActive())
            pc.startProgress("Fetching Direction..");

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {

        directionFound = true;


        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();
        //Log.i("route",routes.toString());


        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
            /*((TextView) findViewById(R.id.tvDuration)).setText(route.duration.text);
            ((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);*/
            //Log.i("Direcection success  ",route.distance.text+" "+route.distance.value+" "+route.duration.text+" "+route.duration.value);

            duration = route.duration.value;
            distance = route.distance.value/1000;

            MarkerOptions st = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.tomarker))
                    .title(route.startAddress)
                    .position(route.startLocation);
            MarkerOptions en = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.frommarker))
                    .title(route.endAddress)
                    .position(route.endLocation);
            originMarkers.add(mMap.addMarker(st));
            destinationMarkers.add(mMap.addMarker(en));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.parseColor("#bd3f32")).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(st.getPosition());
            builder.include(en.getPosition());
            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200);
            mMap.animateCamera(cu);

            startDirection = route.startLocation;
            endDirection = route.endLocation;

            //Log.i("location - ", route.startLocation+" -- "+route.endLocation);

        }
        if(pc != null && pc.isActive()) pc.stopProgress();

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) down_relative_layout.getLayoutParams();
        params.height = (int) convertDpToPixel(210, getApplicationContext());
        params.topMargin = (int) -convertDpToPixel(22, getApplicationContext());
        //Log.i("params height -- ", params.height + "");
        down_relative_layout.setVisibility(View.VISIBLE);
        vehicle_text_layout.setVisibility(View.GONE);
        //slideToTop(input_layout);
    }
    public String getLocationNameFromCoordinate(LatLng coord){
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(coord.latitude, coord.longitude, 1);
            if (addresses.size() > 0)
            {

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String locale = addresses.get(0).getSubLocality();
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                if(address == null)
                    address = "";
                //Log.i("a ",address+" "+state+" "+country+" "+postalCode+" "+knownName);

                return address+" "+city+" "+country;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    // To animate view slide out from left to right
    public void slideToRight(View view){
        TranslateAnimation animate = new TranslateAnimation(0,view.getWidth(),0,0);
        animate.setDuration(500);
        //animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }
    // To animate view slide out from right to left
    public void slideToLeft(View view){
        TranslateAnimation animate = new TranslateAnimation(0,-view.getWidth(),0,0);
        animate.setDuration(500);
        //animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    // To animate view slide out from top to bottom
    public void slideToBottom(View view){
        TranslateAnimation animate = new TranslateAnimation(0,0,0,view.getHeight());
        animate.setDuration(500);
        //animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    // To animate view slide out from bottom to top
    public void slideToTop(View view){
        TranslateAnimation animate = new TranslateAnimation(0,0,0,-view.getHeight());
        animate.setDuration(1000);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    public void makeToast(String query){
        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
    }

    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        //Log.i("metrcs -- ", ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT) + "");
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public class sendAlsDetails extends AsyncTask<String, String, String> {

        String res, st;
        Map<String, String> param;
        String url = ConstantsLocal.HOME_URL+"send_als_details.php";

        public sendAlsDetails(Map<String, String> param){
            this.param = param;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                res = RequestServer.POST(client, url, RequestServer.LoginBody(param));
            } catch (IOException e) {
                e.printStackTrace();
            }


            //Log.d("accept response - - - ", res);

            return res;
        }

        @Override
        protected void onPostExecute(String s) {

            if(s != null) {

                if (pc != null && pc.isActive())
                    pc.stopProgress();

                try {
                    //Log.d("res", s);
                    JSONObject obj = new JSONObject(s);

                    String r = obj.getString("status");
                    if (r.equalsIgnoreCase("success")) {
                        MyFunctions.showToast(Home.this, "als sent!");

                    } else {
                        MyFunctions.showToast(Home.this, "There was some error!");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

