package com.example.sarfraz.ems;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.instamojo.android.Instamojo;
import com.instamojo.android.activities.PaymentDetailsActivity;
import com.instamojo.android.callbacks.OrderRequestCallBack;
import com.instamojo.android.helpers.Constants;
import com.instamojo.android.models.Errors;
import com.instamojo.android.models.Order;
import com.instamojo.android.network.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class Payment extends AppCompatActivity {

    TextView toolbar_title, service_name_1, service_name_2, service_name_3, service_name_4, service_name_5, service_name_6, service_name_7, base_fare_head, services_head, total_cost_head, payment_from_address, payment_to_address, distance_covered_payment, distance_fare_payment, service_price_1_payment, service_price_2_payment, service_price_3_payment, service_price_4_payment, service_price_5_payment, service_price_6_payment, service_price_7_payment, total_cost_payment;
    LinearLayout services_layout_payment, service_1_payment, service_2_payment, service_3_payment, service_4_payment, service_5_payment, service_6_payment, service_7_payment;
    Button cash_btn, online_btn;
    Typeface raleway_medium, raleway_light;
    ProgressClass pc;
    AlertClass ac;
    OkHttpClient client;
    Bundle bundle;
    String user_name, phone, email, total_fare, vehicle_selected, user_id, driver_id, services, payment_option, distance, from_address, to_address, from_lat_lng, to_lat_lng, user_payment_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Instamojo.initialize(this);
        //Instamojo.setLogLevel(Log.DEBUG);
        Instamojo.setBaseUrl("https://api.instamojo.com/");

        init();
    }

    private void init() {
        raleway_light = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        raleway_medium = Typeface.createFromAsset(getAssets(), "Raleway-Medium.ttf");

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        payment_from_address = (TextView) findViewById(R.id.payment_from_address);
        payment_to_address = (TextView) findViewById(R.id.payment_to_address);
        distance_covered_payment = (TextView) findViewById(R.id.distance_covered_payment);
        distance_fare_payment = (TextView) findViewById(R.id.distance_fare_payment);
        service_price_1_payment = (TextView) findViewById(R.id.service_price_1_payment);
        service_price_2_payment = (TextView) findViewById(R.id.service_price_2_payment);
        service_price_3_payment = (TextView) findViewById(R.id.service_price_3_payment);
        service_price_4_payment = (TextView) findViewById(R.id.service_price_4_payment);
        service_price_5_payment = (TextView) findViewById(R.id.service_price_5_payment);
        service_price_6_payment = (TextView) findViewById(R.id.service_price_6_payment);
        service_price_7_payment = (TextView) findViewById(R.id.service_price_7_payment);
        total_cost_payment = (TextView) findViewById(R.id.total_cost_payment);
        services_head = (TextView) findViewById(R.id.services_head);
        base_fare_head = (TextView) findViewById(R.id.base_fare_head);
        total_cost_head = (TextView) findViewById(R.id.total_cose_head);
        service_name_1 = (TextView) findViewById(R.id.service_name_1);
        service_name_2 = (TextView) findViewById(R.id.service_name_2);
        service_name_3 = (TextView) findViewById(R.id.service_name_3);
        service_name_4 = (TextView) findViewById(R.id.service_name_4);
        service_name_5 = (TextView) findViewById(R.id.service_name_5);
        service_name_6 = (TextView) findViewById(R.id.service_name_6);
        service_name_7 = (TextView) findViewById(R.id.service_name_7);

        services_layout_payment = (LinearLayout) findViewById(R.id.services_layout_payment);
        service_1_payment = (LinearLayout) findViewById(R.id.service_1_payment);
        service_2_payment = (LinearLayout) findViewById(R.id.service_2_payment);
        service_3_payment = (LinearLayout) findViewById(R.id.service_3_payment);
        service_4_payment = (LinearLayout) findViewById(R.id.service_4_payment);
        service_5_payment = (LinearLayout) findViewById(R.id.service_5_payment);
        service_6_payment = (LinearLayout) findViewById(R.id.service_6_payment);
        service_7_payment = (LinearLayout) findViewById(R.id.service_7_payment);

        cash_btn = (Button) findViewById(R.id.cash_btn);
        online_btn = (Button) findViewById(R.id.online_btn);
        cash_btn.setTransformationMethod(null);
        online_btn.setTransformationMethod(null);

        //toolbar_title.setTypeface(raleway_medium);
        payment_from_address.setTypeface(raleway_light);
        payment_to_address.setTypeface(raleway_light);
        distance_covered_payment.setTypeface(raleway_light);
        distance_fare_payment.setTypeface(raleway_light);
        service_price_5_payment.setTypeface(raleway_light);
        service_price_6_payment.setTypeface(raleway_light);
        service_price_4_payment.setTypeface(raleway_light);
        service_price_3_payment.setTypeface(raleway_light);
        service_price_1_payment.setTypeface(raleway_light);
        service_price_7_payment.setTypeface(raleway_light);
        service_price_2_payment.setTypeface(raleway_light);
        total_cost_payment.setTypeface(raleway_light);
        service_name_1.setTypeface(raleway_light);
        service_name_2.setTypeface(raleway_light);
        service_name_3.setTypeface(raleway_light);
        service_name_4.setTypeface(raleway_light);
        service_name_5.setTypeface(raleway_light);
        service_name_6.setTypeface(raleway_light);
        service_name_7.setTypeface(raleway_light);
        services_head.setTypeface(raleway_medium);
        base_fare_head.setTypeface(raleway_medium);
        total_cost_head.setTypeface(raleway_medium);
        cash_btn.setTypeface(raleway_medium);
        online_btn.setTypeface(raleway_medium);

        ac = new AlertClass(this);
        pc = new ProgressClass(this);

        client = new OkHttpClient();
        bundle = new Bundle();

        getPaymentData();

        online_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchTokenAndTransactionID();
            }
        });
    }

    private void getPaymentData() {
        final Map<String, String> param = new HashMap<String, String>();
        param.put("user_payment_id", "348");


        pc = new ProgressClass(this);
        pc.startProgress("Please wait...");
        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.i("server res ",s);
                if(pc != null && pc.isActive())
                pc.stopProgress();

                try {
                    JSONObject parentObject = new JSONObject(s);
                    JSONObject obj = parentObject.getJSONObject("data");

                    String status = obj.get("status").toString();

                    if(status.equals("success")){

                        JSONObject journey_row = obj.getJSONObject("journey_row");
                        JSONObject user_row = obj.getJSONObject("user_row");
                        JSONObject driver_row = obj.getJSONObject("driver_row");

                        user_name = user_row.getString("name");
                        phone = user_row.getString("phone");
                        email = user_row.getString("email");
                        total_fare = journey_row.getString("amount");
                        vehicle_selected = journey_row.getString("vehicle_type");
                        user_id = user_row.getString("id");
                        driver_id = driver_row.getString("id");
                        services = journey_row.getString("services");
                        distance = journey_row.getString("distance");
                        from_address = journey_row.getString("from_address");
                        to_address = journey_row.getString("to_address");
                        from_lat_lng = journey_row.getString("from_lat_lng");
                        to_lat_lng = journey_row.getString("to_lat_lng");
                        user_payment_id = journey_row.getString("user_payment_id");
                    } else {
                        showToast("There was some error!");
                        Log.d("error",  obj.get("error").toString());
                        return;

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(ConstantsLocal.GET_PAYMENT_DATA);
    }

    private void fetchTokenAndTransactionID() {

        final String[] access_token = new String[1];
        final String[] token_type = new String[1];

        final Map<String, String> param = new HashMap<String, String>();
        param.put("CREATE", "TRUE");


        pc = new ProgressClass(this);
        pc.startProgress("Please wait...");
        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.i("server res ",s);
                if(pc != null && pc.isActive())
                pc.stopProgress();

                try {
                    JSONObject parentObject = new JSONObject(s);
                    JSONObject obj = parentObject.getJSONObject("data");

                    String status = obj.get("status").toString();

                    if(status.equals("success")){

                        access_token[0] = obj.get("access_token").toString();
                        token_type[0] = obj.get("token_type").toString();

                        showToast("access_token - " + access_token[0]);
                        createPaymentRequest(access_token[0], token_type[0]);

                    } else {
                        showToast("There was some error!");
                        return;

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(ConstantsLocal.GENERATE_TOKEN_URL);
    }

    private void createPaymentRequest(String access_token, String token_type) {

        final String[] order_id = new String[1];
        final String[] a_token = new String[1];
        final String[] prid = new String[1];
        //purpose = "Ambulance";


        pc = new ProgressClass(this);
        pc.startProgress("Please wait...");

        final Map<String, String> param = new HashMap<String, String>();
        param.put("ACCESS_TOKEN", access_token);
        param.put("TOKEN_TYPE", token_type);
        param.put("NAME", user_name);
        param.put("EMAIL", email);
        param.put("PHONE", phone);
        param.put("AMOUNT", total_fare);
        param.put("PURPOSE", vehicle_selected);
        param.put("USER_ID", user_id);
        param.put("DRIVER_ID", driver_id);
        param.put("SERVICES", services);
        param.put("PAYMENT_OPTION", "ONLINE");
        param.put("VEHICLE_SELECTED", vehicle_selected);
        param.put("DISTANCE", distance);
        param.put("FROM_ADDRESS", from_address);
        param.put("TO_ADDRESS", to_address);
        param.put("FROM_LAT_LNG", from_lat_lng);
        param.put("TO_LAT_LNG", to_lat_lng);
        param.put("USER_PAYMENT_ID", user_payment_id);

        //showToast(from_lat_lng + to_lat_lng);
        //ac.showConfirmDialog(param.toString());

        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.d("server res ",s);
                //ac.showConfirmDialog(s);
                //showToast(s);
                if(pc != null && pc.isActive())
                pc.stopProgress();

                try {
                    JSONObject parentObject = new JSONObject(s);
                    JSONObject obj = parentObject.getJSONObject("data");

                    String status = obj.get("status").toString();

                    if(status.equals("success")){

                        a_token[0] = obj.get("access_token").toString();
                        order_id[0] = obj.get("order_id").toString();
                        prid[0] = obj.get("prid").toString();

                        //ac.showConfirmDialog("access_token - " + a_token[0] + ", order_id - " + order_id[0] + ", prid - " + prid[0]);

                        fetchOrder(a_token[0], order_id[0]);
                        //showToast("prid - " + prid[0]);


                    } else {

                        showToast("There was some error!");
                        return;

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ac.showOkDialog("JSON PARSER ERROR!", "");
                }
            }
        }.execute(ConstantsLocal.PAYMENT_REQUEST_URL);
    }

    private void fetchOrder(String access_token, String order_id) {

        // Good time to show dialog


        pc = new ProgressClass(this);
        pc.startProgress("Please wait...");

        Request request = new Request(access_token, order_id, new OrderRequestCallBack() {
            @Override
            public void onFinish(final Order order, final Exception error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pc.stopProgress();
                        if (error != null) {
                            if (error instanceof Errors.ConnectionError) {
                                showToast("No internet connection");
                            } else if (error instanceof Errors.ServerError) {
                                showToast("Server Error. Try again");
                            } else if (error instanceof Errors.AuthenticationError) {
                                showToast("Access token is invalid or expired. Please Update the token!!");
                            } else {
                                showToast(error.toString());
                            }
                            return;
                        }

                        //showToast(order.toString());
                        Log.i("order - ", order.toString());
                        order.setWebhook("http://emsplus.co.in/android/payment/webhook.php");
                        startPreCreatedUI(order);
                    }
                });

            }
        });
        request.execute();

    }

    private void startPreCreatedUI(Order order){
        //Using Pre created UI
        Intent intent = new Intent(getBaseContext(), PaymentDetailsActivity.class);
        intent.putExtra(Constants.ORDER, order);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE && data != null) {
            String orderID = data.getStringExtra(Constants.ORDER_ID);
            String transactionID = data.getStringExtra(Constants.TRANSACTION_ID);
            String paymentID = data.getStringExtra(Constants.PAYMENT_ID);

            // Check transactionID, orderID, and orderID for null before using them to check the Payment status.
            if (orderID != null && transactionID != null && paymentID != null) {
                //Check for Payment status with Order ID or Transaction ID
                Log.i("order status - ", orderID + ", " + transactionID + ", " + paymentID);
                checkPaymentStatus(orderID, transactionID, paymentID);
            } else {

                showToast("Oops!! Payment was cancelled");
                if(bundle == null){
                    bundle = new Bundle();
                }
                bundle.putString("status", "cancelled");
                startActivity(new Intent(Payment.this, Home.class).putExtras(bundle));
            }


        }
    }

    private void checkPaymentStatus(String orderID, String transactionID, String paymentID) {

        showToast("onActivityResult");

        //GET STATUS
        final Map<String, String> param = new HashMap<String, String>();
        param.put("PR_ID", orderID);

        final String[] payment_status = new String[1];
        final String[] pid = new String[1];
        final String[] prid = new String[1];


        pc = new ProgressClass(this);
        pc.startProgress("Please wait...");

        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.i("server res ",s);
                if(pc != null && pc.isActive())
                pc.stopProgress();
                try {
                    JSONObject parentObject = new JSONObject(s);
                    JSONObject obj = parentObject.getJSONObject("data");

                    String status = obj.get("status").toString();

                    if(status.equals("success")){

                        payment_status[0] = obj.get("payment_status").toString();
                        pid[0] = obj.get("pid").toString();
                        prid[0] = obj.get("prid").toString();

                        Log.i("status", payment_status[0] + ", " + pid[0] + ", " + prid[0]);
                        bundle.putString("status", payment_status[0]);
                        bundle.putString("pid", pid[0]);
                        startActivity(new Intent(Payment.this, Home.class).putExtras(bundle));

                    } else {

                        showToast("There was some error - " + obj.get("error").toString());
                        if(bundle == null){
                            bundle = new Bundle();
                        }
                        bundle.putString("status", payment_status[0]);
                        startActivity(new Intent(Payment.this, Home.class).putExtras(bundle));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(ConstantsLocal.GET_STATUS_URL);

    }

    public void showToast(String query){
        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
    }
}
