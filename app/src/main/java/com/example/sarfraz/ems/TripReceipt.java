package com.example.sarfraz.ems;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import okhttp3.OkHttpClient;

public class TripReceipt extends AppCompatActivity {

    private LinearLayout service_1;
    private LinearLayout service_2;
    private LinearLayout service_3;
    private LinearLayout service_4;
    private LinearLayout service_5;
    private LinearLayout service_6;
    private LinearLayout service_7;
    private LinearLayout services_layout;
    private ScrollView finish_journey_scrollview;

    private TextView service_price_1;
    private TextView service_price_2;
    private TextView service_price_3;
    private TextView service_price_4;
    private TextView service_price_5;
    private TextView service_price_6;
    private TextView service_price_7;

    private LinearLayout set_feed;
    private RelativeLayout get_feed;

    TextView feedback_text, feedback_header, did_you_mean;
    LinearLayout feedback_layout, diseases_layout;
    RelativeLayout loader_layout;

    float distance_travelled;
    long duration;
    String services_db, vehicle_type;
    FareCalculator fc;
    EmsDatabase ed;

    //float service_fare_1, service_fare_2, service_fare_3, service_fare_4, service_fare_5, service_fare_6, service_fare_7, tms_fare, bms_fare, sms_fare, morgue_fare, event_fare, services_fare = 0.0f;
    float total_fare, duration_fare, service_fare;

    private TextView base_fare;
    private TextView distance_covered;
    private TextView time_taken;
    private TextView time_fare;
    private TextView category;
    private TextView distance_fare_finish_journey, duration_finish_journey;
    private TextView toolbar_title, base_fare_head, services_head, service_name_1, service_name_2, service_name_3, service_name_4, service_name_5, service_name_6, service_name_7;


    private TextView payment_method;
    private TextView total_cost;

    private TextView trip_receipt_driver_name, trip_receipt_driver_phone, trip_receipt_driver_vehicle_number;
    private TextView trip_receipt_from_address, trip_receipt_to_address;

    String user_payment_id, response;

    OkHttpClient client;
    SharedPreferences sp;
    ProgressClass pc;
    AlertClass ac;

    Bundle bundle;
    Typeface raleway_light, raleway_medium, raleway_light_italic;

    //private Button pay_now_finish_journey;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_receipt);

        bundle = getIntent().getExtras();
        user_payment_id = bundle.getString("USER_PAYMENT_ID");
        response = bundle.getString("RESPONSE");

        init();

    }

    private void init() {
        service_1 = (LinearLayout) findViewById(R.id.service_1_finish_journey);
        service_2 = (LinearLayout) findViewById(R.id.service_2_finish_journey);
        service_3 = (LinearLayout) findViewById(R.id.service_3_finish_journey);
        service_4 = (LinearLayout) findViewById(R.id.service_4_finish_journey);
        service_5 = (LinearLayout) findViewById(R.id.service_5_finish_journey);
        service_6 = (LinearLayout) findViewById(R.id.service_6_finish_journey);
        service_7 = (LinearLayout) findViewById(R.id.service_7_finish_journey);
        services_layout = (LinearLayout) findViewById(R.id.services_layout);
        finish_journey_scrollview = (ScrollView) findViewById(R.id.finish_journey_scrollview);

        service_price_1 = (TextView) findViewById(R.id.service_price_1_finish_journey);
        service_price_2 = (TextView) findViewById(R.id.service_price_2_finish_journey);
        service_price_3 = (TextView) findViewById(R.id.service_price_3_finish_journey);
        service_price_4 = (TextView) findViewById(R.id.service_price_4_finish_journey);
        service_price_5 = (TextView) findViewById(R.id.service_price_5_finish_journey);
        service_price_6 = (TextView) findViewById(R.id.service_price_6_finish_journey);
        service_price_7 = (TextView) findViewById(R.id.service_price_7_finish_journey);

        service_name_1 = (TextView) findViewById(R.id.service_name_1);
        service_name_2 = (TextView) findViewById(R.id.service_name_2);
        service_name_3 = (TextView) findViewById(R.id.service_name_3);
        service_name_4 = (TextView) findViewById(R.id.service_name_4);
        service_name_5 = (TextView) findViewById(R.id.service_name_5);
        service_name_6 = (TextView) findViewById(R.id.service_name_6);
        service_name_7 = (TextView) findViewById(R.id.service_name_7);
        base_fare_head = (TextView) findViewById(R.id.base_fare_head);
        services_head = (TextView) findViewById(R.id.services_head);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);

        feedback_text = (TextView) findViewById(R.id.feedback_text);
        feedback_header = (TextView) findViewById(R.id.feedback_header);
        feedback_layout = (LinearLayout) findViewById(R.id.feedback_layout);
        loader_layout = (RelativeLayout) findViewById(R.id.loader_layout);
        diseases_layout = (LinearLayout) findViewById(R.id.diseases_layout);



        //base_fare = (TextView) findViewById(R.id.base_fare_finish_journey);
        distance_covered = (TextView) findViewById(R.id.distance_covered_finish_journey);
        distance_fare_finish_journey = (TextView) findViewById(R.id.distance_fare_finish_journey);
        //time_taken = (TextView) findViewById(R.id.time_taken_finish_journey);
        //time_fare = (TextView) findViewById(R.id.time_fare_finish_journey);
        duration_finish_journey = (TextView) findViewById(R.id.duration_finish_journey);

        payment_method = (TextView) findViewById(R.id.payment_method_finish_journey);
        total_cost = (TextView) findViewById(R.id.total_cost_finish_journey);

        trip_receipt_driver_name = (TextView) findViewById(R.id.trip_receipt_driver_name);
        trip_receipt_driver_phone = (TextView) findViewById(R.id.trip_receipt_driver_phone);
        trip_receipt_driver_vehicle_number = (TextView) findViewById(R.id.trip_receipt_driver_vehicle_number);

        trip_receipt_from_address = (TextView) findViewById(R.id.trip_receipt_from_address);
        trip_receipt_to_address = (TextView) findViewById(R.id.trip_receipt_to_address);

        ac = new AlertClass(this);
        pc = new ProgressClass(this);



        //pay_now_finish_journey = (Button) findViewById(R.id.pay_now_finish_journey);

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //fonts

        raleway_light = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        raleway_medium = Typeface.createFromAsset(getAssets(), "Raleway-Medium.ttf");
        raleway_light_italic = Typeface.createFromAsset(getAssets(), "Raleway-LightItalic.ttf");

        toolbar_title.setTypeface(raleway_medium);
        trip_receipt_driver_name.setTypeface(raleway_medium);
        trip_receipt_from_address.setTypeface(raleway_light);
        trip_receipt_to_address.setTypeface(raleway_light);
        base_fare_head.setTypeface(raleway_medium);
        services_head.setTypeface(raleway_medium);
        service_name_1.setTypeface(raleway_light);
        service_name_2.setTypeface(raleway_light);
        service_name_3.setTypeface(raleway_light);
        service_name_4.setTypeface(raleway_light);
        service_name_5.setTypeface(raleway_light);
        service_name_6.setTypeface(raleway_light);
        service_name_7.setTypeface(raleway_light);


        payment_method.setTypeface(raleway_medium);

        hideServices();

        finish_journey_scrollview.setVisibility(View.GONE);

        getReceiptDetails();
    }

    private void getBasicFare(String distance, String services, long duration, String vehicle_type){
        Log.d("database - ", distance + "  " + services + "  " + duration + "  " + vehicle_type);
        fc = new FareCalculator(distance, services, duration, vehicle_type, this);
        ed = new EmsDatabase(this);

        total_fare = fc.getFare().getTotal_fare();
        Log.d("total fare - ", +total_fare+"");
        service_fare = fc.getFare().getService_fare();
        duration_fare = fc.getFare().getDuration_fare();
    }

    /*private void getBasicFare() {

        pc = new ProgressClass(this);
        ac = new AlertClass(this);
        client = new OkHttpClient();

        final Map<String, String> param = new HashMap<String, String>();
        param.put("flag", "TRUE");

        pc.startProgress("Fetching details. Please wait...");
        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.i("server res ",s);

                try {
                    jsonArray = new JSONArray(s);
                    JSONObject obj = jsonArray.getJSONObject(0);

                    String status = obj.get("STATUS").toString();

                    if(status.equals("SUCCESS")){

                        //fare_flag = true;
                        service_fare_1 = Float.parseFloat(obj.get("VENTILLATOR").toString());
                        service_fare_2 = Float.parseFloat(obj.get("STRETCHER").toString());
                        service_fare_3 = Float.parseFloat(obj.get("PARAMEDIC").toString());
                        service_fare_4 = Float.parseFloat(obj.get("OXYGEN").toString());
                        service_fare_5 = Float.parseFloat(obj.get("WHEELCHAIR").toString());
                        service_fare_6 = Float.parseFloat(obj.get("HELPER").toString());
                        service_fare_7 = Float.parseFloat(obj.get("DOCTOR").toString());
                        tms_fare = Float.parseFloat(obj.get("TMS").toString());
                        bms_fare = Float.parseFloat(obj.get("BMS").toString());
                        sms_fare = Float.parseFloat(obj.get("ALS").toString());
                        morgue_fare = Float.parseFloat(obj.get("MORGUE").toString());
                        event_fare = Float.parseFloat(obj.get("EVENT").toString());

                        getReceiptDetails();

                    } else {

                        ac.showOkDialog("There was some error!", "");
                        return;

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(ConstantsLocal.BASE_FARE_URL);
    }*/


    private void getReceiptDetails() {
        //showToast(user_payment_id);
        client = new OkHttpClient();
        //sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, MODE_PRIVATE);

        final String[] services = new String[1];
        final String[][] services_selected = {{""}};

        //getting trip receipt

        final Map<String, String> param = new HashMap<String, String>();
        param.put("USER_PAYMENT_ID", user_payment_id);

        //showToast(param.toString());

        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    //Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                    //ac.showOkDialog(e.toString(), e.toString());
                    //showToast(e.toString());
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(pc != null && pc.isActive())
                    pc.stopProgress();

                if(s != null) {

                    Log.i("server res - ", s);
                    //ac.showOkDialog(s, "");
                    JSONObject parentObject = null;
                    try {
                        parentObject = new JSONObject(s);
                        JSONObject obj = parentObject.getJSONObject("data");

                        String status = obj.get("status").toString();

                        if (status.equals("success")) {
                            //showToast("success");
                            //ac.showOkDialog(obj.get("row").toString(), "");
                            //ac.showOkDialog(obj.get("driver_row").toString(), "");
                            final JSONObject row = obj.getJSONObject("row");
                            JSONObject driver_row = obj.getJSONObject("driver_row");
                            final JSONObject disease_row = obj.getJSONObject("disease_row");

                            trip_receipt_driver_name.setText(driver_row.get("name").toString());
                            trip_receipt_driver_phone.setText(driver_row.get("phone").toString());
                            trip_receipt_driver_vehicle_number.setText(row.get("vehicle_type").toString() + " (" + driver_row.get("vehicle_number").toString() + ")");
                            trip_receipt_from_address.setText(row.get("from_address").toString());
                            trip_receipt_to_address.setText(row.get("to_address").toString());
                            distance_covered.setText(row.get("distance").toString() + " km");
                            String payment_type = row.get("payment_type").toString();
                            payment_type = payment_type.substring(0, 1).toUpperCase() + payment_type.substring(1).toLowerCase();
                            payment_method.setText(payment_type);
                            //total_cost.setText("₹" + row.get("amount").toString());
                            distance_travelled = Float.parseFloat(row.getString("distance"));
                            services_db = row.getString("services");
                            vehicle_type = row.getString("vehicle_type");
                            duration = Long.parseLong(row.getString("time_taken"));

                            if(disease_row.getString("flag").equalsIgnoreCase("true")){
                                JSONArray diseases = disease_row.getJSONArray("data");
                                for(int i=0;i<diseases.length();i++){
                                    final JSONObject disease = diseases.getJSONObject(i);
                                    View child = getLayoutInflater().inflate(R.layout.disease_name, null);
                                    TextView name = (TextView) child.findViewById(R.id.disease_name);
                                    name.setTypeface(raleway_light);
                                    name.setText(disease.getString("name"));
                                    LinearLayout.LayoutParams childParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    childParams.setMargins(0, 10, 0, 0);
                                    child.setLayoutParams(childParams);
                                    child.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showDetailsPopUp(disease);
                                        }
                                    });
                                    diseases_layout.addView(child);
                                }
                            }


                            if(row.getString("purpose").equalsIgnoreCase("")){
                                feedback_layout.setVisibility(View.GONE);
                                final Dialog dialog = new Dialog(TripReceipt.this);
                                dialog.setContentView(R.layout.feedback_layout);

                                // set the custom dialog components - text, image and button
                                ImageView image = (ImageView) dialog.findViewById(R.id.image);
                                TextView text = (TextView) dialog.findViewById(R.id.text);
                                final Button dialogButton = (Button) dialog.findViewById(R.id.dialogButton);
                                TextInputLayout inputLayout = (TextInputLayout) dialog.findViewById(R.id.inputLayout);

                                text.setTypeface(raleway_light_italic);
                                dialogButton.setTypeface(raleway_medium);
                                inputLayout.setTypeface(raleway_medium);
                                final EditText input = (EditText) dialog.findViewById(R.id.input);

                                input.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                        if(input.getText().toString().trim().length() == 0){
                                            dialogButton.setText("SKIP");
                                        } else {
                                            dialogButton.setText("UPDATE");
                                        }
                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        if(input.getText().toString().trim().length() == 0){
                                            dialogButton.setText("SKIP");
                                        } else {
                                            dialogButton.setText("UPDATE");
                                        }
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        if(input.getText().toString().trim().length() == 0){
                                            dialogButton.setText("SKIP");
                                        } else {
                                            dialogButton.setText("UPDATE");
                                        }
                                    }
                                });

                                // if button is clicked, close the custom dialog
                                dialogButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(input.getText().toString().trim().length() != 0){
                                            //Toast.makeText(TripReceipt.this, input.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                                            Map<String, String> map = new HashMap<String, String>();
                                            try {
                                                map.put("user_payment_id", row.getString("user_payment_id"));
                                                map.put("purpose", input.getText().toString().trim());
                                                Log.d("map -- - - ", map.toString());
                                                //ac.showConfirmDialog(map.toString());
                                                pc.startProgress("Please wait...");
                                                new getSuggestions(ConstantsLocal.GET_SUGGESTIONS_LINK, map,row.getString("user_payment_id")).execute();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }



                                        }

                                            dialog.dismiss();
                                    }
                                });

                                //dialog.show();
                            } else {

                                feedback_header.setTypeface(raleway_medium);
                                feedback_text.setTypeface(raleway_light_italic);
                                feedback_text.setText(row.getString("purpose"));

                            }

                            Log.d("time_taken", duration + "");

                            getBasicFare(distance_travelled + "", services_db, duration, vehicle_type);


                            if (!row.get("services").toString().equals("")) {

                                services[0] = services_db;
                                services_selected[0] = services[0].split(",");

                                for (String serv :
                                        services_selected[0]) {

                                    switch (serv.trim()) {
                                        case "VENTILATOR":
                                            service_1.setVisibility(View.VISIBLE);
                                            service_price_1.setText("₹" + ed.getServiceFare("VENTILATOR"));
                                            break;
                                        case "STRETCHER":
                                            service_2.setVisibility(View.VISIBLE);
                                            service_price_2.setText("₹" + ed.getServiceFare("STRETCHER"));
                                            break;
                                        case "PARAMEDIC":
                                            service_3.setVisibility(View.VISIBLE);
                                            service_price_3.setText("₹" + ed.getServiceFare("PARAMEDIC"));
                                            break;
                                        case "OXYGEN":
                                            service_4.setVisibility(View.VISIBLE);
                                            if (distance_travelled <= 40) {
                                                service_price_4.setText("₹" + ed.getServiceFare("OXYGEN_40"));
                                            } else {
                                                service_price_4.setText("₹" + ed.getServiceFare("OXYGEN_40_PLUS"));
                                            }
                                            break;
                                        case "WHEELCHAIR":
                                            service_5.setVisibility(View.VISIBLE);
                                            service_price_5.setText("₹" + ed.getServiceFare("WHEELCHAIR"));
                                            break;
                                        case "HELPER":
                                            service_6.setVisibility(View.VISIBLE);
                                            service_price_6.setText("₹" + ed.getServiceFare("HELPER"));
                                            break;
                                        case "DOCTOR":
                                            service_7.setVisibility(View.VISIBLE);
                                            service_price_7.setText("₹" + ed.getServiceFare("DOCTOR"));
                                            break;
                                        default:
                                            ac.showOkDialog(serv, "");
                                    }
                                }

                                services_layout.setVisibility(View.VISIBLE);


                            }

                            distance_fare_finish_journey.setText("₹" + String.format("%.2f", total_fare - service_fare));
                            long hour, min, sec;
                            hour = duration / 3600;
                            min = (duration % 3600) / 60;
                            sec = (duration / 60);

                            String time = "";

                            if (hour > 0) {
                                time = String.format("%02d hr", hour);
                            }

                            if (min > 0) {
                                time += String.format(" %02d min", min);
                            }

                            duration_finish_journey.setText(time);
                            total_cost.setText("₹" + String.format("%.2f", total_fare));

                            loader_layout.setVisibility(View.GONE);
                            finish_journey_scrollview.setVisibility(View.VISIBLE);
                        } else {

                            //showToast("there was some error - " + obj.get("error").toString());
                            ac.showOkDialog("Error", obj.get("error").toString());
                            finish();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute(ConstantsLocal.GET_TRIP_RECEIPT_URL);

    }

    public class getSuggestions extends AsyncTask<String, String, String>{

        String url;
        Map<String, String> map;
        String upid;
        public getSuggestions(String url, Map<String, String> map, String upid){
            this.map = map;
            this.url = url;
            this.upid = upid;
        }

        @Override
        protected String doInBackground(String... params) {
            String res = "";
            //Log.d("map --- ", map.toString());
            try {
                res = RequestServer.POST(client, url, RequestServer.LoginBody(map));
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Log.d("res - - -- ",res);
            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s != null) {
                if (pc != null && pc.isActive()) pc.stopProgress();
                try {
                    Log.d("update response -- ", s);
                    JSONObject obj = new JSONObject(s);
                    //ac.showConfirmDialog(s.toString());
                    if(obj.getString("status").equalsIgnoreCase("success")){
                        final String query = obj.getString("query");
                        JSONObject o = obj.getJSONObject("disease_data");
                        Iterator<String> keys= o.keys();
                        if(o.getBoolean("flag")){
                           final Dialog dialog = new Dialog(TripReceipt.this);
                            dialog.setContentView(R.layout.feedback_layout);
                            dialog.setCancelable(true);
                            get_feed = (RelativeLayout)dialog.findViewById(R.id.get_feedback_layout);
                            set_feed = (LinearLayout)dialog.findViewById(R.id.set_feedback_layout);
                            get_feed.setVisibility(View.GONE);
                            set_feed.setVisibility(View.VISIBLE);
                            did_you_mean = (TextView) dialog.findViewById(R.id.did_you_mean);
                            did_you_mean.setTypeface(raleway_medium);


                            TypedValue typedValue = new TypedValue();
                            getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
                            int clr = typedValue.data;
                            final Set<String> set = new HashSet<String>();
                            while (keys.hasNext())
                            {
                                final String keyValue = (String)keys.next();
                                if(!keyValue.equalsIgnoreCase("flag")){
                                    JSONObject data = o.getJSONObject(keyValue);
                                    final String disease = data.getString("disease");

                                    Log.d("disease",disease);
                                    CheckBox c = new CheckBox(getApplicationContext());
                                    c.setText(disease);
                                    c.setTextSize(16);
                                    c.setTextColor(Color.BLACK);
                                    c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                        @Override
                                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                            if(isChecked) set.add(keyValue);
                                            else  set.remove(keyValue);
                                            //Log.d("set : ",set.toString());
                                        }
                                    });
                                    set_feed.addView(c);


                                }

                            }

                            TextView submit = new TextView(getApplicationContext());
                            submit.setGravity(Gravity.RIGHT);
                            submit.setText("Submit");
                            submit.setTextColor(Color.BLACK);
                            submit.setPadding(0,0,50,20);
                            set_feed.addView(submit);
                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if(pc==null) pc = new ProgressClass(TripReceipt.this);
                                    pc.startProgress("Saving your response...");

                                    Map<String, String> mp = new HashMap<String, String>();


                                    String disease="";
                                    for(String d : set){
                                        disease+=d;
                                        disease+="/";
                                    }
                                    disease = disease.substring(0, disease.length()-1);
                                    mp.put("user_payment_id",upid);
                                    mp.put("diseases",disease);
                                    mp.put("purpose",query);
                                    Log.d("diseases", mp.toString());
                                    new send_real_disease(mp, dialog).execute();

                                }
                            });
                            dialog.show();

                        }else{
                            Log.d("flag",o.getBoolean("flag")+"");
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class send_real_disease extends AsyncTask<String, String, String>{

        Map<String, String> mp;
        String resp;
        Dialog dialog;
        String url = ConstantsLocal.HOME_URL+"update_purpose.php";

        public send_real_disease(Map<String, String> mp, Dialog dialog){
            this.mp = mp;
            this.dialog = dialog;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                resp = RequestServer.POST(client, url, RequestServer.LoginBody(mp));
            } catch (IOException e) {
                e.printStackTrace();
            }

            return resp;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(pc!=null && pc.isActive()) pc.stopProgress();
            dialog.dismiss();
            MyFunctions.showToast(TripReceipt.this, "Saved Successfully");
            Log.d("Response ",s);
            try {
                JSONObject obj = new JSONObject(s);
                if(obj.getString("status").equalsIgnoreCase("success")){
                        JSONObject disease_row = obj.getJSONObject("disease_row");
                        JSONArray diseases = disease_row.getJSONArray("data");
                        for(int i=0;i<diseases.length();i++){
                            final JSONObject disease = diseases.getJSONObject(i);
                            View child = getLayoutInflater().inflate(R.layout.disease_name, null);
                            TextView name = (TextView) child.findViewById(R.id.disease_name);
                            name.setTypeface(raleway_light);
                            name.setText(disease.getString("name"));
                            LinearLayout.LayoutParams childParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            childParams.setMargins(0, 10, 0, 0);
                            child.setLayoutParams(childParams);
                            child.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showDetailsPopUp(disease);
                                }
                            });
                            diseases_layout.addView(child);
                        }
                        feedback_text.setText(obj.getString("query"));
                        feedback_layout.setVisibility(View.VISIBLE);
                } else {
                    showToast("Error occured.");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void showDetailsPopUp(JSONObject data){

        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(TripReceipt.this);
        View dialog = getLayoutInflater().inflate(R.layout.dialog_my_feedback,null);
        bottomSheetDialog.setContentView(dialog);


        TextView disease_name = (TextView) dialog.findViewById(R.id.txt_disease_name);
        TextView disease_cat = (TextView) dialog.findViewById(R.id.txt_disease_category);
        TextView disease_desc = (TextView)dialog.findViewById(R.id.txt_disease_desc);
        TextView disease_symp = (TextView)dialog.findViewById(R.id.txt_symptoms);
        TextView disease_prec = (TextView)dialog.findViewById(R.id.txt_disease_prec);

        TextView disease_cat_head = (TextView) dialog.findViewById(R.id.txt_disease_category_head);
        TextView disease_desc_head = (TextView)dialog.findViewById(R.id.txt_disease_desc_head);
        TextView disease_symp_head = (TextView)dialog.findViewById(R.id.txt_symptoms_head);
        TextView disease_prec_head = (TextView)dialog.findViewById(R.id.txt_disease_prec_head);

        disease_name.setTypeface(raleway_medium);
        disease_cat.setTypeface(raleway_light);
        disease_desc.setTypeface(raleway_light);
        disease_symp.setTypeface(raleway_light);
        disease_prec.setTypeface(raleway_light);
        disease_cat_head.setTypeface(raleway_medium);
        disease_desc_head.setTypeface(raleway_medium);
        disease_symp_head.setTypeface(raleway_medium);
        disease_prec_head.setTypeface(raleway_medium);

        try {
            disease_name.setText(data.getString("name"));
            disease_cat.setText(data.getString("category"));
            disease_desc.setText(data.getString("description"));
            disease_symp.setText(data.getString("symtoms"));
            disease_prec.setText(data.getString("precautions"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        bottomSheetDialog.show();

    }

    private void hideServices() {

        service_1.setVisibility(View.GONE);
        service_2.setVisibility(View.GONE);
        service_3.setVisibility(View.GONE);
        service_4.setVisibility(View.GONE);
        service_5.setVisibility(View.GONE);
        service_6.setVisibility(View.GONE);
        service_7.setVisibility(View.GONE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed(){
        finish();
    }

    public void showToast(String query){
        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
    }

}
