package com.example.sarfraz.ems;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.OkHttpClient;

public class PaymentStatus extends AppCompatActivity {

    ImageView back_payment_status, status_image;
    Button timeout_retry_btn, timeout_cancel_btn;
    TextView  status_text, id_text, id_alert_text, loader_text, timeout_head, timeout_text;
    String status = "", pid = "", payment_status, driver_name, driver_phone, driver_vehicle_name, driver_vehicle_number;
    ProgressClass pc;
    Bundle bundle;
    GifImageView loader_image;
    Typeface raleway_light, raleway_medium;
    RelativeLayout success_layout, loader_layout;
    OkHttpClient client;
    int req_count = 0;
    AlertClass ac;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_status);

        bundle = getIntent().getExtras();
        Log.i("bundle", bundle.toString());
        status = bundle.getString("status");
        if(!status.equals("cancelled")){
            pid = bundle.getString("pid");
            driver_name = bundle.getString("driver_name");
            driver_phone = bundle.getString("driver_phone");
            driver_vehicle_number = bundle.getString("driver_vehicle_number");
            driver_vehicle_name = bundle.getString("driver_vehicle_name");
        }

        init();
    }

    private void init() {

        back_payment_status = (ImageView) findViewById(R.id.back_payment_status);
        status_image = (ImageView) findViewById(R.id.status_image);
        status_text = (TextView) findViewById(R.id.status_text);
        //id_text = (TextView) findViewById(R.id.id_text);
        //id_alert_text = (TextView) findViewById(R.id.id_alert_text);
        loader_text = (TextView) findViewById(R.id.loader_text);

        client = new OkHttpClient();
        ac = new AlertClass(this);

        success_layout = (RelativeLayout) findViewById(R.id.success_layout);
        loader_layout = (RelativeLayout) findViewById(R.id.loader_layout);

        loader_image = (GifImageView) findViewById(R.id.loader_image);
        loader_image.setGifImageResource(R.drawable.find_driver);

        //fonts

        raleway_light = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        raleway_medium = Typeface.createFromAsset(getAssets(), "Raleway-Medium.ttf");

        status_text.setTypeface(raleway_light);
        //id_text.setTypeface(raleway_medium);
        //id_alert_text.setTypeface(raleway_light);
        loader_text.setTypeface(raleway_medium);

        pc = new ProgressClass(this);

        success_layout.setVisibility(View.GONE);
        loader_layout.setVisibility(View.VISIBLE);




        if(status.equals("cancelled")){
            loader_layout.setVisibility(View.GONE);
            success_layout.setVisibility(View.VISIBLE);
            payment_status = "Your order has been cancelled!";
            //id_alert_text.setVisibility(View.GONE);
            status_image.setImageResource(R.drawable.cancelled);
            //id_text.setText(pid);
            status_text.setText(payment_status);
        } else if(status.equals("success")){

            //showToast("success");

            getDriverResponse();

        } else if(status.equals("failed")){
            loader_layout.setVisibility(View.GONE);
            success_layout.setVisibility(View.VISIBLE);
            payment_status = "Payment failed.";
            status_image.setImageResource(R.drawable.failed);
            //id_text.setText(pid);
            status_text.setText(payment_status);
        }


        back_payment_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                Intent intent = new Intent(PaymentStatus.this, Home.class).putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(PaymentStatus.this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void getDriverResponse() {

        if(!MyFunctions.isInternetAvailable(this)){
            MyFunctions.showToast(this, "No internet");
            return;
        }

        int timeout = 3000; // make the activity visible for 4 seconds

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                new DriverResponse(pid).execute();
            }
        }, timeout);

    }

    public class DriverResponse extends AsyncTask<String, String, String>{

        String pid, res, url = ConstantsLocal.HOME_URL + "check_driver_response.php", journey_status, user_payment_id;

        public DriverResponse(String pid){
            this.pid = pid;
        }

        @Override
        protected String doInBackground(String... params) {

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("pid", pid);
            try {
                res = RequestServer.POST(client, url, RequestServer.LoginBody(map));
                //Log.d("response", res);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(s != null) {

                try {
                    JSONObject obj = new JSONObject(s);
                    JSONObject data = obj.getJSONObject("data");
                    if (data.getString("status").equalsIgnoreCase("success")) {
                        JSONObject response = data.getJSONObject("response");
                        journey_status = response.getString("journey_status");
                        Log.d("journey_status", journey_status);
                        //showToast(journey_status);
                        user_payment_id = response.getString("id");

                        if (journey_status.equalsIgnoreCase("accepted")) {
                            loader_layout.setVisibility(View.GONE);
                            success_layout.setVisibility(View.VISIBLE);
                            payment_status = "Your order has been placed successfully.";
                            status_image.setImageResource(R.drawable.checked);
                            //id_text.setText(pid);
                            status_text.setText(payment_status);


                            pc = new ProgressClass(PaymentStatus.this);
                            pc.startProgress("Redirecting...");
                            int timeout = 2000; // make the activity visible for 4 seconds

                            Timer timer = new Timer();
                            timer.schedule(new TimerTask() {

                                @Override
                                public void run() {
                                    if (pc != null && pc.isActive())
                                        pc.stopProgress();
                                    bundle.putBoolean("ORDER_SUCCESSFULL", true);
                                    bundle.putString("PID", pid);
                                    Intent homepage = new Intent(PaymentStatus.this, Home.class);
                                    homepage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(homepage.putExtras(bundle));
                                }
                            }, timeout);
                        } else if (journey_status.equalsIgnoreCase("rejected")) {
                            startActivity(new Intent(PaymentStatus.this, Rejected.class));
                        } else if (journey_status.equalsIgnoreCase("requested_now") || journey_status.equalsIgnoreCase("requested")) {
                            req_count++;
                            if (req_count <= 40) {
                                getDriverResponse();
                            } else {
                                final Dialog dialog = new Dialog(PaymentStatus.this, R.style.DialogSlideAnimation);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.timeout_layout);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                                dialog.setCancelable(false);
                                dialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                                timeout_head = (TextView) dialog.findViewById(R.id.timeout_head);
                                timeout_text = (TextView) dialog.findViewById(R.id.timeout_text);
                                timeout_retry_btn = (Button) dialog.findViewById(R.id.timeout_retry_btn);
                                timeout_cancel_btn = (Button) dialog.findViewById(R.id.timeout_cancel_btn);
                                timeout_head.setTypeface(raleway_medium);
                                timeout_text.setTypeface(raleway_light);
                                if (!dialog.isShowing())
                                    dialog.show();

                                timeout_retry_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        int timeout = 2000; // make the activity visible for 4 seconds


                                        pc = new ProgressClass(PaymentStatus.this);
                                        pc.startProgress("Retrying...");

                                        Timer timer = new Timer();
                                        timer.schedule(new TimerTask() {

                                            @Override
                                            public void run() {
                                                if (pc.isActive())
                                                    pc.stopProgress();
                                                finish();
                                                Intent intent = new Intent(PaymentStatus.this, DriverDetails.class).putExtras(bundle);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                            }
                                        }, timeout);
                                    }
                                });

                                timeout_cancel_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        bundle = new Bundle();
                                        finish();
                                        Intent intent = new Intent(PaymentStatus.this, Home.class).putExtras(bundle);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                });


                            }
                        }
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void showToast(String query){
        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
    }
}
