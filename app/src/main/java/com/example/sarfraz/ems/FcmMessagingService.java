package com.example.sarfraz.ems;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sarfraz on 2/8/17.
 */

public class FcmMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

//        Toast.makeText(this, "In message received", Toast.LENGTH_SHORT).show();
        Log.d("In message received","any thing");

        int requestID = (int) System.currentTimeMillis();
        SharedPreferences sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);

        String title = remoteMessage.getNotification().getTitle();
        String body = remoteMessage.getNotification().getBody();
        String msg="";
        try {
            JSONObject obj = new JSONObject(body);
            String id = obj.getString("user_id");
            Log.d("obj -- ", obj.toString());
            if(sp.getString(ConstantsLocal.USER_ID,"").equalsIgnoreCase(id)){
                msg = obj.getString("msg");
                Bundle bundle = new Bundle();
                bundle.putString("upid", obj.getString("upid"));
                Intent intent = new Intent(this, Completed.class).putExtras(bundle);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, requestID, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
                NotificationCompat.Builder nb = new NotificationCompat.Builder(this);
                nb.setContentTitle(title);
                nb.setContentText(msg);
                nb.setSmallIcon(R.mipmap.ic_launcher);
                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                nb.setSound(alarmSound);
                nb.setAutoCancel(true);
                nb.setContentIntent(pendingIntent);


                SharedPreferences.Editor editor = sp.edit();
                editor.putString(ConstantsLocal.REQUEST_MODE, "completed");
                editor.commit();

                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.notify(0, nb.build());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
