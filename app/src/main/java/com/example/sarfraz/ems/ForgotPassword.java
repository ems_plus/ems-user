package com.example.sarfraz.ems;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class ForgotPassword extends AppCompatActivity {

    EditText new_user_password, new_user_confirm_password;
    Button new_password_confirm_button;
    String password, confirm_password, phone;
    ProgressClass pc;
    AlertClass ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        Bundle bundle = getIntent().getExtras();
        phone = bundle.getString("PHONE_NUMBER");

        init();
    }

    public void init(){
        new_user_password = (EditText) findViewById(R.id.new_user_password);
        new_user_confirm_password = (EditText) findViewById(R.id.new_user_confirm_password);
        new_password_confirm_button = (Button) findViewById(R.id.new_password_confirm_button);

        pc = new ProgressClass(this);
        ac = new AlertClass(this);

        new_password_confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                password = new_user_password.getText().toString().trim();
                confirm_password = new_user_confirm_password.getText().toString().trim();

                if(password.isEmpty()){
                    new_user_password.setError("This field is required...");
                    return;
                }

                if(confirm_password.isEmpty()){
                    new_user_confirm_password.setError("This field is required...");
                    return;
                }

                if(password.equals(confirm_password)){
                    final OkHttpClient client = new OkHttpClient();

                    pc = new ProgressClass(ForgotPassword.this);
                    pc.startProgress("Please wait. Updating your password...");

                    //getting transaction history

                    final Map<String, String> param = new HashMap<String, String>();
                    param.put("PHONE", phone);
                    param.put("PASSWORD", password);
                    param.put("TABLE", "users");

                    //ac.showConfirmDialog(param.toString());

                    new AsyncTask<String, String, String>(){

                        String response;
                        JSONArray jsonArray = null;

                        @Override
                        protected String doInBackground(String... params) {
                            try {
                                response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                                Log.i("response --  -- ", response);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return response;
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            if(s != null) {

                                if (pc != null && pc.isActive())
                                    pc.stopProgress();
                                //ac.showConfirmDialog(s);
                                if (s.equals("SUCCESS")) {
                                    makeToast("Yay. Password updated successfully!");
                                    startActivity(new Intent(ForgotPassword.this, LoginActivity.class));
                                } else {
                                    makeToast("Oops. There was some error! Please try again later.");
                                    startActivity(new Intent(ForgotPassword.this, LoginActivity.class));
                                }
                            }
                        }
                    }.execute(ConstantsLocal.FORGOT_PASSWORD_URL);
                } else {
                    makeToast("Passwords don't match!");
                }
            }
        });
    }

    public void makeToast(String query){
        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
    }
}
