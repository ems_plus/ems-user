package com.example.sarfraz.ems;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class Gender extends AppCompatActivity {

    private ImageView back;
    private ImageView next;
    private ImageView male;
    private ImageView female;
    private String gender;
    private TextView gender_text;
    private Typeface raleway;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender);

        init();

    }

    private void init() {

        back = (ImageView) findViewById(R.id.back_gender);
        next = (ImageView) findViewById(R.id.next_gender);
        male = (ImageView) findViewById(R.id.male);
        female = (ImageView) findViewById(R.id.female);
        gender_text = (TextView) findViewById(R.id.gender_text);

        raleway = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        gender_text.setTypeface(raleway);

        gender = "";

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(gender.isEmpty()){
                    return;
                } else {

                    //register(gender);
                    Bundle bundle = getIntent().getExtras();
                    bundle.putString("GENDER", gender);

                    startActivity(new Intent(Gender.this, UserAgreement.class).putExtras(bundle));
                }

            }
        });

        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gender = "MALE";
                male.setImageResource(R.drawable.male_coloured);
                female.setImageResource(R.drawable.male);

            }
        });

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gender = "FEMALE";
                female.setImageResource(R.drawable.female_coloured);
                male.setImageResource(R.drawable.female);

            }
        });

    }

}
