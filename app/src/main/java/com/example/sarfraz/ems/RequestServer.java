package com.example.sarfraz.ems;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by sarang on 26/3/17.
 */

public class RequestServer{
    //GET network request
    public static String GET(OkHttpClient client, HttpUrl url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    //POST network request
    public static String POST(OkHttpClient client, String url, RequestBody body) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    //Login request body
    // For post
    public static RequestBody LoginBody(Map<String,String> params) {


        FormBody.Builder formBody = new FormBody.Builder();
        for ( Map.Entry<String, String> entry : params.entrySet() ) {
            formBody.add(entry.getKey(), entry.getValue());
        }
        RequestBody requestBody = formBody.build();
        return  requestBody;


    }

    //For GET
    public static HttpUrl buildURL() {
        return new HttpUrl.Builder()
                .scheme("https") //http
                .host("www.somehostname.com")
                .addPathSegment("pathSegment")//adds "/pathSegment" at the end of hostname
                .addQueryParameter("param1", "value1") //add query parameters to the URL
                .addEncodedQueryParameter("encodedName", "encodedValue")//add encoded query parameters to the URL
                .build();
        /**
         * The return URL:
         *  https://www.somehostname.com/pathSegment?param1=value1&encodedName=encodedValue
         */
    }
}


// how to call

/*
private OkHttpClient client;

 client = new OkHttpClient();

* private void loadContent() {
    new AsyncTask<Void, Void, Void>() {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                response = ApiCall.GET(client, RequestBuilder.buildURL());
                //Parse the response string here
                Log.d("Response", response);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }.execute();
}

private void attemptLogin(String url) {
    new AsyncTask<String, Void, Void>() {
        @Override
        protected Void doInBackground(String... params) {

            try {
                response = ApiCall.POST(
                        client,
                        params[0],
                        RequestBuilder.LoginBody("username", "password","token"));

                Log.d("Response", response);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }.execute(url);
}*/
