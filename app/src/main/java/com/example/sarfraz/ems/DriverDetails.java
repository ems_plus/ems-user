package com.example.sarfraz.ems;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.instamojo.android.Instamojo;
import com.instamojo.android.activities.PaymentDetailsActivity;
import com.instamojo.android.callbacks.OrderRequestCallBack;
import com.instamojo.android.helpers.Constants;
import com.instamojo.android.models.Errors;
import com.instamojo.android.models.Order;
import com.instamojo.android.network.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Driver;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.OkHttpClient;

import static java.lang.Thread.sleep;

public class DriverDetails extends AppCompatActivity {

    ImageView back_driver_details;
    Button book_now_driver_details;
    Bundle bundle;
    TextView driver_name_textView, driver_phone_textView, driver_vehicle_name_textView, driver_vehicle_number_textView, loader_text;
    String services, payment_option, name, email, phone, user_id, driver_id, vehicle_selected, from_address, to_address, from_lat_lng, to_lat_lng;
    Double total_fare, distance;
    RelativeLayout loader_layout, driver_layout;
    SharedPreferences sp;
    OkHttpClient client;
    private ProgressDialog dialog;
    private AlertClass ac;
    private ProgressClass pc;
    GifImageView loader_image;
    Typeface raleway;
    Thread thread;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_details);
        Instamojo.initialize(this);
        //Instamojo.setLogLevel(Log.DEBUG);
        Instamojo.setBaseUrl("https://api.instamojo.com/");

        bundle = getIntent().getExtras();

        total_fare = bundle.getDouble("TOTAL_FARE");
        services = bundle.getString("SERVICES");
        //payment_option = bundle.getString("PAYMENT");
        vehicle_selected = bundle.getString("VEHICLE_TYPE");
        distance = bundle.getDouble("DISTANCE");
        from_address = bundle.getString("FROM_ADDRESS");
        to_address = bundle.getString("TO_ADDRESS");
        from_lat_lng = bundle.getString("FROM_LAT_LNG");
        to_lat_lng = bundle.getString("TO_LAT_LNG");

        //showToast("from_address - " + from_address + ", to_address - " + to_address);

         init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void init() {

        back_driver_details = (ImageView) findViewById(R.id.back_driver_details);
        book_now_driver_details = (Button) findViewById(R.id.book_now_driver_details);
        loader_layout = (RelativeLayout) findViewById(R.id.loader_layout);
        driver_layout = (RelativeLayout) findViewById(R.id.driver_layout);
        sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        ac = new AlertClass(this);
        pc = new ProgressClass(this);

        driver_name_textView = (TextView) findViewById(R.id.driver_name);
        driver_phone_textView = (TextView) findViewById(R.id.driver_phone);
        driver_vehicle_name_textView = (TextView) findViewById(R.id.driver_vehicle_name);
        driver_vehicle_number_textView = (TextView) findViewById(R.id.driver_vehicle_number);
        loader_text = (TextView) findViewById(R.id.loader_text);

        name = sp.getString(ConstantsLocal.USER_NAME, "");
        email = sp.getString(ConstantsLocal.USER_EMAIL, "");
        phone = sp.getString(ConstantsLocal.USER_PHONE, "");
        user_id = sp.getString(ConstantsLocal.USER_ID, "");
        client = new OkHttpClient();

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage("please wait...");
        dialog.setCancelable(false);

        //font

        raleway = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        loader_text.setTypeface(raleway);

        loader_image = (GifImageView) findViewById(R.id.loader_image);
        loader_image.setGifImageResource(R.drawable.find_driver);

        driver_layout.setVisibility(View.GONE);
        getDriverDetails();


        back_driver_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        book_now_driver_details.setVisibility(View.GONE);
    }



    private void getDriverDetails() {

        final Map<String, String> param = new HashMap<String, String>();
        //purpose = "Ambulance";

        param.put("AMOUNT", total_fare.toString());
        param.put("SERVICES", services);
        //param.put("PAYMENT_OPTION", payment_option);
        param.put("VEHICLE_SELECTED", vehicle_selected);
        param.put("DISTANCE", distance.toString());
        param.put("FROM_ADDRESS", from_address);
        param.put("FROM_LAT_LNG", from_lat_lng);

        Log.i("param - ", param.toString());

        final String[] driver_name = new String[1];
        final String[] driver_phone = new String[1];
        final String[] ambulance_model = new String[1];
        final String[] vehicle_number = new String[1];
        final String[] driver_email = new String[1];

        //dialog.show();

        pc = new ProgressClass(this);
        pc.startProgress("Please wait...");
        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if(s != null) {
                    //dialog.dismiss();
                    if (pc != null && pc.isActive())
                        pc.stopProgress();
                    Log.i("server res ", s);
                    //ac.showOkDialog(s, "");

                    try {
                        JSONObject parentObject = new JSONObject(s);
                        JSONObject obj = parentObject.getJSONObject("data");

                        String status = obj.get("status").toString();


                        if (status.equals("success")) {

                            driver_id = obj.get("id").toString();
                            driver_name[0] = obj.get("name").toString();
                            driver_phone[0] = obj.get("phone").toString();
                            ambulance_model[0] = obj.get("ambulance_model").toString();
                            vehicle_number[0] = obj.get("vehicle_number").toString();
                            driver_email[0] = obj.get("email").toString();

                            //ac.showOkDialog(driver_id[0] + ", " + driver_name[0] + ", " + driver_phone[0] + ", " + ambulance_model[0] + ", " + vehicle_number[0] + ", " +driver_email[0], "");

                            driver_name_textView.setText(driver_name[0]);
                            driver_phone_textView.setText(driver_phone[0]);
                            driver_vehicle_name_textView.setText(ambulance_model[0]);
                            driver_vehicle_number_textView.setText(vehicle_number[0]);

                            //book_now_driver_details.setVisibility(View.VISIBLE);

                            //loader for finding driver
                            loader_layout.setVisibility(View.VISIBLE);

                            //loader_layout.setVisibility(View.GONE);
                            //driver_layout.setVisibility(View.VISIBLE);

                            progressBar = (ProgressBar) findViewById(R.id.progressBar);
                            progressBar.setProgress(0);

                            thread = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        sleep(1500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    cashPayment();
                                }
                            });
                            thread.start();


                        } else {

                            if (obj.getString("error").equalsIgnoreCase("NO_DRIVER_FOUND")) {
                                final String okResponse = " ";
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DriverDetails.this, R.style.MyDialogTheme);
                                alertDialogBuilder.setTitle("NO DRIVER FOUND");
                                alertDialogBuilder.setMessage("For emergency,\ncall - 8765696172, \nemail - support@emsplus.co.in");
                                alertDialogBuilder.setPositiveButton("close",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                if (!okResponse.isEmpty())
                                                    finish();
                                            }
                                        });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            } else {
                                showToast("There was some error - " + obj.getString("error").toLowerCase());
                                if (bundle == null) {
                                    bundle = new Bundle();
                                }
                                finish();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute(ConstantsLocal.GET_DRIVER_DETAILS_URL);

    }

    private void cashPayment() {

        //showToast("cash payment");
        final Map<String, String> param = new HashMap<String, String>();
        //purpose = "Ambulance";

        //param.put("AMOUNT", total_fare.toString());
        param.put("PURPOSE", vehicle_selected);
        param.put("USER_ID", user_id);
        param.put("SERVICES", services);
        //param.put("PAYMENT_OPTION", payment_option);
        param.put("VEHICLE_SELECTED", vehicle_selected);
        param.put("DISTANCE", distance.toString());
        param.put("FROM_ADDRESS", from_address);
        param.put("TO_ADDRESS", to_address);
        param.put("FROM_LAT_LNG", from_lat_lng);
        param.put("TO_LAT_LNG", to_lat_lng);
        param.put("DRIVER_ID", driver_id);

        Log.i("param - ", param.toString());
        final String[] pid = {""};
        final String[] payment_status = { "" };

        //dialog.show();
        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //dialog.dismiss();

                if(s != null) {

                    Log.i("server res ", s);

                    try {
                        JSONObject parentObject = new JSONObject(s);
                        JSONObject obj = parentObject.getJSONObject("data");

                        String status = obj.get("status").toString();


                        if (status.equals("success")) {

                            payment_status[0] = obj.getString("status");
                            pid[0] = obj.getString("order_id");

                            showToast("Requested successfully!");
                            bundle.putString("status", payment_status[0]);
                            bundle.putString("pid", pid[0]);
                            bundle.putString("driver_name", driver_name_textView.getText().toString());
                            bundle.putString("driver_phone", driver_phone_textView.getText().toString());
                            bundle.putString("driver_vehicle_number", driver_vehicle_number_textView.getText().toString());
                            bundle.putString("driver_vehicle_name", driver_vehicle_name_textView.getText().toString());
                            bundle.putString("from_address", from_address);
                            bundle.putString("to_address", to_address);

                            startActivity(new Intent(DriverDetails.this, PaymentStatus.class).putExtras(bundle));

                        } else {

                            showToast("There was some error!");
                            if (bundle == null) {
                                bundle = new Bundle();
                            }
                            bundle.putString("status", "failure");

                            startActivity(new Intent(DriverDetails.this, Home.class).putExtras(bundle));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute(ConstantsLocal.CASH_PAYMENT_URL);

    }

    private void fetchTokenAndTransactionID() {

        final String[] access_token = new String[1];
        final String[] token_type = new String[1];

        final Map<String, String> param = new HashMap<String, String>();
        param.put("CREATE", "TRUE");

        dialog.show();
        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if(s != null) {
                    Log.i("server res ", s);

                    try {
                        JSONObject parentObject = new JSONObject(s);
                        JSONObject obj = parentObject.getJSONObject("data");

                        String status = obj.get("status").toString();

                        if (status.equals("success")) {

                            access_token[0] = obj.get("access_token").toString();
                            token_type[0] = obj.get("token_type").toString();

                            //showToast("access_token - " + access_token[0]);
                            createPaymentRequest(access_token[0], token_type[0]);

                        } else {
                            dialog.dismiss();
                            showToast("There was some error!");
                            return;

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute(ConstantsLocal.GENERATE_TOKEN_URL);
    }

    private void createPaymentRequest(String access_token, String token_type) {

        final String[] order_id = new String[1];
        final String[] a_token = new String[1];
        final String[] prid = new String[1];
        //purpose = "Ambulance";

        if(!dialog.isShowing()) dialog.show();

        final Map<String, String> param = new HashMap<String, String>();
        param.put("ACCESS_TOKEN", access_token);
        param.put("TOKEN_TYPE", token_type);
        param.put("NAME", name);
        param.put("EMAIL", email);
        param.put("PHONE", phone);
        param.put("AMOUNT", total_fare.toString());
        param.put("PURPOSE", vehicle_selected);
        param.put("USER_ID", user_id);
        param.put("DRIVER_ID", driver_id);
        param.put("SERVICES", services);
        param.put("PAYMENT_OPTION", payment_option);
        param.put("VEHICLE_SELECTED", vehicle_selected);
        param.put("DISTANCE", distance.toString());
        param.put("FROM_ADDRESS", from_address);
        param.put("TO_ADDRESS", to_address);
        param.put("FROM_LAT_LNG", from_lat_lng);
        param.put("TO_LAT_LNG", to_lat_lng);

        //showToast(from_lat_lng + to_lat_lng);

        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if(s != null) {

                    Log.i("server res ", s);
                    //showToast(s);

                    try {
                        JSONObject parentObject = new JSONObject(s);
                        JSONObject obj = parentObject.getJSONObject("data");

                        String status = obj.get("status").toString();

                        if (status.equals("success")) {

                            a_token[0] = obj.get("access_token").toString();
                            order_id[0] = obj.get("order_id").toString();
                            prid[0] = obj.get("prid").toString();

                            //showToast("access_token - " + a_token[0] + ", order_id - " + order_id[0] + ", prid - " + prid[0]);

                            fetchOrder(a_token[0], order_id[0]);
                            //showToast("prid - " + prid[0]);


                        } else {

                            dialog.dismiss();
                            showToast("There was some error!");
                            return;

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        dialog.dismiss();
                        ac.showOkDialog("JSON PARSER ERROR!", "");
                    }
                }
            }
        }.execute(ConstantsLocal.PAYMENT_REQUEST_URL);
    }

    private void fetchOrder(String access_token, String order_id) {

        // Good time to show dialog
        if(!dialog.isShowing()) dialog.show();

        Request request = new Request(access_token, order_id, new OrderRequestCallBack() {
            @Override
            public void onFinish(final Order order, final Exception error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        if (error != null) {
                            if (error instanceof Errors.ConnectionError) {
                                showToast("No internet connection");
                            } else if (error instanceof Errors.ServerError) {
                                showToast("Server Error. Try again");
                            } else if (error instanceof Errors.AuthenticationError) {
                                showToast("Access token is invalid or expired. Please Update the token!!");
                            } else {
                                showToast(error.toString());
                            }
                            return;
                        }

                        //showToast(order.toString());
                        Log.i("order - ", order.toString());
                        order.setWebhook("http://emsplus.co.in/android/payment/webhook.php");
                        startPreCreatedUI(order);
                    }
                });

            }
        });
        request.execute();

    }

    private void startPreCreatedUI(Order order){
        //Using Pre created UI
        Intent intent = new Intent(getBaseContext(), PaymentDetailsActivity.class);
        intent.putExtra(Constants.ORDER, order);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE && data != null) {
            String orderID = data.getStringExtra(Constants.ORDER_ID);
            String transactionID = data.getStringExtra(Constants.TRANSACTION_ID);
            String paymentID = data.getStringExtra(Constants.PAYMENT_ID);

            // Check transactionID, orderID, and orderID for null before using them to check the Payment status.
            if (orderID != null && transactionID != null && paymentID != null) {
                //Check for Payment status with Order ID or Transaction ID
                Log.i("order status - ", orderID + ", " + transactionID + ", " + paymentID);
                checkPaymentStatus(orderID, transactionID, paymentID);
            } else {

                showToast("Oops!! Payment was cancelled");
                if(bundle == null){
                    bundle = new Bundle();
                }
                bundle.putString("status", "cancelled");
                startActivity(new Intent(DriverDetails.this, PaymentStatus.class).putExtras(bundle));
            }


        }
    }

    private void checkPaymentStatus(String orderID, String transactionID, String paymentID) {

        showToast("onActivityResult");

        //GET STATUS
        final Map<String, String> param = new HashMap<String, String>();
        param.put("PR_ID", orderID);

        final String[] payment_status = new String[1];
        final String[] pid = new String[1];
        final String[] prid = new String[1];

        dialog.show();

        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if(s != null) {

                    Log.i("server res ", s);
                    dialog.dismiss();
                    try {
                        JSONObject parentObject = new JSONObject(s);
                        JSONObject obj = parentObject.getJSONObject("data");

                        String status = obj.get("status").toString();

                        if (status.equals("success")) {

                            payment_status[0] = obj.get("payment_status").toString();
                            pid[0] = obj.get("pid").toString();
                            prid[0] = obj.get("prid").toString();

                            Log.i("status", payment_status[0] + ", " + pid[0] + ", " + prid[0]);
                            bundle.putString("status", payment_status[0]);
                            bundle.putString("pid", pid[0]);
                            startActivity(new Intent(DriverDetails.this, PaymentStatus.class).putExtras(bundle));

                        } else {

                            showToast("There was some error - " + obj.get("error").toString());
                            if (bundle == null) {
                                bundle = new Bundle();
                            }
                            bundle.putString("status", payment_status[0]);
                            startActivity(new Intent(DriverDetails.this, PaymentStatus.class).putExtras(bundle));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute(ConstantsLocal.GET_STATUS_URL);

    }

    public void showToast(String query){
        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
    }
}
