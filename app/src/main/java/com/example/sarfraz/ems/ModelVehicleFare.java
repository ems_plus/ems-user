package com.example.sarfraz.ems;

/**
 * Created by sarangkartikey on 15/08/17.
 */

public class ModelVehicleFare {
    public String DB_ID, NAME, BF_10, BF_20, BF_40, BF_250, BF_250_PLUS, PK_10, PK_20, PK_40, PK_250, PK_250_PLUS, PM_10, PM_20, PM_40, PM_250, PM_250_PLUS;

    public String getDB_ID() {
        return DB_ID;
    }

    public void setDB_ID(String DB_ID) {
        this.DB_ID = DB_ID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getBF_10() {
        return BF_10;
    }

    public void setBF_10(String BF_10) {
        this.BF_10 = BF_10;
    }

    public String getBF_20() {
        return BF_20;
    }

    public void setBF_20(String BF_20) {
        this.BF_20 = BF_20;
    }

    public String getBF_40() {
        return BF_40;
    }

    public void setBF_40(String BF_40) {
        this.BF_40 = BF_40;
    }

    public String getBF_250() {
        return BF_250;
    }

    public void setBF_250(String BF_250) {
        this.BF_250 = BF_250;
    }

    public String getBF_250_PLUS() {
        return BF_250_PLUS;
    }

    public void setBF_250_PLUS(String BF_250_PLUS) {
        this.BF_250_PLUS = BF_250_PLUS;
    }

    public String getPK_10() {
        return PK_10;
    }

    public void setPK_10(String PK_10) {
        this.PK_10 = PK_10;
    }

    public String getPK_20() {
        return PK_20;
    }

    public void setPK_20(String PK_20) {
        this.PK_20 = PK_20;
    }

    public String getPK_40() {
        return PK_40;
    }

    public void setPK_40(String PK_40) {
        this.PK_40 = PK_40;
    }

    public String getPK_250() {
        return PK_250;
    }

    public void setPK_250(String PK_250) {
        this.PK_250 = PK_250;
    }

    public String getPK_250_PLUS() {
        return PK_250_PLUS;
    }

    public void setPK_250_PLUS(String PK_250_PLUS) {
        this.PK_250_PLUS = PK_250_PLUS;
    }

    public String getPM_10() {
        return PM_10;
    }

    public void setPM_10(String PM_10) {
        this.PM_10 = PM_10;
    }

    public String getPM_20() {
        return PM_20;
    }

    public void setPM_20(String PM_20) {
        this.PM_20 = PM_20;
    }

    public String getPM_40() {
        return PM_40;
    }

    public void setPM_40(String PM_40) {
        this.PM_40 = PM_40;
    }

    public String getPM_250() {
        return PM_250;
    }

    public void setPM_250(String PM_250) {
        this.PM_250 = PM_250;
    }

    public String getPM_250_PLUS() {
        return PM_250_PLUS;
    }

    public void setPM_250_PLUS(String PM_250_PLUS) {
        this.PM_250_PLUS = PM_250_PLUS;
    }
}
