package com.example.sarfraz.ems;

/**
 * Created by sarangkartikey on 16/08/17.
 */

public class ModelCalculatedFare {
    float service_fare, total_fare, duration_fare;

    public float getService_fare() {
        return service_fare;
    }

    public void setService_fare(float service_fare) {
        this.service_fare = service_fare;
    }

    public float getTotal_fare() {
        return total_fare;
    }

    public void setTotal_fare(float total_fare) {
        this.total_fare = total_fare;
    }

    public float getDuration_fare() {
        return duration_fare;
    }

    public void setDuration_fare(float duration_fare) {
        this.duration_fare = duration_fare;
    }
}
