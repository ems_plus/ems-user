package com.example.sarfraz.ems;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Splash extends AppCompatActivity {
    private PrefManager prefManager;
    private TextView splash_text;
    private Typeface Raleway;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
       // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.splash);
        splash_text = (TextView) findViewById(R.id.splash_text);
        Raleway = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        splash_text.setTypeface(Raleway);
        final SharedPreferences sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        
        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(3000);
                    prefManager = new PrefManager(Splash.this);
                    if(prefManager.isFirstTimeLaunch()){
                        Intent intent = new Intent(Splash.this, WelcomeActivity.class);
                        startActivity(intent);
                    }else {
                        if(MyFunctions.isUserLoggedIn(Splash.this)){
                            Intent intent = new Intent(getApplicationContext(), Home.class);
                            startActivity(intent);
                        }else{
                            if(sp.getString(ConstantsLocal.REQUEST_MODE, "").equalsIgnoreCase("requested")){
                                startActivity(new Intent(Splash.this, Completed.class));
                            } else {
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                            }
                        }
                    }
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }
}
