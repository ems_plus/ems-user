package com.example.sarfraz.ems;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by sarangkartikey on 15/08/17.
 */

public class EmsDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "EMS_DATABASE.db";
    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_VEHICLE_FARE_TABLE = "DATABASE_VEHICLE_FARE_TABLE";
    public static final String DATABASE_SERVICE_FARE_TABLE = "DATABASE_SERVICE_FARE_TABLE";

    public static final String ID1 = "id1";
    public static final String ID2 = "id2";
    public static final String DEVICE_ID = "device_id";
    public static final String DB_ID = "id";
    public static final String NAME = "name";
    public static final String BF_10 = "BF_10";
    public static final String BF_20 = "BF_20";
    public static final String BF_40 = "BF_40";
    public static final String BF_250 = "BF_250";
    public static final String BF_250_PLUS = "BF_250_PLUS";
    public static final String PK_10 = "PK_10";
    public static final String PK_20 = "PK_20";
    public static final String PK_40 = "PK_40";
    public static final String PK_250 = "PK_250";
    public static final String PK_250_PLUS = "PK_250_PLUS";
    public static final String PM_10 = "PM_10";
    public static final String PM_20 = "PM_20";
    public static final String PM_40 = "PM_40";
    public static final String PM_250 = "PM_250";
    public static final String PM_250_PLUS = "PM_250_PLUS";

    public static final String SERVICE_RATE = "rate";

    Context context;

    public EmsDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query_1 = "CREATE TABLE " + DATABASE_VEHICLE_FARE_TABLE + " (" + ID1 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DEVICE_ID + " TEXT NOT NULL," + DB_ID + " TEXT NOT NULL," + NAME + " TEXT NOT NULL," + BF_10 + " TEXT NOT NULL," + BF_20 + " TEXT NOT NULL," + BF_40 + " TEXT NOT NULL," + BF_250 + " TEXT NOT NULL," + BF_250_PLUS + " TEXT NOT NULL," + PK_10 + " TEXT NOT NULL," + PK_20 + " TEXT NOT NULL," + PK_40 + " TEXT NOT NULL," + PK_250 + " TEXT NOT NULL," + PK_250_PLUS + " TEXT NOT NULL," + PM_10 + " TEXT NOT NULL," + PM_20 + " TEXT NOT NULL," + PM_40 + " TEXT NOT NULL," + PM_250 + " TEXT NOT NULL," + PM_250_PLUS + " TEXT NOT NULL );";
        String query_2 = "CREATE TABLE " + DATABASE_SERVICE_FARE_TABLE + " (" + ID2 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DEVICE_ID + " TEXT NOT NULL, " + DB_ID + " TEXT NOT NULL," + NAME + " TEXT NOT NULL," + SERVICE_RATE + " TEXT NOT NULL );";
        db.execSQL(query_1);
        db.execSQL(query_2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_VEHICLE_FARE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_SERVICE_FARE_TABLE);
    }

    public void insertVehicleFare(String device_id, String db_id, String name, String bf_10, String bf_20, String bf_40, String bf_250, String bf_250_plus, String pk_10, String pk_20, String pk_40, String pk_250, String pk_250_plus, String pm_10, String pm_20, String pm_40, String pm_250, String pm_250_plus){

        SQLiteDatabase sql_db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DEVICE_ID, device_id);
        values.put(DB_ID, db_id);
        values.put(NAME, name);
        values.put(BF_10, bf_10);
        values.put(BF_20, bf_20);
        values.put(BF_40, bf_40);
        values.put(BF_250, bf_250);
        values.put(BF_250_PLUS, bf_250_plus);
        values.put(PK_10, pk_10);
        values.put(PK_20, pk_20);
        values.put(PK_40, pk_40);
        values.put(PK_250, pk_250);
        values.put(PK_250_PLUS, pk_250_plus);
        values.put(PM_10, pm_10);
        values.put(PM_20, pm_20);
        values.put(PM_40, pm_40);
        values.put(PM_250, pm_250);
        values.put(PM_250_PLUS, pm_250_plus);

        if(isExists(DATABASE_VEHICLE_FARE_TABLE, NAME, name)){
            long bc = sql_db.update(DATABASE_VEHICLE_FARE_TABLE, values, NAME + " = '" + name  + "'", null);
            Log.d("updateVehicleFare", values.toString());
            //Log.d("bc - - - - - -", bc+"");
        } else {
            long bc = sql_db.insert(DATABASE_VEHICLE_FARE_TABLE, null, values);
            Log.d("insertVehicleFare", values.toString());
            //Log.d("bc - - - - - -", bc+"");
        }
    }

    public void insertServiceFare(String device_id, String db_id, String name, String rate){

        SQLiteDatabase sql_db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DEVICE_ID, device_id);
        values.put(DB_ID, db_id);
        values.put(NAME, name);
        values.put(SERVICE_RATE, rate);

        if(isExists(DATABASE_SERVICE_FARE_TABLE, NAME, name)){
            sql_db.update(DATABASE_SERVICE_FARE_TABLE, values, NAME + " = '" + name  + "'", null);
            Log.d("updateServiceFare", values.toString());
        } else {
            sql_db.insert(DATABASE_SERVICE_FARE_TABLE, null, values);
            Log.d("insertServiceFare", values.toString());
        }
    }

    public boolean isExists(String table_name, String db_field, String field_value){
        SQLiteDatabase sql_db = this.getWritableDatabase();
        //String query = "SELECT * FROM " + table_name + " WHERE " + db_field + " =? " + field_value;
        Cursor cursor = sql_db.rawQuery("SELECT * FROM " + table_name + " WHERE " + db_field + " =? ",new String[]{field_value}, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }

        cursor.close();
        return true;
    }

    public ModelVehicleFare getVehicleFare(String vehicle_name){

        ModelVehicleFare mvf = new ModelVehicleFare();

        SQLiteDatabase sql_db = getWritableDatabase();
        //String[] projection = {DB_ID, BF_10};
        Cursor cursor = sql_db.query(DATABASE_VEHICLE_FARE_TABLE, null, NAME + " = ?", new String[]{vehicle_name}, null, null, null);
        //Cursor cursor = sql_db.query();


        if(cursor.getCount() <= 0){
            cursor.close();
            return null;
        }

        cursor.moveToFirst();

        mvf.setDB_ID(cursor.getString(cursor.getColumnIndex(DB_ID)));
        mvf.setBF_10(cursor.getString(cursor.getColumnIndex(BF_10)));
        mvf.setBF_20(cursor.getString(cursor.getColumnIndex(BF_20)));
        mvf.setBF_40(cursor.getString(cursor.getColumnIndex(BF_40)));
        mvf.setBF_250(cursor.getString(cursor.getColumnIndex(BF_250)));
        mvf.setBF_250_PLUS(cursor.getString(cursor.getColumnIndex(BF_250_PLUS)));
        mvf.setPK_10(cursor.getString(cursor.getColumnIndex(PK_10)));
        mvf.setPK_20(cursor.getString(cursor.getColumnIndex(PK_20)));
        mvf.setPK_40(cursor.getString(cursor.getColumnIndex(PK_40)));
        mvf.setPK_250(cursor.getString(cursor.getColumnIndex(PK_250)));
        mvf.setPK_250_PLUS(cursor.getString(cursor.getColumnIndex(PK_250_PLUS)));
        mvf.setPM_10(cursor.getString(cursor.getColumnIndex(PM_10)));
        mvf.setPM_20(cursor.getString(cursor.getColumnIndex(PM_20)));
        mvf.setPM_40(cursor.getString(cursor.getColumnIndex(PM_40)));
        mvf.setPM_250(cursor.getString(cursor.getColumnIndex(PM_250)));
        mvf.setPM_250_PLUS(cursor.getString(cursor.getColumnIndex(PM_250_PLUS)));
        cursor.close();

        return mvf;
    }

    public String getServiceFare(String service_name){

        Log.d("service_name", service_name);
        SQLiteDatabase sql_db = this.getWritableDatabase();
        Cursor cursor = sql_db.query(DATABASE_SERVICE_FARE_TABLE, null, NAME + " =? ", new String[]{service_name}, null, null, null);
        //Cursor cursor = sql_db.rawQuery("SELECT rate FROM DATABASE_SERVICE_FARE_TABLE WHERE name = '" + service_name + "'", null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return null;
        }

        cursor.moveToFirst();

        String result = cursor.getString(cursor.getColumnIndex(SERVICE_RATE));

        return result;
    }
}
