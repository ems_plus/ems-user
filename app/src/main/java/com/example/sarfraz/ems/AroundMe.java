package com.example.sarfraz.ems;


import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Typeface;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.OkHttpClient;

/**
 * <p/>
 * Calculate the position of the sticky header view according to the
 * position of the first item in the ListView. When the first item is already
 * reached to top, you don't need to position the sticky header view.
 *
 * @author Nilanchala
 */
public class AroundMe extends ActionBarActivity {

    private LinearLayout stickyView;
    private ListView listView;
    private View heroImageView;
    JSONArray by_dist, by_grp;
    JSONObject disease_data;
    Toolbar toolbar;
    ImageView back_aroundme;

    OkHttpClient client;
    private View stickyViewSpacer;
    TextView disease_header;
    TextView distance_header, stickyView1, stickyView2;

    RelativeLayout tr1, tr2, topLayout;

    private int MAX_ROWS = 20;

    Bundle bundle;
    String lat, lng, myId;

    ProgressClass pc;

    Typeface raleway_light, raleway_medium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_around_me);

        /* Initialise list view, hero image, and sticky view */
        listView = (ListView) findViewById(R.id.listView);
        topLayout = (RelativeLayout) findViewById(R.id.topLayout);
        stickyView = (LinearLayout) findViewById(R.id.stickyView);
        stickyView1 = (TextView) findViewById(R.id.stickyView1);
        stickyView2 = (TextView) findViewById(R.id.stickyView2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        back_aroundme = (ImageView) findViewById(R.id.back_aroundme);

        back_aroundme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        raleway_light = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        raleway_medium = Typeface.createFromAsset(getAssets(), "Raleway-Medium.ttf");

        stickyView1.setTypeface(raleway_medium);
        stickyView2.setTypeface(raleway_medium);


        tr1 = (RelativeLayout)findViewById(R.id.top_layout_trangle1);
        tr2 = (RelativeLayout)findViewById(R.id.top_layout_trangle2);

        if(getIntent().getExtras()!=null){
            bundle = getIntent().getExtras();
            lat = bundle.getString("lat");
            lng = bundle.getString("long");
        }
        client = new OkHttpClient();

        /* Inflate list header layout */
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listHeader = inflater.inflate(R.layout.around_me_list_header, null);
        stickyViewSpacer = listHeader.findViewById(R.id.stickyViewPlaceholder);

        /* Add list view header*/
        listView.addHeaderView(listHeader);

        /* Handle list View scroll events */
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                /* Check if the first item is already reached to top.*/
                if (listView.getFirstVisiblePosition() == 0) {
                    View firstChild = listView.getChildAt(0);
                    int topY = 0;
                    if (firstChild != null) {
                        topY = firstChild.getTop();
                    }

                    int heroTopY = stickyViewSpacer.getTop();
                    stickyView.setY(Math.max(0, heroTopY + topY));

                    /* Set the image to scroll half of the amount that of ListView */
                    topLayout.setY(topY * 0.5f);
                }
            }
        });

        //get data


         /* Populate the ListView with sample data */
       /* ArrayList<ModelCard> modelList = new ArrayList<>();
        for (int i = 0; i < MAX_ROWS; i++) {
            ModelCard mc = new ModelCard();
            mc.setDisease_name("disease" + i);
            mc.setDes(i+" Cases found ");
            modelList.add(mc);
        }*/

       /* AroundRecyclerViewAdapter adapter = new AroundRecyclerViewAdapter(AroundMe.this, modelList, raleway_light, raleway_medium);
        listView.setAdapter(adapter);*/

        pc = new ProgressClass(this);
        pc.startProgress("Fetching Data...");
        new getAroundMeData(lat,lng, MyFunctions.getUserId(this)).execute();

        disease_header = (TextView)findViewById(R.id.stickyView1);
        distance_header = (TextView)findViewById(R.id.stickyView2);

        disease_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tr1.setVisibility(View.VISIBLE);
                tr2.setVisibility(View.GONE);



                ArrayList<ModelCard> list = new ArrayList<>();
                for(int i=0;i<by_grp.length();i++){

                    JSONObject ob = null;
                    try {
                        ob = by_grp.getJSONObject(i);
                        disease_data = ob.getJSONObject("disease_data");

                        ModelCard mc =  new ModelCard();
                        mc.setDisease_name(disease_data.getString("name"));
                        mc.setDes(ob.getString("count"));
                        mc.setType("disease");
                        mc.setData(disease_data);
                        list.add(mc);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                AroundRecyclerViewAdapter adapter = new AroundRecyclerViewAdapter(AroundMe.this, list, raleway_light, raleway_medium);
                listView.setAdapter(adapter);
            }
        });

        distance_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tr1.setVisibility(View.GONE);
                tr2.setVisibility(View.VISIBLE);
                ArrayList<ModelCard> list = new ArrayList<>();
                for(int i=0;i<by_dist.length();i++){

                    JSONObject ob = null;
                    try {
                        ob = by_dist.getJSONObject(i);
                        disease_data = ob.getJSONObject("disease_data");
                        ModelCard mc =  new ModelCard();
                        mc.setDisease_name(disease_data.getString("name"));
                        mc.setDes(String.format("%.02f km", Float.parseFloat(ob.getString("distance"))));
                        mc.setType("distance");
                        mc.setData(disease_data);
                        list.add(mc);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                AroundRecyclerViewAdapter adapter = new AroundRecyclerViewAdapter(AroundMe.this, list, raleway_light, raleway_medium);
                listView.setAdapter(adapter);
            }
        });



    }

    class getAroundMeData extends AsyncTask<String, String, String>{

        String lat, lng, id, resp;
        String url = ConstantsLocal.HOME_URL+"around_me.php";
        JSONObject disease_data;
        String dname, count;



        public getAroundMeData(String lat, String lng, String id){
            this.lat = lat;
            this.lng = lng;
            this.id = id;
        }

        @Override
        protected String doInBackground(String... params) {

            Map<String, String > map = new HashMap<String, String>();
            map.put("lat",lat);
            map.put("lng",lng);
            map.put("user_id",id);

            try {
                 resp = RequestServer.POST(client, url, RequestServer.LoginBody(map));
            } catch (IOException e) {
                e.printStackTrace();
            }


            return resp;
        }

        @Override
        protected void onPostExecute(String s) {

           /// Log.d("data :  ", s);

            if(pc!=null && pc.isActive()) pc.stopProgress();
            try {
                JSONObject obj = new JSONObject(s);

                by_dist = obj.getJSONArray("by_distance");
                by_grp = obj.getJSONArray("by_group");

               /* ArrayList<ModelCard> list = new ArrayList<>();
                for(int i=0;i<by_grp.length();i++){

                    JSONObject ob = by_grp.getJSONObject(i);
                    disease_data = ob.getJSONObject("disease_data");

                    ModelCard mc =  new ModelCard();
                    mc.setDisease_name(disease_data.getString("name"));
                    mc.setDes("Cases Found : "+ ob.getString("count"));
                    list.add(mc);
                }



                AroundRecyclerViewAdapter adapter = new AroundRecyclerViewAdapter(AroundMe.this, list, raleway_light, raleway_medium);
                listView.setAdapter(adapter);*/
               disease_header.performClick();





            } catch (JSONException e) {
                e.printStackTrace();
            }

            super.onPostExecute(s);
        }



    }

    public void onBackPressed(){
        finish();
    }
}



class AroundRecyclerViewAdapter extends BaseAdapter{

    View v;
    Holder holder;
    Activity activity;
    ArrayList<ModelCard> modelList;
    Typeface light, medium;


    public AroundRecyclerViewAdapter(Activity activity, ArrayList<ModelCard> arrayModel, Typeface light, Typeface medium){
        this.activity = activity;
        this.modelList = arrayModel;
        this.light = light;
        this.medium = medium;
    }

    public void showDetailsPopUp(JSONObject data){

        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity);
        View dialog = activity.getLayoutInflater().inflate(R.layout.dialog_my_feedback, null);
        bottomSheetDialog.setContentView(dialog);


        TextView disease_name = (TextView) dialog.findViewById(R.id.txt_disease_name);
        TextView disease_cat = (TextView) dialog.findViewById(R.id.txt_disease_category);
        TextView disease_desc = (TextView)dialog.findViewById(R.id.txt_disease_desc);
        TextView disease_symp = (TextView)dialog.findViewById(R.id.txt_symptoms);
        TextView disease_prec = (TextView)dialog.findViewById(R.id.txt_disease_prec);

        TextView disease_cat_head = (TextView) dialog.findViewById(R.id.txt_disease_category_head);
        TextView disease_desc_head = (TextView)dialog.findViewById(R.id.txt_disease_desc_head);
        TextView disease_symp_head = (TextView)dialog.findViewById(R.id.txt_symptoms_head);
        TextView disease_prec_head = (TextView)dialog.findViewById(R.id.txt_disease_prec_head);

        disease_name.setTypeface(medium);
        disease_cat.setTypeface(light);
        disease_desc.setTypeface(light);
        disease_symp.setTypeface(light);
        disease_prec.setTypeface(light);
        disease_cat_head.setTypeface(medium);
        disease_desc_head.setTypeface(medium);
        disease_symp_head.setTypeface(medium);
        disease_prec_head.setTypeface(medium);

        try {
            disease_name.setText(data.getString("name"));
            disease_cat.setText(data.getString("category"));
            disease_desc.setText(data.getString("description"));
            disease_symp.setText(data.getString("symtoms"));
            disease_prec.setText(data.getString("precautions"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        bottomSheetDialog.show();

    }


    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        v = convertView;
        if(v==null){
            LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=inflater.inflate(R.layout.around_me_list_item, null);
            holder = new Holder();
            holder.txt_disease = (TextView) v.findViewById(R.id.txt_disease_name);
            holder.txt_desc = (TextView)v.findViewById(R.id.txt_disease_desc);
            holder.txt_head = (TextView)v.findViewById(R.id.head);
            holder.icon = (ImageView)v.findViewById(R.id.icon);
            holder.bars = (ImageView)v.findViewById(R.id.bars);

            holder.bars.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showDetailsPopUp(modelList.get(position).getData());

                }
            });

            holder.txt_disease.setTypeface(light);
            holder.txt_head.setTypeface(medium);

            holder.txt_disease.setText(modelList.get(position).getDisease_name());
            holder.txt_desc.setText(modelList.get(position).getDes());

            if(modelList.get(position).getType().equalsIgnoreCase("distance")){
                holder.txt_head.setVisibility(View.GONE);
                holder.icon.setImageResource(R.drawable.frommarker);
            } else {
                holder.txt_head.setText("CASES FOUND");
                holder.icon.setImageResource(R.drawable.hospital);
            }

            v.setTag(holder);


        }else{
            holder = (Holder)v.getTag();
        }

        return v;
    }
    static class Holder
    {
        TextView txt_disease;
       TextView txt_desc;
       TextView txt_head;
       ImageView icon;
       ImageView bars;
    }
}

class ModelCard{
    String disease_name;
    String des;
    String type;
    JSONObject data;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {

        return type;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public JSONObject getData() {
        return data;

    }

    public String getDisease_name() {
        return disease_name;
    }

    public void setDisease_name(String disease_name) {
        this.disease_name = disease_name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}