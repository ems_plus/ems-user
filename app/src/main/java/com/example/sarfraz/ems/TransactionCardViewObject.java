package com.example.sarfraz.ems;

/**
 * Created by Suruchi on 6/30/17.
 */

public class TransactionCardViewObject {
    private String vehicle_type;
    private String total_fare;
    private String status;
    private String order_id;
    private String time;

    TransactionCardViewObject (String text1, String text2, String text3, String text4, String text5){
        vehicle_type = text1;
        total_fare = text2;
        status = text3;
        order_id = text4;
        time = text5;
    }

    public String getVehicleType() {
        return vehicle_type;
    }

    public void setVehicleType(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public String getTotalFare() {
        return total_fare;
    }

    public void setTotalFare(String total_fare) {
        this.total_fare = total_fare;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getOrderId(){
        return order_id;
    }

    public void setOrderId(String order_id){
        this.order_id = order_id;
    }

    public String getTime(){
        return time;
    }

    public void setTime(String time){
        this.time = time;
    }

}
