package com.example.sarfraz.ems;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class Completed extends AppCompatActivity {

    ImageView completed_icon;
    TextView completed_text;
    String response;
    Button home_btn;
    RatingBar rating_bar;
    boolean rated;
    Bundle bundle;
    String rat;
    OkHttpClient client;
    EditText feedback_text;
    ProgressClass pc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed);

        bundle = getIntent().getExtras();
        init();
    }

    public void init(){
        completed_icon = (ImageView) findViewById(R.id.completed_icon);
        completed_text = (TextView) findViewById(R.id.completed_text);
        home_btn = (Button) findViewById(R.id.home_btn);
        rating_bar = (RatingBar) findViewById(R.id.rating_bar);
        client = new OkHttpClient();
        feedback_text = (EditText) findViewById(R.id.feedback_text);
        pc = new ProgressClass(this);



        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(rated){
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("upid", bundle.getString("upid"));
                    map.put("rating", rat);

                    String feedback  = feedback_text.getText().toString();
                    map.put("feedback", feedback);


                    pc = new ProgressClass(Completed.this);
                    pc.startProgress("Please wait...");
                    new sendRating(map).execute();
                }

                finish();
                Intent intent = new Intent(Completed.this, Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        rating_bar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                rated = true;
                rat = rating + "";
                home_btn.setText("Goto home");

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Completed.this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public class sendRating extends AsyncTask<String, String, String> {

        String res, st;
        Map<String, String> param;
        String url = ConstantsLocal.HOME_URL+"send_rating.php";

        public sendRating(Map<String, String> param){
            this.param = param;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                res = RequestServer.POST(client, url, RequestServer.LoginBody(param));
            } catch (IOException e) {
                e.printStackTrace();
            }


            Log.d("accept response - - - ", res);

            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            if(pc != null && pc.isActive())
                 pc.stopProgress();


            if(s != null)
            try {
                Log.d("res", s);
                JSONObject obj = new JSONObject(s);

                String r = obj.getString("status");
                if(r.equalsIgnoreCase("success")) {
                    MyFunctions.showToast(Completed.this ,"Thanks for rating!");
                    Intent intent = new Intent(Completed.this, Home.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    MyFunctions.showToast(Completed.this, "There was some error!");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }


}
