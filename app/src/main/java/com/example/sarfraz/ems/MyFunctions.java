package com.example.sarfraz.ems;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.widget.Toast;

/**
 * Created by sarfraz on 23/3/17.
 */

public class MyFunctions {
    public  static  SharedPreferences sp;
    public static boolean isUserLoggedIn(Context context){
        if(context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getBoolean(ConstantsLocal.IS_USER_LOGIN, false)) return true;
        return false;
    }

    public static void setLoginType(Context context, String type){
        sp = context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(ConstantsLocal.LOGIN_TYPE_NAME, type);
        editor.commit();
    }
    public static String getLoginType(Context context){
        return context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(ConstantsLocal.LOGIN_TYPE_NAME,"");
    }
    public static void clearPreferenceFile(Context context){
        sp = context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
    }
    public static void saveDataToPreference(Context context, String id, String name, String phone, String email, String pic, String gender) {
        SharedPreferences sp = context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(ConstantsLocal.IS_USER_LOGIN, true);
        editor.putString(ConstantsLocal.USER_ID, id);
        editor.putString(ConstantsLocal.USER_NAME, name);
        editor.putString(ConstantsLocal.USER_EMAIL, email);
        editor.putString(ConstantsLocal.USER_PHONE, phone);
        editor.putString(ConstantsLocal.USER_PIC, pic);
        editor.putString(ConstantsLocal.USER_GENDER, gender);
        editor.commit();
    }
    public  static void showToast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    public static boolean isInternetAvailable(Context context) {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return connected;
    }

    public static boolean checkService(String services, String service){
        if(services.indexOf(service) != -1){
            return true;
        } else {
            return false;
        }
    }

    public static String getUserId(Context context){
        return  context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(ConstantsLocal.USER_ID,"");
    }
    public static String getUserName(Context context){
        return  context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(ConstantsLocal.USER_NAME,"");
    }
    public static String getUserPhone(Context context){
        return  context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(ConstantsLocal.USER_PHONE,"");
    }

    public static String getUserEmail(Context context){
        return  context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(ConstantsLocal.USER_EMAIL,"");
    }
    public static void updateUserPic(Context context, String pic){
        SharedPreferences sp = context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(ConstantsLocal.USER_PIC, pic);
        editor.commit();
    }
    public static String getUserPic(Context context){
        return  context.getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(ConstantsLocal.USER_PIC,"");
    }
    public static Bitmap Decodebitmap(String myImageData)
    {
        byte[] imageAsBytes = Base64.decode(myImageData.getBytes(),Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }
}
