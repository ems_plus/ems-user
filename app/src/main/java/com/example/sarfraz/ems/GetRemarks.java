package com.example.sarfraz.ems;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class GetRemarks extends AppCompatActivity {

    TextView head, text, point1_text, point2_text, point3_text, remarks_text, my_journey_text, number;
    Typeface light, medium, bold;
    OkHttpClient client;
    SharedPreferences sp;
    ImageView cross_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_remarks);

        init();
    }

    private void init(){
        head = (TextView) findViewById(R.id.head);
        text = (TextView) findViewById(R.id.text);
        point1_text = (TextView) findViewById(R.id.point1_text);
        point2_text = (TextView) findViewById(R.id.point2_text);
        point3_text = (TextView) findViewById(R.id.point3_text);
        remarks_text = (TextView) findViewById(R.id.remarks_text);
        my_journey_text = (TextView) findViewById(R.id.my_journey_text);
        number = (TextView) findViewById(R.id.number);

        light = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        medium = Typeface.createFromAsset(getAssets(), "Raleway-Medium.ttf");
        bold = Typeface.createFromAsset(getAssets(), "Raleway-SemiBold.ttf");

        head.setTypeface(bold);
        text.setTypeface(medium);
        point1_text.setTypeface(light);
        point2_text.setTypeface(light);
        point3_text.setTypeface(light);
        remarks_text.setTypeface(medium);
        number.setTypeface(bold);
        my_journey_text.setTypeface(medium);

        client = new OkHttpClient();
        sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, MODE_PRIVATE);

        new getRemarks().execute();

        my_journey_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GetRemarks.this, Home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

    class getRemarks extends AsyncTask<String, String, String> {

        String resp, user_id;
        String url = ConstantsLocal.HOME_URL+"get_remarks.php";



        public getRemarks(){
            this.user_id = sp.getString(ConstantsLocal.USER_ID, "");
        }

        @Override
        protected String doInBackground(String... params) {

            Map<String, String > map = new HashMap<String, String>();
            map.put("user_id",user_id);
            Log.d("map -- ", map.toString());


            try {
                resp = RequestServer.POST(client, url, RequestServer.LoginBody(map));
            } catch (IOException e) {
                e.printStackTrace();
            }


            return resp;
        }

        @Override
        protected void onPostExecute(String s) {

            try {
                JSONObject obj = new JSONObject(s);

                if(obj.getString("status").equalsIgnoreCase("success")){
                    number.setText(obj.getString("num_rows"));
                } else {
                    MyFunctions.showToast(GetRemarks.this, "error occurred");
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            super.onPostExecute(s);
        }



    }
}
