package com.example.sarfraz.ems;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class UserAgreement extends AppCompatActivity {

    private CheckBox agree_checkbox;
    private Button proceed_button;
    private OkHttpClient client;
    private ProgressClass pc;
    private AlertClass ac;
    private TextView agreement_head, license_granting_head, license_granting_text, restriction_head, restriction_text, infringement_head, infringement_text;
    private Typeface raleway;
    private WebView webView;
    private ScrollView scrollView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_agreement);

        init();
    }

    private void init() {

        client = new OkHttpClient();
        pc = new ProgressClass(this);
        ac = new AlertClass(this);

        agree_checkbox = (CheckBox) findViewById(R.id.agree_checkbox);
        proceed_button = (Button) findViewById(R.id.proceed_button);
        proceed_button.setEnabled(false);

        agreement_head = (TextView) findViewById(R.id.agreement_head);
        license_granting_head = (TextView) findViewById(R.id.license_granting_head);
        license_granting_text = (TextView) findViewById(R.id.license_granting_text);
        restriction_head = (TextView) findViewById(R.id.restriction_head);
        restriction_text = (TextView) findViewById(R.id.restriction_text);
        infringement_head = (TextView) findViewById(R.id.infringement_head);
        infringement_text = (TextView) findViewById(R.id.infringement_text);

        raleway = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        agreement_head.setTypeface(raleway);
        license_granting_text.setTypeface(raleway);
        license_granting_head.setTypeface(raleway);
        restriction_text.setTypeface(raleway);
        restriction_head.setTypeface(raleway);
        infringement_text.setTypeface(raleway);
        infringement_head.setTypeface(raleway);
        agree_checkbox.setTypeface(raleway);
        proceed_button.setTypeface(raleway);

        agree_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(agree_checkbox.isChecked()){

                    proceed_button.setEnabled(true);
                    proceed_button.setBackgroundResource(R.color.colorPrimary);

                } else {

                    proceed_button.setEnabled(false);
                    proceed_button.setBackgroundResource(R.color.disabled);

                }

            }
        });

        //webview

        scrollView2 = (ScrollView) findViewById(R.id.scrollView2);
        agree_checkbox.setVisibility(View.GONE);
        proceed_button.setVisibility(View.GONE);

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(ConstantsLocal.TERMS_AND_COND_LINK);

        pc = new ProgressClass(this);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                super.onPageStarted(view, url, favicon);
                // textView.setVisibility(View.VISIBLE);

                pc = new ProgressClass(UserAgreement.this);
                pc.startProgress("Loading...");

            }

            @Override
            public void onPageFinished(WebView view, String url) {

                super.onPageFinished(view, url);
                if(pc != null && pc.isActive())
                pc.stopProgress();
                agree_checkbox.setVisibility(View.VISIBLE);
                proceed_button.setVisibility(View.VISIBLE);
            }



        });



        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = getIntent().getExtras();
                final String name = bundle.getString("NAME");
                final String phone = bundle.getString("PHONE_NUMBER");
                final String email = bundle.getString("EMAIL");
                final String password = bundle.getString("PASSWORD");
                final String gender = bundle.getString("GENDER");

                Log.i("bundle - ", name + phone + email + password + gender);

                //SENDITN DATA TO SERVER
                final Map<String, String> param = new HashMap<String, String>();

                param.put("name",name);
                param.put("phone",phone);
                param.put("email",email);
                param.put("password",password);
                param.put("gender", gender);


                pc = new ProgressClass(UserAgreement.this);

                pc.startProgress("Validating your phone number");

                new AsyncTask<String, String, String>(){

                    String response;

                    @Override
                    protected String doInBackground(String... params) {
                        try {
                            response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                            //Log.i("response --  -- ", response);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return response;
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);

                        if(s != null) {

                            Log.i("server res ", s);

                            if (s.equals("SUCCESS")) {

                                if (pc != null && pc.isActive())
                                    pc.stopProgress();
                                MyFunctions.showToast(UserAgreement.this, "Registered successfully!");
                                startActivity(new Intent(UserAgreement.this, LoginActivity.class));

                            } else {

                                if (pc != null && pc.isActive())
                                    pc.stopProgress();
                                ac.showOkDialog("There was some error!", "");
                                return;

                            }
                        }
                    }
                }.execute(ConstantsLocal.USER_REGISTER_URL);

            }
        });

    }
}
