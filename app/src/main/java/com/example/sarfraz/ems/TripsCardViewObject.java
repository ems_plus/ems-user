package com.example.sarfraz.ems;

/**
 * Created by Suruchi on 6/17/17.
 */



public class TripsCardViewObject {
    private String date;
    private String time;
    private String totalFare;
    private String vehicle_type;
    private String user_payment_id;
    private String purpose_class;

    public void setPurpose_class(String purpose_class) {
        this.purpose_class = purpose_class;
    }

    public String getPurpose_class() {

        return purpose_class;
    }

    TripsCardViewObject(String text1, String text2, String text3, String text4, String text5){
        date = text1;
        time = text2;
        totalFare = text3;
        vehicle_type = text4;
        user_payment_id = text5;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTotalFare(){
        return totalFare;
    }

    public void setTotalFare(String fare){
        this.totalFare  = fare;
    }

    public String getVehicleType(){
        return vehicle_type;
    }

    public void setVehicleType(String vehicle_type){
        this.vehicle_type = vehicle_type;
    }

    public String getUserPaymentId(){
        return user_payment_id;
    }

    public void setUserPaymentId(String user_payment_id){
        this.user_payment_id = user_payment_id;
    }

}


