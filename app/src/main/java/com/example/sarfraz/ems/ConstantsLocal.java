package com.example.sarfraz.ems;

import android.graphics.Typeface;

/**
 * Created by sarfraz on 23/3/17.
 */

public class ConstantsLocal {
    //SHARED PREFERENCE RELATED CONSTANTS
    public static String SHARED_PREFERENCE_NAME = "com.example.sarfraz.ems.SHARED_PREFERENCE_NAME";
    public static String IS_USER_LOGIN = "com.example.sarfraz.ems_IS_USER_LOGIN";
    public static String IS_FIRST_TIME_USER = "com.example.sarfraz.ems.IS_FIRST_TIME_USER";
    public static String LOGIN_TYPE_NAME = "com.example.sarfraz.ems.LOGIN_TYPE_NAME";
    public static String LOGIN_TYPE_NORMAL = "com.example.sarfraz.ems.LOGIN_TYPE_NORMAL";
    public static String LOGIN_TYPE_GOOGLE = "com.example.sarfraz.ems.LOGIN_TYPE_GOOGLE";
    public static String LOGIN_TYPE_FACEBOOK = "com.example.sarfraz.ems.LOGIN_TYPE_FACEBOOK";
    public static String REQUEST_MODE = "com.example.sarfraz.ems.REQUEST_MODE";

    //USER PROFILE
    public static String USER_ID = "com.example.sarfraz.ems.USER_ID";
    public static String USER_NAME = "com.example.sarfraz.ems.USER_NAME";
    public static String USER_EMAIL = "com.example.sarfraz.ems.USER_EMAIL";
    public static String USER_PHONE = "com.example.sarfraz.ems.USER_PHONE";
    public static String USER_PIC = "com.example.sarfraz.ems.USER_PIC";
    public static String USER_GENDER = "com.example.sarfraz.ems.USER_GENDER";


    //SERVER LINKS
    public static String HOME_URL = "http://emsplus.co.in/android/";
    public static String USER_REGISTER_URL = HOME_URL + "register_user.php";
    public static String LOGIN_URL = HOME_URL + "login.php";
    public static String SOCIAL_LOGIN_URL = HOME_URL + "social_login.php";
    public static String CHECK_PHONE_URL = HOME_URL + "check_phone.php";
    public static String BASE_FARE_URL = HOME_URL + "base_fare.php";
    public static String GENERATE_TOKEN_URL = HOME_URL + "payment/generate_token.php";
    public static String PAYMENT_REQUEST_URL = HOME_URL + "payment/payment_request.php";
    public static String GET_STATUS_URL = HOME_URL + "payment/get_status.php";
    public static String CASH_PAYMENT_URL = HOME_URL + "payment/cash_payment.php";
    public static String GET_TRANSACTION_HISTORY_URL = HOME_URL + "transaction_history.php";
    public static String GET_JOURNEY_URL = HOME_URL + "my_journey.php";
    public static String GET_TRIP_RECEIPT_URL = HOME_URL + "get_trip_receipt.php";
    public static String FORGOT_PASSWORD_URL = HOME_URL + "forgot_password.php";
    public static String GET_DRIVER_DETAILS_URL = HOME_URL + "get_driver_details.php";
    public static String CANCEL_ORDER = HOME_URL + "cancel_order.php";
    public static String GET_PAYMENT_DATA = HOME_URL + "payment/get_user_payment_details.php";
    public static String IMAGE_UPLOAD_URL = HOME_URL+"upload.php";
    public static String EVENT_UPLOAD_URL = HOME_URL + "event_upload.php";

    public static String TERMS_AND_COND_LINK = "http://emsplus.co.in/settings/terms.php";
    public static String ABOUT_US_LINK = "http://emsplus.co.in/settings/about.php";
    public static String COPYRIGHT_POLICY_LINK = "http://emsplus.co.in/settings/privacy.php";
    public static String HELP_LINK = "http://emsplus.co.in/settings/help.php";
    public static String CONTACT_US_LINK = HOME_URL+"contact_us.php";
    public static String UPDATE_PURPOSE_LINK = HOME_URL + "update_purpose.php";
    public static String GET_SUGGESTIONS_LINK = HOME_URL + "get_suggestions.php";


    //otp
    public static String OTP_URL = "control.msg91.com";
    public static String OTP_SEND_PATH = "api/sendotp.php";
    public static String OTP_VERIFY_PATH = "api/verifyRequestOTP.php";
    public static String OTP_RESEND_PATH = "";
    public static String OTP_ON_CALL_PATH = "";

    public static String OTP_API_KEY = "173082ArT0um9Kpxt59ad92e6";
    public static String OTP_DEFAULT_MSG = "##OTP## is your OTP for verification at EMS+. Do not share it with anyone";
    public static String OTP_SENDER_ID = "EMSPLS";
    public static String OTP_VALIDITY = "30";
    public static String OTP_LENGTH = "4";



}
