package com.example.sarfraz.ems;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class MyJourney extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "MyJourney";
    final ArrayList<TripsCardViewObject> results = new ArrayList<TripsCardViewObject>();
    Toolbar toolbar;
    Bundle bundle;
    String response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_journey);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        TextView my_journey_text = (TextView) findViewById(R.id.my_journey_text);
        RelativeLayout my_journey_layout = (RelativeLayout) findViewById(R.id.my_journey_layout);
        my_journey_layout.setVisibility(View.GONE);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bundle = getIntent().getExtras();
        response = bundle.getString("RESPONSE");

        final int[] num_rows = new int[1];
        final JSONArray[] data_array = new JSONArray[1];
        String user_payment_id, amount, start_time, date, vehicle_type;

        try {
            JSONObject parentObject = new JSONObject(response);
            JSONObject obj = parentObject.getJSONObject("data");

            String status = obj.get("status").toString();

            if(status.equals("success")){


                num_rows[0] = Integer.parseInt(obj.get("num_rows").toString());
                if(num_rows[0] > 0){
                    data_array[0] = obj.getJSONArray("data");
                    for(int i=0; i<data_array[0].length(); i++){
                        //showToast(data_array[0].getJSONObject(i).toString());
                        JSONObject newParentObject = new JSONObject(data_array[0].getJSONObject(i).toString());
                        Log.d("json -- ", newParentObject.toString());
                        user_payment_id = newParentObject.getString("user_payment_id").toString();
                        amount = newParentObject.getString("amount").toString();



                        String st1 = newParentObject.getString("start_time").toString();
                        String st = newParentObject.getString("date").toString();

                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
                        Date newDate = sdf.parse(st1);

                        SimpleDateFormat spf = new SimpleDateFormat("MMM dd, yyyy");
                        SimpleDateFormat spf1 = new SimpleDateFormat("hh:mm a");

                        start_time =spf1.format(newDate);
                        date = spf.format(newDate);

                        //date = newParentObject.getString("date").toString();
                        vehicle_type = newParentObject.getString("vehicle_type").toString();

                        TripsCardViewObject newObj =  new TripsCardViewObject(date, start_time, "Rs. " + amount, vehicle_type, user_payment_id);
                        newObj.setPurpose_class(newParentObject.getString("purpose"));

                        results.add(i, newObj);
                    }

                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    my_journey_layout.setVisibility(View.VISIBLE);
                    my_journey_text.setText("No journies found!");
                    showToast("no journey found!");
                }


            } else {

                mRecyclerView.setVisibility(View.GONE);
                my_journey_layout.setVisibility(View.VISIBLE);
                my_journey_text.setText("There was some error. " + obj.get("error").toString());
                //showToast(obj.get("error").toString());
                return;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        mAdapter = new TripsRecyclerViewAdapter(results);
        mRecyclerView.setAdapter(mAdapter);

        // Code to Add an item with default animation
        //((TripsRecyclerViewAdapter) mAdapter).addItem(obj, index);

        // Code to remove an item with default animation
        //((TripsRecyclerViewAdapter) mAdapter).deleteItem(index);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed(){
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((TripsRecyclerViewAdapter) mAdapter).setOnItemClickListener(new TripsRecyclerViewAdapter
                .MyClickListener() {
            @Override
            public void onItemClick(int position, View v){
                //showToast(" Clicked on Item " + results.get(position).getUserPaymentId());
                bundle.putString("USER_PAYMENT_ID", results.get(position).getUserPaymentId());
                bundle.putString("RESPONSE", response);
                startActivity(new Intent(MyJourney.this, TripReceipt.class).putExtras(bundle));
            }
        });
    }

    public void showToast(String query){
        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
    }
}