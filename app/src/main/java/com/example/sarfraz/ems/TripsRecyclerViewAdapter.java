package com.example.sarfraz.ems;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class TripsRecyclerViewAdapter extends RecyclerView
        .Adapter<TripsRecyclerViewAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "TripsRecyclerViewAdapter";
    private ArrayList<TripsCardViewObject> mDataset;
    private static MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView date;
        TextView time;
        TextView fare, vehicle_type;
        ImageView trip_card_view_icon;
        RelativeLayout remark_layout;

        public DataObjectHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.trip_card_view_date_text);
            time = (TextView) itemView.findViewById(R.id.trip_card_view_time_text);
            fare = (TextView) itemView.findViewById(R.id.trip_card_view_fare_text);
            vehicle_type = (TextView) itemView.findViewById(R.id.trip_card_view_vehicle_text);
            trip_card_view_icon = (ImageView) itemView.findViewById(R.id.trip_card_view_icon);
            remark_layout = (RelativeLayout) itemView.findViewById(R.id.remark_layout);

            //Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public TripsRecyclerViewAdapter(ArrayList<TripsCardViewObject> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trips_card_view, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.date.setText(mDataset.get(position).getDate());
        holder.time.setText(mDataset.get(position).getTime());
        holder.fare.setText(mDataset.get(position).getTotalFare());
        holder.vehicle_type.setText(mDataset.get(position).getVehicleType());
        holder.remark_layout.setVisibility(View.GONE);


        if(!mDataset.get(position).getPurpose_class().equalsIgnoreCase("")){
            holder.remark_layout.setVisibility(View.GONE);
        } else {
            //holder.remark_layout.setVisibility(View.VISIBLE);
        }

        switch(mDataset.get(position).getVehicleType()){
            case "TLS" : holder.trip_card_view_icon.setImageResource(R.drawable.ambulance_select_1); break;
            case "BLS" : holder.trip_card_view_icon.setImageResource(R.drawable.ambulance_select_2); break;
            case "ALS" : holder.trip_card_view_icon.setImageResource(R.drawable.ambulance_select_3); break;
            case "MORGUE" : holder.trip_card_view_icon.setImageResource(R.drawable.ambulance_select_4); break;
            case "EVENT" : holder.trip_card_view_icon.setImageResource(R.drawable.ambulance_select_5); break;
        }
    }

    public void addItem(TripsCardViewObject dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}