package com.example.sarfraz.ems;


import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

/**
 * Created by sarangkartikey on 14/08/17.
 */

public class FareCalculator extends AsyncTask<String, String, String>{

    public float services_fare , total_fare, duration_fair;;
    public static boolean dataFetched;
    float distance;
    String services, vehicle_type;
    long duration;
    OkHttpClient client;
    EmsDatabase db;
    ModelCalculatedFare mcf;
    String device_id;
    float   ventilator_fare, stretcher_fare, paramedic_fare, oxygen_fare_1, oxygen_fare_2, wheel_chair_fare, helper_fare, doctor_fare;
    float tls_bf_10, tls_bf_20, tls_bf_40, tls_bf_250, tls_bf_250_plus,  tls_pm_10, tls_pm_20, tls_pm_40, tls_pm_250, tls_pm_250_plus,  tls_pk_10, tls_pk_20, tls_pk_40, tls_pk_250, tls_pk_250_plus;
    float bls_bf_10, bls_bf_20, bls_bf_40, bls_bf_250, bls_bf_250_plus,  bls_pm_10, bls_pm_20, bls_pm_40, bls_pm_250, bls_pm_250_plus,  bls_pk_10, bls_pk_20, bls_pk_40, bls_pk_250, bls_pk_250_plus;
    float als_bf_10, als_bf_20, als_bf_40, als_bf_250, als_bf_250_plus,  als_pm_10, als_pm_20, als_pm_40, als_pm_250, als_pm_250_plus,  als_pk_10, als_pk_20, als_pk_40, als_pk_250, als_pk_250_plus;

    String response;
    JSONArray jsonArray = null;

    public FareCalculator(String d, String s, long duration, String vehicle_type, Context context) {
        this.vehicle_type = vehicle_type;
        this.distance = Float.parseFloat(d);
        //Log.d("this.d" , this.distance+"");
        this.services = s;
        this.duration = duration;               //second
        this.total_fare = 0.0f;
        this.services_fare = 0.0f;
        this.duration_fair = 0.0f;
        this.dataFetched = false;
        db = new EmsDatabase(context);
        device_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        mcf = new ModelCalculatedFare();
        calculateFare(vehicle_type);

    }

    public FareCalculator (Context context){
        db = new EmsDatabase(context);
        client = new OkHttpClient();
        device_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    public ModelCalculatedFare getFare(){
        return mcf;
    }


    // getting basic fare from server
    @Override
    protected String doInBackground(String... params) {
        final Map<String, String> param = new HashMap<String, String>();
        param.put("flag", "TRUE");

        try {
            response = RequestServer.POST(client, ConstantsLocal.BASE_FARE_URL, RequestServer.LoginBody(param));
            Log.i("response --  -- ", response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s != null) {

            //Log.i("server res ", s);

            try {
                JSONObject obj = new JSONObject(s);
                JSONObject data = obj.getJSONObject("data");

                String status = data.get("status").toString();

                if (status.equals("success")) {

                    JSONArray service_row = data.getJSONArray("service_row");
                    JSONObject tls_row = data.getJSONObject("tls_row");
                    JSONObject bls_row = data.getJSONObject("bls_row");
                    JSONObject als_row = data.getJSONObject("als_row");

                    //fare_flag = true;

                    JSONObject ven, str, para, oxy_1, oxy_2, wheel, helper, dr;
                    ven = service_row.getJSONObject(0);
                    str = service_row.getJSONObject(1);
                    para = service_row.getJSONObject(2);
                    oxy_1 = service_row.getJSONObject(3);
                    oxy_2 = service_row.getJSONObject(4);
                    wheel = service_row.getJSONObject(5);
                    helper = service_row.getJSONObject(6);
                    dr = service_row.getJSONObject(7);


                    // tls fare
                    tls_bf_10 = Float.parseFloat(tls_row.getString("BF_10"));
                    tls_bf_20 = Float.parseFloat(tls_row.getString("BF_20"));
                    tls_bf_40 = Float.parseFloat(tls_row.getString("BF_40"));
                    tls_bf_250 = Float.parseFloat(tls_row.getString("BF_250"));
                    tls_bf_250_plus = Float.parseFloat(tls_row.getString("BF_250_PLUS"));

                    tls_pk_10 = Float.parseFloat(tls_row.getString("PK_10"));
                    tls_pk_20 = Float.parseFloat(tls_row.getString("PK_20"));
                    tls_pk_40 = Float.parseFloat(tls_row.getString("PK_40"));
                    tls_pk_250 = Float.parseFloat(tls_row.getString("PK_250"));
                    tls_bf_250_plus = Float.parseFloat(tls_row.getString("PK_250_PLUS"));

                    tls_pm_10 = Float.parseFloat(tls_row.getString("PM_10"));
                    tls_pm_20 = Float.parseFloat(tls_row.getString("PM_20"));
                    tls_pm_40 = Float.parseFloat(tls_row.getString("PM_40"));
                    tls_pm_250 = Float.parseFloat(tls_row.getString("PM_250"));
                    tls_pm_250_plus = Float.parseFloat(tls_row.getString("PM_250_PLUS"));

                    db.insertVehicleFare(device_id, tls_row.getString("id"), "TLS", tls_bf_10 + "", tls_bf_20 + "", tls_bf_40 + "", tls_bf_250 + "", tls_bf_250_plus + "", tls_pk_10 + "", tls_pk_20 + "", tls_pk_40 + "", tls_pk_250 + "", tls_pk_250_plus + "", tls_pm_10 + "", tls_pm_20 + "", tls_pm_40 + "", tls_pm_250 + "", tls_pm_250_plus + "");


                    // bls fare
                    bls_bf_10 = Float.parseFloat(bls_row.getString("BF_10"));
                    bls_bf_20 = Float.parseFloat(bls_row.getString("BF_20"));
                    bls_bf_40 = Float.parseFloat(bls_row.getString("BF_40"));
                    bls_bf_250 = Float.parseFloat(bls_row.getString("BF_250"));
                    bls_bf_250_plus = Float.parseFloat(bls_row.getString("BF_250_PLUS"));

                    bls_pk_10 = Float.parseFloat(bls_row.getString("PK_10"));
                    bls_pk_20 = Float.parseFloat(bls_row.getString("PK_20"));
                    bls_pk_40 = Float.parseFloat(bls_row.getString("PK_40"));
                    bls_pk_250 = Float.parseFloat(bls_row.getString("PK_250"));
                    bls_bf_250_plus = Float.parseFloat(bls_row.getString("PK_250_PLUS"));

                    bls_pm_10 = Float.parseFloat(bls_row.getString("PM_10"));
                    bls_pm_20 = Float.parseFloat(bls_row.getString("PM_20"));
                    bls_pm_40 = Float.parseFloat(bls_row.getString("PM_40"));
                    bls_pm_250 = Float.parseFloat(bls_row.getString("PM_250"));
                    bls_pm_250_plus = Float.parseFloat(bls_row.getString("PM_250_PLUS"));

                    db.insertVehicleFare(device_id, bls_row.getString("id"), "BLS", bls_bf_10 + "", bls_bf_20 + "", bls_bf_40 + "", bls_bf_250 + "", bls_bf_250_plus + "", bls_pk_10 + "", bls_pk_20 + "", bls_pk_40 + "", bls_pk_250 + "", bls_pk_250_plus + "", bls_pm_10 + "", bls_pm_20 + "", bls_pm_40 + "", bls_pm_250 + "", bls_pm_250_plus + "");


                    // als fare
                    als_bf_10 = Float.parseFloat(als_row.getString("BF_10"));
                    als_bf_20 = Float.parseFloat(als_row.getString("BF_20"));
                    als_bf_40 = Float.parseFloat(als_row.getString("BF_40"));
                    als_bf_250 = Float.parseFloat(als_row.getString("BF_250"));
                    als_bf_250_plus = Float.parseFloat(als_row.getString("BF_250_PLUS"));

                    als_pk_10 = Float.parseFloat(als_row.getString("PK_10"));
                    als_pk_20 = Float.parseFloat(als_row.getString("PK_20"));
                    als_pk_40 = Float.parseFloat(als_row.getString("PK_40"));
                    als_pk_250 = Float.parseFloat(als_row.getString("PK_250"));
                    als_bf_250_plus = Float.parseFloat(als_row.getString("PK_250_PLUS"));

                    als_pm_10 = Float.parseFloat(als_row.getString("PM_10"));
                    als_pm_20 = Float.parseFloat(als_row.getString("PM_20"));
                    als_pm_40 = Float.parseFloat(als_row.getString("PM_40"));
                    als_pm_250 = Float.parseFloat(als_row.getString("PM_250"));
                    als_pm_250_plus = Float.parseFloat(als_row.getString("PM_250_PLUS"));

                    db.insertVehicleFare(device_id, als_row.getString("id"), "ALS", als_bf_10 + "", als_bf_20 + "", als_bf_40 + "", als_bf_250 + "", als_bf_250_plus + "", als_pk_10 + "", als_pk_20 + "", als_pk_40 + "", als_pk_250 + "", als_pk_250_plus + "", als_pm_10 + "", als_pm_20 + "", als_pm_40 + "", als_pm_250 + "", als_pm_250_plus + "");


                    db.insertServiceFare(device_id, ven.getString("id"), ven.getString("name"), ven.getString("rate"));
                    db.insertServiceFare(device_id, str.getString("id"), str.getString("name"), str.getString("rate"));
                    db.insertServiceFare(device_id, para.getString("id"), para.getString("name"), para.getString("rate"));
                    db.insertServiceFare(device_id, oxy_1.getString("id"), oxy_1.getString("name"), oxy_1.getString("rate"));
                    db.insertServiceFare(device_id, oxy_2.getString("id"), oxy_2.getString("name"), oxy_2.getString("rate"));
                    db.insertServiceFare(device_id, wheel.getString("id"), wheel.getString("name"), wheel.getString("rate"));
                    db.insertServiceFare(device_id, helper.getString("id"), helper.getString("name"), helper.getString("rate"));
                    db.insertServiceFare(device_id, dr.getString("id"), dr.getString("name"), dr.getString("rate"));

                    dataFetched = true;

                } else {

                    Log.d("There was some error!", "");
                    return;

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



    public void calculateFare(String vehicle_type) {


        //FOR SERVICES


        if (MyFunctions.checkService(services, "VENTILATOR")) {
            Log.d("VENTILATOR - ", db.getServiceFare("VENTILATOR"));
            total_fare += Float.parseFloat(db.getServiceFare("VENTILATOR"));
            services_fare += Float.parseFloat(db.getServiceFare("VENTILATOR"));
        }

        if(MyFunctions.checkService(services, "STRETCHER")){
            Log.d("STRETCHER - ", db.getServiceFare("STRETCHER"));
            total_fare += Float.parseFloat(db.getServiceFare("STRETCHER"));
            services_fare += Float.parseFloat(db.getServiceFare("STRETCHER"));
        }

        if(MyFunctions.checkService(services, "PARAMEDIC")){
            Log.d("PARAMEDIC - ", db.getServiceFare("PARAMEDIC"));
            total_fare += Float.parseFloat(db.getServiceFare("PARAMEDIC"));
            services_fare += Float.parseFloat(db.getServiceFare("PARAMEDIC"));
        }

        if(MyFunctions.checkService(services, "OXYGEN")){
            if(distance <=40 ){
                Log.d("OXYGEN_40 - ", db.getServiceFare("OXYGEN_40"));
                total_fare += Float.parseFloat(db.getServiceFare("OXYGEN_40"));
                services_fare += Float.parseFloat(db.getServiceFare("OXYGEN_40"));
            }else {
                Log.d("OXYGEN_40_PLUS - ", db.getServiceFare("OXYGEN_40_PLUS"));
                total_fare += Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));
                services_fare += Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));
            }
        }

        if(MyFunctions.checkService(services, "WHEELCHAIR")){
            Log.d("WHEELCHAIR - ", db.getServiceFare("WHEELCHAIR"));
            total_fare += Float.parseFloat(db.getServiceFare("WHEELCHAIR"));
            services_fare += Float.parseFloat(db.getServiceFare("WHEELCHAIR"));
        }

        if(MyFunctions.checkService(services, "HELPER")){
            Log.d("HELPER - ", db.getServiceFare("HELPER"));
            total_fare += Float.parseFloat(db.getServiceFare("HELPER"));
            services_fare += Float.parseFloat(db.getServiceFare("HELPER"));
        }

        if(MyFunctions.checkService(services, "DOCTOR")){
            Log.d("DOCTOR - ", db.getServiceFare("DOCTOR"));
            total_fare += Float.parseFloat(db.getServiceFare("DOCTOR"));
            services_fare += Float.parseFloat(db.getServiceFare("DOCTOR"));
        }

        mcf.setService_fare(services_fare);

        // vehicle fair calc

        if(vehicle_type.equalsIgnoreCase("TLS")){


            ModelVehicleFare mvf = db.getVehicleFare("TLS");

            if(distance <=10){
                Log.d("TLS_BF_10",mvf.getBF_10());
                total_fare += Float.parseFloat(mvf.getBF_10());
                //total_fare += Float.parseFloat(mvf.getPK_10()) * distance;
                duration_fair = Float.parseFloat(mvf.getPM_10()) * duration/60;
                total_fare += duration_fair;

            }else if(distance >10 && distance <18){
                total_fare += Float.parseFloat(mvf.getBF_10());
                total_fare += Float.parseFloat(mvf.getPK_20()) * (distance-10);
                duration_fair = (Float.parseFloat(mvf.getPM_20()) * duration/60);
                total_fare += duration_fair;

            }else if(distance>=18 && distance<=20){

                total_fare += Float.parseFloat(mvf.getBF_20());
                duration_fair = Float.parseFloat(mvf.getPM_20()) * duration/60;
                Log.d("in tls", duration + "  --- " + duration_fair);
                total_fare += duration_fair;

            } else if(distance >20 && distance < 38){

                total_fare += Float.parseFloat(mvf.getBF_20());
                total_fare += Float.parseFloat(mvf.getPK_40()) * (distance-20);
                duration_fair = Float.parseFloat(mvf.getPM_40()) * duration/60;
                total_fare += duration_fair;

            }else if(distance >= 38 && distance <= 40){
                total_fare += Float.parseFloat(mvf.getBF_40());
                duration_fair = Float.parseFloat(mvf.getPM_40()) * duration/60;
                total_fare += duration_fair;
            }

            else if(distance > 0 && distance <= 250){

                if(distance < 105){
                    total_fare += 210 * (Float.parseFloat(mvf.getBF_250()) + Float.parseFloat(mvf.getPK_250()));
                }else{
                    total_fare += distance * 2 * (Float.parseFloat(mvf.getBF_250()) + Float.parseFloat(mvf.getPK_250()));
                }


                total_fare += Float.parseFloat(mvf.getPM_250()) * duration/60.0f;
                duration_fair = Float.parseFloat(mvf.getPM_250()) * duration/60.0f;

            }else if(distance > 250){

                total_fare += distance * 2 * (Float.parseFloat(mvf.getBF_250_PLUS()) + Float.parseFloat(mvf.getPK_250_PLUS()));
                total_fare += Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60;
                duration_fair = Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60;

            }


        }else if(vehicle_type.equalsIgnoreCase("BLS")){



            /*if(distance <=10){

                Log.d("BLS_BF_10",mvf.getBF_10());
                total_fare += Float.parseFloat(mvf.getBF_10());
                total_fare += Float.parseFloat(mvf.getPK_10()) * distance;
                duration_fair = Float.parseFloat(mvf.getPM_10()) * duration/60.0f;
                total_fare += duration_fair;

            }else if(distance >10 && distance <=20){

                total_fare += Float.parseFloat(mvf.getBF_20());
                total_fare += Float.parseFloat(mvf.getPK_20()) * distance;
                duration_fair = Float.parseFloat(mvf.getPM_20()) * duration/60.0f;
                total_fare += duration_fair;

            }else if(distance >=20 && distance <= 40){

                total_fare += Float.parseFloat(mvf.getBF_40());
                total_fare += Float.parseFloat(mvf.getPK_40()) * distance;
                duration_fair = Float.parseFloat(mvf.getPM_40()) * duration/60.0f;
                total_fare += duration_fair;

            }else if(distance > 0 && distance <= 250){

                total_fare += distance * (Float.parseFloat(mvf.getBF_250()) + Float.parseFloat(mvf.getPK_250()));
                total_fare += Float.parseFloat(mvf.getPM_250()) * duration/60.0f;
                duration_fair = Float.parseFloat(mvf.getPM_250()) * duration/60.0f;

            }else if(distance > 250){

                total_fare += distance * (Float.parseFloat(mvf.getBF_250_PLUS()) + Float.parseFloat(mvf.getPK_250_PLUS()));
                total_fare += Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60.0f;
                duration_fair = Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60.0f;

            }*/

            ModelVehicleFare mvf = db.getVehicleFare("BLS");

            if(distance <=10){
                total_fare += Float.parseFloat(mvf.getBF_10());
                //total_fare += Float.parseFloat(mvf.getPK_10()) * distance;
                duration_fair = Float.parseFloat(mvf.getPM_10()) * duration/60;
                total_fare += duration_fair;

            }else if(distance >10 && distance <18){
                total_fare += Float.parseFloat(mvf.getBF_10());
                total_fare += Float.parseFloat(mvf.getPK_20()) * (distance-10);
                duration_fair = Float.parseFloat(mvf.getPM_20()) * duration/60;
                total_fare += duration_fair;

            }else if(distance>=18 && distance<=20){

                total_fare += Float.parseFloat(mvf.getBF_20());
                duration_fair = Float.parseFloat(mvf.getPM_20()) * duration/60;
                total_fare += duration_fair;

            } else if(distance >20 && distance < 38){

                total_fare += Float.parseFloat(mvf.getBF_20());
                total_fare += Float.parseFloat(mvf.getPK_40()) * (distance-20);
                duration_fair = Float.parseFloat(mvf.getPM_40()) * duration/60;
                total_fare += duration_fair;

            }else if(distance >= 38 && distance <= 40){
                total_fare += Float.parseFloat(mvf.getBF_40());
                duration_fair = Float.parseFloat(mvf.getPM_40()) * duration/60;
                total_fare += duration_fair;
            }
            else if(distance > 0 && distance <= 250){

                if(distance < 105){
                    total_fare += 210 * (Float.parseFloat(mvf.getBF_250()) + Float.parseFloat(mvf.getPK_250()));
                }else{
                    total_fare += distance * 2 * (Float.parseFloat(mvf.getBF_250()) + Float.parseFloat(mvf.getPK_250()));
                }


                total_fare += Float.parseFloat(mvf.getPM_250()) * duration/60.0f;
                duration_fair = Float.parseFloat(mvf.getPM_250()) * duration/60.0f;

            }else if(distance > 250){

                total_fare += distance * 2 * (Float.parseFloat(mvf.getBF_250_PLUS()) + Float.parseFloat(mvf.getPK_250_PLUS()));
                total_fare += Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60.0f;
                duration_fair = Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60.0f;

            }
           /* else if(distance > 0 && distance <= 250){

                total_fare += distance * (Float.parseFloat(mvf.getBF_250()) + Float.parseFloat(mvf.getPK_250()));
                total_fare += Float.parseFloat(mvf.getPM_250()) * duration/60;
                duration_fair = Float.parseFloat(mvf.getPM_250()) * duration/60;

            }else if(distance > 250){

                total_fare += distance * (Float.parseFloat(mvf.getBF_250_PLUS()) + Float.parseFloat(mvf.getPK_250_PLUS()));
                total_fare += Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60;
                duration_fair = Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60;

            }*/

        }else if(vehicle_type.equalsIgnoreCase("ALS")){

            ModelVehicleFare mvf = db.getVehicleFare("ALS");

            if(distance <=10){
                total_fare += Float.parseFloat(mvf.getBF_10());
                //total_fare += Float.parseFloat(mvf.getPK_10()) * distance;
                duration_fair = Float.parseFloat(mvf.getPM_10()) * duration/60;
                total_fare += duration_fair;

            }else if(distance >10 && distance <18){
                total_fare += Float.parseFloat(mvf.getBF_10());
                total_fare += Float.parseFloat(mvf.getPK_20()) * (distance-10);
                duration_fair = Float.parseFloat(mvf.getPM_20()) * duration/60;
                total_fare += duration_fair;

            }else if(distance>=18 && distance<=20){

                total_fare += Float.parseFloat(mvf.getBF_20());
                duration_fair = Float.parseFloat(mvf.getPM_20()) * duration/60;
                total_fare += duration_fair;

            } else if(distance >20 && distance < 38){

                total_fare += Float.parseFloat(mvf.getBF_20());
                total_fare += Float.parseFloat(mvf.getPK_40()) * (distance-20);
                duration_fair = Float.parseFloat(mvf.getPM_40()) * duration/60;
                total_fare += duration_fair;

            }else if(distance >= 38 && distance <= 40){
                total_fare += Float.parseFloat(mvf.getBF_40());
                duration_fair = Float.parseFloat(mvf.getPM_40()) * duration/60;
                total_fare += duration_fair;
            }
            else if(distance > 0 && distance <= 250){

                if(distance < 105){
                    total_fare += 210 * (Float.parseFloat(mvf.getBF_250()) + Float.parseFloat(mvf.getPK_250()));
                }else{
                    total_fare += distance * 2 * (Float.parseFloat(mvf.getBF_250()) + Float.parseFloat(mvf.getPK_250()));
                }


                total_fare += Float.parseFloat(mvf.getPM_250()) * duration/60.0f;
                duration_fair = Float.parseFloat(mvf.getPM_250()) * duration/60.0f;

            }else if(distance > 250){

                total_fare += distance * 2 * (Float.parseFloat(mvf.getBF_250_PLUS()) + Float.parseFloat(mvf.getPK_250_PLUS()));
                total_fare += Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60.0f;
                duration_fair = Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60.0f;

            }
           /* else if(distance > 0 && distance <= 250){

                total_fare += distance * (Float.parseFloat(mvf.getBF_250()) + Float.parseFloat(mvf.getPK_250()));
                total_fare += Float.parseFloat(mvf.getPM_250()) * duration/60;
                duration_fair = Float.parseFloat(mvf.getPM_250()) * duration/60;

            }else if(distance > 250){

                total_fare += distance * (Float.parseFloat(mvf.getBF_250_PLUS()) + Float.parseFloat(mvf.getPK_250_PLUS()));
                total_fare += Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60;
                duration_fair = Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60;

            }*/

            /*if(distance <=10){

                Log.d("ALS_BF_10",mvf.getBF_10());
                total_fare += Float.parseFloat(mvf.getBF_10());
                total_fare += Float.parseFloat(mvf.getPK_10()) * distance;
                duration_fair = Float.parseFloat(mvf.getPM_10()) * duration/60.0f;
                total_fare += duration_fair;

            }else if(distance >10 && distance <=20){

                total_fare += Float.parseFloat(mvf.getBF_20());
                total_fare += Float.parseFloat(mvf.getPK_20()) * distance;
                duration_fair = Float.parseFloat(mvf.getPM_20()) * duration/60.0f;
                total_fare += duration_fair;

            }else if(distance >=20 && distance <= 40){

                total_fare += Float.parseFloat(mvf.getBF_40());
                total_fare += Float.parseFloat(mvf.getPK_40()) * distance;
                duration_fair = Float.parseFloat(mvf.getPM_40()) * duration/60.0f;
                total_fare += duration_fair;

            }else if(distance > 0 && distance <= 250){

                total_fare += distance * (Float.parseFloat(mvf.getBF_250()) + Float.parseFloat(mvf.getPK_250()));
                total_fare += Float.parseFloat(mvf.getPM_250()) * duration/60.0f;
                duration_fair = Float.parseFloat(mvf.getPM_250()) * duration/60.0f;

            }else if(distance > 250){

                total_fare += distance * (Float.parseFloat(mvf.getBF_250_PLUS()) + Float.parseFloat(mvf.getPK_250_PLUS()));
                total_fare += Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60.0f;
                duration_fair = Float.parseFloat(mvf.getPM_250_PLUS()) * duration/60.0f;

            }*/



        }

        mcf.setDuration_fare(duration_fair);
        mcf.setTotal_fare(total_fare);

    }

}
