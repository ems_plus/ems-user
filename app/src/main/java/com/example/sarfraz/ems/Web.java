package com.example.sarfraz.ems;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class Web extends AppCompatActivity {

    String URL;
    WebView webView;
    ProgressClass pc;
    ImageView back_web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web);

        URL=getIntent().getStringExtra("url");
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(URL);
        back_web = (ImageView) findViewById(R.id.back_web);

        pc = new ProgressClass(this);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                super.onPageStarted(view, url, favicon);
                // textView.setVisibility(View.VISIBLE);

                pc = new ProgressClass(Web.this);
                pc.startProgress("Loading...");

            }

            @Override
            public void onPageFinished(WebView view, String url) {

                super.onPageFinished(view, url);

                if(pc != null && pc.isActive())
                pc.stopProgress();
            }



        });

        back_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
