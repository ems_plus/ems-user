package com.example.sarfraz.ems;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.OkHttpClient;

public class ContactUs extends AppCompatActivity {
    Toolbar toolbar;
    EditText nametv, phonetv, emailtv, msgtv;
    Button btn;
    TextView emailText;
    OkHttpClient client;
    ProgressClass pc;
    AlertClass ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();
    }

    private void init() {

        client = new OkHttpClient();
        pc = new ProgressClass(this);
        ac = new AlertClass(this);

        nametv = (EditText) findViewById(R.id.user_name);
        phonetv = (EditText) findViewById(R.id.user_phone);
        emailtv = (EditText) findViewById(R.id.user_email);
        msgtv = (EditText) findViewById(R.id.user_msg);
        btn = (Button) findViewById(R.id.send);

        nametv.setText(MyFunctions.getUserName(this));
        phonetv.setText(MyFunctions.getUserPhone(this));
        emailtv.setText(MyFunctions.getUserEmail(this));

        emailText = (TextView) findViewById(R.id.emailTv);

        String msg = "If you have any questions about us, please feel free to email us at support@emsplus.co.in. We will revert back to you within 24 hours.";
        SpannableString ss = new SpannableString(msg);
        ClickableSpan cs = new ClickableSpan() {
            @Override
            public void onClick(View view) {

                String email = "support@emsplus.co.in";
                String subject = "EMS+ Android App Support";

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] {email});
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                intent.setType("message/rfc822");
                Intent mailer = Intent.createChooser(intent, null);
                startActivity(mailer);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
            }
        };

        ss.setSpan(cs, msg.indexOf("support@emsplus.co.in"), msg.indexOf(". We"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(Color.BLUE),msg.indexOf("support@emsplus.co.in"), msg.indexOf(". We"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        emailText.setText(ss);
        emailText.setMovementMethod(LinkMovementMethod.getInstance());
        emailText.setHighlightColor(Color.TRANSPARENT);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!MyFunctions.isInternetAvailable(ContactUs.this)){

                    ac.showOkDialog("No Internet Connection!!","");
                }else if(nametv.getText().toString().trim().isEmpty()){
                    nametv.setError("This field can't be empty");
                }else if(phonetv.getText().toString().trim().isEmpty()){
                    phonetv.setError("This field can't be empty");
                }else if(emailtv.getText().toString().trim().isEmpty()){
                    emailtv.setError("This field can't be empty");
                }else if(msgtv.getText().toString().trim().isEmpty()){
                    msgtv.setError("This field can't be empty");
                }
                else{

                    pc = new ProgressClass(ContactUs.this);
                    pc.startProgress("Sending...");
                    new SendMessage(nametv.getText().toString().trim(), phonetv.getText().toString().trim(), emailtv.getText().toString().trim(), msgtv.getText().toString().trim()).execute();
                }
            }
        });
    }

    public class SendMessage extends AsyncTask<String, String, String>{

        String name,phone, email, msg, res="",url;
        public SendMessage(String name, String phone, String email, String msg){
            this.name = name ;
            this.phone = phone;
            this.email = email;
            this.msg = msg;
            this.url = ConstantsLocal.CONTACT_US_LINK;
        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String , String > map = new HashMap<String, String>();
            map.put("name",name);
            map.put("phone",phone);
            map.put("email",email);
            map.put("msg",msg);
            map.put("type","user");

            try {
                res = RequestServer.POST(client, url, RequestServer.LoginBody(map));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("res - - -- ",res);
            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s != null) {
                if (pc != null && pc.isActive()) pc.stopProgress();
                try {
                    JSONObject obj = new JSONObject(s);
                    if (obj.getString("status").equalsIgnoreCase("success")) {
                        MyFunctions.showToast(ContactUs.this, "Successfully Sent.");
                        finish();
                    } else {
                        ac.showOkDialog("Some Error Occurred, Please try again!", "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed(){
        finish();
    }
}
