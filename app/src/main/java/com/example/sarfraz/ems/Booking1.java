package com.example.sarfraz.ems;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class Booking1 extends AppCompatActivity implements View.OnClickListener{

    LinearLayout tmsView, bmsView, smsView, morgueView, eventView, service_layout, book_now_info, date_time_layout;
    Button bookNow_btn, datebtn, timebtn;
    private Calendar calendar;
    ImageView back_booking, info_booking;
    private int year, month, day,mHour, mMinute;
    String am_pm, vehicle_selected, from_address, to_address, from_lat_lng, to_lat_lng;
    CheckBox future_booking_checkbox, select_service_1, select_service_2, select_service_3 , select_service_4, select_service_5, select_service_6, select_service_7;
    boolean fare_flag;
    long duration;
    boolean[] services_selected = {false, false, false, false, false, false, false};
    TextView booking_fare;
    double final_distance, service_1 , service_2, service_3, service_4, service_5, service_6, service_7, tms_fare, bms_fare, sms_fare, morgue_fare, event_fare;
    ProgressClass pc;
    AlertClass ac;
    private OkHttpClient client;
    private RadioGroup payment_menu;
    FareCalculator fc;
    EmsDatabase db;
    float total_fare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking1);

        Bundle bundle = getIntent().getExtras();
        final_distance = bundle.getDouble("DISTANCE");
        final_distance = Double.parseDouble(new DecimalFormat("##.###").format(final_distance));
        from_address = bundle.getString("FROM_ADDRESS");
        to_address = bundle.getString("TO_ADDRESS");
        from_lat_lng = bundle.getString("FROM_LAT_LNG");
        to_lat_lng = bundle.getString("TO_LAT_LNG");
        duration = bundle.getLong("DURATION");
        Log.i("distance", final_distance+"");

        //getBasicFare();
        init();
    }

    private void getBasic(float distance, long duration, String vehicle_type){
        Log.d("database - ", distance + "  " + duration + "  " + vehicle_type);
        fc = new FareCalculator(distance+"", "", duration, vehicle_type, this);
        db = new EmsDatabase(this);

        total_fare = fc.getFare().getTotal_fare();

        fare_flag = true;

    }

    /*private void getBasicFare() {

        pc = new ProgressClass(this);
        ac = new AlertClass(this);
        client = new OkHttpClient();

        final Map<String, String> param = new HashMap<String, String>();
        param.put("flag", "TRUE");

        new AsyncTask<String, String, String>(){

            String response;
            JSONArray jsonArray = null;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    Log.i("response --  -- ", response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.i("server res ",s);

                try {
                    jsonArray = new JSONArray(s);
                    JSONObject obj = jsonArray.getJSONObject(0);

                    String status = obj.get("STATUS").toString();

                       if(status.equals("SUCCESS")){

                           fare_flag = true;
                           service_1 = Float.parseFloat(obj.get("VENTILLATOR").toString());
                           service_2 = Float.parseFloat(obj.get("STRETCHER").toString());
                           service_3 = Float.parseFloat(obj.get("PARAMEDIC").toString());
                           service_4 = Float.parseFloat(obj.get("OXYGEN").toString());
                           service_5 = Float.parseFloat(obj.get("WHEELCHAIR").toString());
                           service_6 = Float.parseFloat(obj.get("HELPER").toString());
                           service_7 = Float.parseFloat(obj.get("DOCTOR").toString());
                           tms_fare = Float.parseFloat(obj.get("TMS").toString());
                           bms_fare = Float.parseFloat(obj.get("BMS").toString());
                           sms_fare = Float.parseFloat(obj.get("ALS").toString());
                           morgue_fare = Float.parseFloat(obj.get("MORGUE").toString());
                           event_fare = Float.parseFloat(obj.get("EVENT").toString());

                        } else {

                            ac.showOkDialog("There was some error!", "");
                            return;

                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(ConstantsLocal.BASE_FARE_URL);
    }*/

    private void init() {
        fare_flag = true;
        tmsView = (LinearLayout) findViewById(R.id.tms_image_view);
        bmsView = (LinearLayout) findViewById(R.id.bms_image_view);
        smsView = (LinearLayout) findViewById(R.id.sms_image_view);
        morgueView = (LinearLayout) findViewById(R.id.morgue_image_view);
        eventView = (LinearLayout) findViewById(R.id.event_image_view);
        bookNow_btn = (Button) findViewById(R.id.book_now_btn);
        bookNow_btn.setTransformationMethod(null);
        future_booking_checkbox = (CheckBox) findViewById(R.id.future_booking_checkbox);
        date_time_layout = (LinearLayout) findViewById(R.id.date_time_layout);
        back_booking = (ImageView) findViewById(R.id.back_booking);
        info_booking = (ImageView) findViewById(R.id.info_booking);
        booking_fare = (TextView) findViewById(R.id.booking_fare);


        //payment_menu = (RadioGroup) findViewById(R.id.payment_menu);
        //RadioButton defaultCat = (RadioButton)findViewById(R.id.online_payment);
        //defaultCat.setChecked(true);


        select_service_1 = (CheckBox) findViewById(R.id.select_service_1);
        select_service_2 = (CheckBox) findViewById(R.id.select_service_2);
        select_service_3 = (CheckBox) findViewById(R.id.select_service_3);
        select_service_4 = (CheckBox) findViewById(R.id.select_service_4);
        select_service_5 = (CheckBox) findViewById(R.id.select_service_5);
        select_service_6 = (CheckBox) findViewById(R.id.select_service_6);
        select_service_7 = (CheckBox) findViewById(R.id.select_service_7);

        //time picker and date picker

        date_time_layout.setVisibility(View.GONE);
        datebtn = (Button) findViewById(R.id.datebtn);
        timebtn = (Button) findViewById(R.id.timebtn);

        datebtn.setOnClickListener(this);
        timebtn.setOnClickListener(this);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinute = calendar.get(Calendar.MINUTE);

        info_booking.setVisibility(View.GONE);

        back_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        info_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clearAllSelection();
                service_layout.setVisibility(View.GONE);
                bookNow_btn.setVisibility(View.GONE);
                book_now_info.setVisibility(View.VISIBLE);
                info_booking.setVisibility(View.GONE);

            }
        });

        service_layout = (LinearLayout) findViewById(R.id.services_layout);
        service_layout.setVisibility(View.GONE);
        bookNow_btn.setVisibility(View.GONE);
        book_now_info = (LinearLayout) findViewById(R.id.book_now_info);

        tmsView.setOnClickListener(this);
        bmsView.setOnClickListener(this);
        smsView.setOnClickListener(this);
        morgueView.setOnClickListener(this);
        eventView.setOnClickListener(this);
        bookNow_btn.setOnClickListener(this);

        future_booking_checkbox.setOnClickListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_menu, menu);
        menu.findItem(R.id.action_emergency).setTitle(Html.fromHtml("<font color='#fff'>Emergency</font>"));
        menu.findItem(R.id.action_emergency).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.book_now_btn){
            clickedBookNow();
        }

        if(v.getId() == R.id.tms_image_view || v.getId() == R.id.bms_image_view || v.getId() == R.id.sms_image_view || v.getId() == R.id.morgue_image_view || v.getId() == R.id.event_image_view){
            service_layout.setVisibility(View.VISIBLE);
            book_now_info.setVisibility(View.GONE);
            bookNow_btn.setVisibility(View.VISIBLE);
            info_booking.setVisibility(View.VISIBLE);
            clearAllSelection();
        }

        if(fare_flag) {
            switch (v.getId()) {
                case R.id.tms_image_view:
                    tmsView.setBackgroundResource(R.drawable.layout_border);
                    vehicle_selected = "TLS";
                    getBasic((float)final_distance, duration, "TLS");
                    calculateFare();
                    break;
                case R.id.bms_image_view:
                    bmsView.setBackgroundResource(R.drawable.layout_border);
                    vehicle_selected = "BLS";
                    getBasic((float)final_distance, duration, "BLS");
                    calculateFare();
                    break;
                case R.id.sms_image_view:
                    smsView.setBackgroundResource(R.drawable.layout_border);
                    vehicle_selected = "ALS";
                    getBasic((float)final_distance, duration, "ALS");
                    calculateFare();
                    break;
                case R.id.morgue_image_view:
                    morgueView.setBackgroundResource(R.drawable.layout_border);
                    vehicle_selected = "MORGUE";
                    calculateFare();
                    break;
                case R.id.event_image_view:
                    eventView.setBackgroundResource(R.drawable.layout_border);
                    vehicle_selected = "EVENT";
                    calculateFare();
                    break;
                case R.id.datebtn:
                    showDialog(999);
                    break;
                case R.id.timebtn:
                    showDialog(998);
                    break;
                case R.id.future_booking_checkbox:
                    if (future_booking_checkbox.isChecked()) {
                        date_time_layout.setVisibility(View.VISIBLE);
                    } else {
                        date_time_layout.setVisibility(View.GONE);
                    }
                default:
                    break;
            }

        } else {
            ac.showOkDialog("There was some error.", "Please try again later!");
            startActivity(new Intent(Booking1.this, Home.class));
        }
    }

    private void clickedBookNow() {

        /*int radioButtonID = payment_menu.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton)payment_menu.findViewById(radioButtonID);
        int selectedIndex = payment_menu.indexOfChild(radioButton);

        String payment_option = "";
        if(selectedIndex==0)  payment_option = "ONLINE";
        else if(selectedIndex==1) payment_option = "CASH";*/

        String services = "";
        String[] total_services = {"VENTILATOR", "STRETCHER", "PARAMEDIC", "OXYGEN", "WHEELCHAIR", "HELPER", "DOCTOR"};

        if(services_selected != null){
            for(int i=0;i<7;i++){
                if(services_selected[i]){
                    services += total_services[i] + ", ";
                }
            }

            if(services.length() > 0){
                services = services.substring(0, services.length() - 2);
                services = services.trim();
            }

        }


        //Log.i("FINAL STATUS - ", "COST = " + total_fare + ", SERVICES = " + services + ", PAYMENT = " + payment_option);

        Bundle bundle = new Bundle();
        bundle.putDouble("TOTAL_FARE", total_fare);
        bundle.putString("SERVICES", services);
        //bundle.putString("PAYMENT", payment_option);
        bundle.putString("VEHICLE_TYPE", vehicle_selected);
        bundle.putDouble("DISTANCE", (final_distance));
        bundle.putString("FROM_ADDRESS", from_address);
        bundle.putString("TO_ADDRESS", to_address);
        bundle.putString("FROM_LAT_LNG", from_lat_lng);
        bundle.putString("TO_LAT_LNG", to_lat_lng);

        startActivity(new Intent(Booking1.this, DriverDetails.class).putExtras(bundle));

    }


    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        if(id == 998){
            return new TimePickerDialog(this,myTimeListener, mHour, mMinute, false);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            String month = "";
            switch (arg2){
                case 1 : month = "Jan"; break;
                case 2 : month = "Feb"; break;
                case 3 : month = "Mar"; break;
                case 4 : month = "Apr"; break;
                case 5 : month = "May"; break;
                case 6 : month = "Jun"; break;
                case 7 : month = "July"; break;
                case 8 : month = "Aug"; break;
                case 9 : month = "Sept"; break;
                case 10 : month = "Oct"; break;
                case 11 : month = "Nov"; break;
                case 12 : month = "Dec"; break;
            }
            String date = month+" "+arg3+", "+arg1;
            datebtn.setText(date);
        }
    };

    private TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener(){
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if(hourOfDay < 12){
                am_pm = "AM";
            } else {
                am_pm = "PM";
                hourOfDay = (hourOfDay == 12) ? hourOfDay : (hourOfDay - 12);
            }
            String time = hourOfDay+":"+minute+" "+am_pm;
            timebtn.setText(time);
        }
    };

    private void clearAllSelection() {
        tmsView.setBackground(null);
        bmsView.setBackground(null);
        smsView.setBackground(null);
        morgueView.setBackground(null);
        eventView.setBackground(null);
    }

    private void calculateFare(){

        total_fare = Float.parseFloat(String.format("%.2f", total_fare));
        booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));

        unCheckAll();

        select_service_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_1.isChecked()){

                    Log.i("total fare - ",total_fare+"");
                    services_selected[0] = true;
                } else {

                    Log.i("total fare - ", total_fare+"");
                    services_selected[0] = false;
                }
            }
        });

        select_service_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_2.isChecked()){

                    Log.i("total fare - ",total_fare+"");
                    services_selected[1] = true;
                } else {

                    Log.i("total fare - ", total_fare+"");
                    services_selected[1] = false;
                }
            }
        });

        select_service_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_3.isChecked()){
                    total_fare += Float.parseFloat(db.getServiceFare("PARAMEDIC"));
                    Log.i("total fare - ",total_fare+"");
                    services_selected[2] = true;
                    booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));
                } else {
                    total_fare -= Float.parseFloat(db.getServiceFare("PARAMEDIC"));;
                    Log.i("total fare - ", total_fare+"");
                    services_selected[2] = false;
                    booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));
                }
            }
        });

        select_service_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_4.isChecked()){
                    if(final_distance <= 40)
                        total_fare += Float.parseFloat(db.getServiceFare("OXYGEN_40"));
                    else
                        total_fare += Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));
                    Log.i("total fare - ",total_fare+"");
                    services_selected[3] = true;
                    booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));
                } else {
                    if(final_distance < 40)
                        total_fare -= Float.parseFloat(db.getServiceFare("OXYGEN_40"));
                    else
                        total_fare -= Float.parseFloat(db.getServiceFare("OXYGEN_40_PLUS"));;
                    Log.i("total fare - ", total_fare+"");
                    services_selected[3] = false;
                    booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));
                }
            }
        });

        select_service_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_5.isChecked()){
                    total_fare +=  Float.parseFloat(db.getServiceFare("WHEELCHAIR"));;
                    Log.i("total fare - ",total_fare+"");
                    services_selected[4] = true;
                    booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));
                } else {
                    total_fare -=  Float.parseFloat(db.getServiceFare("WHEELCHAIR"));;
                    Log.i("total fare - ", total_fare+"");
                    services_selected[4] = false;
                    booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));
                }
            }
        });

        select_service_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_6.isChecked()){
                    total_fare +=  Float.parseFloat(db.getServiceFare("HELPER"));;
                    Log.i("total fare - ",total_fare+"");
                    services_selected[5] = true;
                    booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));
                } else {
                    total_fare -=  Float.parseFloat(db.getServiceFare("HELPER"));;
                    Log.i("total fare - ", total_fare+"");
                    services_selected[5] = false;
                    booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));
                }
            }
        });

        select_service_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(select_service_7.isChecked()){
                    total_fare +=  Float.parseFloat(db.getServiceFare("DOCTOR"));;
                    Log.i("total fare - ",total_fare+"");
                    services_selected[6] = true;
                    booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));
                } else {
                    total_fare -=  Float.parseFloat(db.getServiceFare("DOCTOR"));;
                    Log.i("total fare - ", total_fare+"");
                    services_selected[6] = false;
                    booking_fare.setText("₹" + (total_fare-17.35) + " ~ " + "₹" +  (total_fare+21.78));
                }
            }
        });
    }

    public void unCheckAll(){
        select_service_1.setChecked(false);
        select_service_2.setChecked(false);
        select_service_4.setChecked(false);
        select_service_3.setChecked(false);
        select_service_5.setChecked(false);
        select_service_6.setChecked(false);
        select_service_7.setChecked(false);
    }

    public void showToast(String query){
        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
    }
}
