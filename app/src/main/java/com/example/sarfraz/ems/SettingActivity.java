package com.example.sarfraz.ems;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener{
    RelativeLayout abtusRltv, rateUsRltv, shareRltv, contactRltv, feedbackRltv, termsRltv, privacyRltv, helpRltv;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        init();
    }

    public void init(){
        abtusRltv = (RelativeLayout) findViewById(R.id.aboutRltv);
        rateUsRltv = (RelativeLayout) findViewById(R.id.rateusRltv);
        shareRltv = (RelativeLayout) findViewById(R.id.shareRltv);
        contactRltv = (RelativeLayout) findViewById(R.id.contactRltv);
        feedbackRltv = (RelativeLayout) findViewById(R.id.feedRltv);
        termsRltv = (RelativeLayout) findViewById(R.id.termsRltv);
        privacyRltv = (RelativeLayout) findViewById(R.id.privacyRltv);
        helpRltv = (RelativeLayout) findViewById(R.id.helpRltv);

        abtusRltv.setOnClickListener(this);
        rateUsRltv.setOnClickListener(this);
        shareRltv.setOnClickListener(this);
        contactRltv.setOnClickListener(this);
        feedbackRltv.setOnClickListener(this);
        termsRltv.setOnClickListener(this);
        privacyRltv.setOnClickListener(this);
        helpRltv.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.aboutRltv:
                startActivity(new Intent(SettingActivity.this, Web.class).putExtra("url",ConstantsLocal.ABOUT_US_LINK));
                break;
            case R.id.rateusRltv :

                break;
            case R.id.shareRltv:

                break;
            case R.id.contactRltv:
                startActivity(new Intent(SettingActivity.this, ContactUs.class));
                break;
            case R.id.feedRltv:
                String email = "support@emsplus.co.in";
                String subject = "EMS+ Android App Feedback";

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] {email});
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                intent.setType("message/rfc822");
                Intent mailer = Intent.createChooser(intent, null);
                startActivity(mailer);
                break;
            case R.id.termsRltv:
                startActivity(new Intent(SettingActivity.this, Web.class).putExtra("url",ConstantsLocal.TERMS_AND_COND_LINK));
                break;
            case R.id.privacyRltv:
                startActivity(new Intent(SettingActivity.this, Web.class).putExtra("url",ConstantsLocal.COPYRIGHT_POLICY_LINK));
                break;
            case R.id.helpRltv:
                startActivity(new Intent(SettingActivity.this, Web.class).putExtra("url",ConstantsLocal.HELP_LINK));
                break;
            default:
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed(){
        finish();
    }
}
