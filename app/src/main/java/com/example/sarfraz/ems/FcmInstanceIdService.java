package com.example.sarfraz.ems;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by sarfraz on 2/8/17.
 */

public class FcmInstanceIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {

        String recent_token = FirebaseInstanceId.getInstance().getToken();
        Log.d("Token Refreshed - - -  ",recent_token);
//      Toast.makeText(getApplicationContext(), "refreshed "+recent_token, Toast.LENGTH_SHORT).show();

        SharedPreferences sp = getApplicationContext().getSharedPreferences("NOTIF", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("FCM_TOKEN",recent_token);
        editor.commit();
    }
}