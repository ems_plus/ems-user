package com.example.sarfraz.ems;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private Button loginBtn;
    private ImageView fb_btn;
    private ImageView g_btn;
    private EditText mobile;
    private EditText pass;
    private TextView signup;
    private TextView forgot_password;
    private TextView dont;
    private TextView or_login;
    public  String server_response;
    private OkHttpClient client;
    private GoogleApiClient googleApiClient;
    private ProgressClass pc;
    private AlertClass ac;
    String name, email, img_url;
    Typeface raleway;


    private static final int GOOGLE_REQ_CODE = 2002;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        init();

    }

    private void init() {
        client = new OkHttpClient();
        pc = new ProgressClass(this);
        ac = new AlertClass(this);

        if(!MyFunctions.isInternetAvailable(this)){
            ac.showOkDialog("Please Connect to Internet !!","");
        }

        loginBtn = (Button) findViewById(R.id.login);
        fb_btn = (ImageView) findViewById(R.id.fb_image);
        g_btn = (ImageView)findViewById(R.id.g_image);
        mobile = (EditText) findViewById(R.id.user_mobile);
        pass = (EditText)findViewById(R.id.user_password);
        signup = (TextView) findViewById(R.id.signup);
        dont = (TextView) findViewById(R.id.dont);
        forgot_password = (TextView) findViewById(R.id.forgot_password);
        or_login = (TextView) findViewById(R.id.or_login);
        raleway = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");

        //MediaPlayer mp = MediaPlayer.create(this, R.raw.soho);

        //FONTS

        dont.setTypeface(raleway);
        signup.setTypeface(raleway);
        mobile.setTypeface(raleway);
        pass.setTypeface(raleway);
        forgot_password.setTypeface(raleway);
        or_login.setTypeface(raleway);


        loginBtn.setTransformationMethod(null);
        //fb_btn.setTransformationMethod(null);
        //g_btn.setTransformationMethod(null);

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(LoginActivity.this).enableAutoManage(LoginActivity.this, LoginActivity.this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!MyFunctions.isInternetAvailable(LoginActivity.this)){
                    ac.showOkDialog("Please Connect to Internet...","Login Failed");
                    return;
                }

                pc = new ProgressClass(LoginActivity.this);
                pc.startProgress("Logging in...");
                //Intent intent = new Intent(LoginActivity.this, user_register.class);
                //startActivity(intent);
                String mobile_string = mobile.getText().toString();
                String pass_string = pass.getText().toString();

                //Log.i("log : ", mobile_string + " " + pass_string);

                checkLogin(mobile_string, pass_string, ConstantsLocal.LOGIN_URL);




            }
        });

        g_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!MyFunctions.isInternetAvailable(LoginActivity.this)){
                    ac.showOkDialog("Make Sure You have an active Internet Connection...","");
                    return;
                }
                signInWithGoogle();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!MyFunctions.isInternetAvailable(LoginActivity.this)){
                    ac.showOkDialog("Make Sure You have an active Internet Connection...","");
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString("PURPOSE", "SIGN_UP");
                Intent intent = new Intent(LoginActivity.this, PhoneNumber.class);
                startActivity(intent.putExtras(bundle));
            }
        });

        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!MyFunctions.isInternetAvailable(LoginActivity.this)){
                    ac.showOkDialog("Please Connect to Internet...","Login Failed");
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString("PURPOSE", "FORGOT_PASSWORD");
                Intent intent = new Intent(LoginActivity.this, PhoneNumber.class);
                startActivity(intent.putExtras(bundle));
                //startActivity(new Intent(LoginActivity.this, GetRemarks.class));
            }
        });

    }

    private void signInWithGoogle() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, GOOGLE_REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GOOGLE_REQ_CODE){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            handleResult(result);
        }
    }

    private void handleResult(GoogleSignInResult result) {
        if(result.isSuccess()){
            GoogleSignInAccount account = result.getSignInAccount();
            name = account.getDisplayName();
            email = account.getEmail();
            img_url = "";
            if(account.getPhotoUrl() != null){
                img_url = account.getPhotoUrl().toString();
            }
            Log.i("res ",name+" "+email+" "+img_url);
            //MyFunctions.setLoginType(LoginActivity.this, ConstantsLocal.LOGIN_TYPE_GOOGLE);
            // check data on server -> go to update or home

            final Map<String, String> param = new HashMap<String, String>();
            param.put("email", email);
            param.put("img_url", img_url);

            new AsyncTask<String, String, String>() {

                String response;

                @Override
                protected String doInBackground(String... params) {
                    try {
                        response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return response;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    Log.i("rspo: ",s);
                    handleSocialLoginResponse(s);
                }
            }.execute(ConstantsLocal.SOCIAL_LOGIN_URL);


        }
    }

    private void handleSocialLoginResponse(String s) {
        try {
            JSONObject obj = new JSONArray(s).getJSONObject(0);
            if(obj.getString("status").trim().equals("SUCCESS")){
                if(obj.getString("is_exist").trim().equals("true")){
                    String id = obj.getString("id");
                    String name = obj.getString("name");
                    String phone = obj.getString("phone");
                    String email = obj.getString("email");
                    String img  = obj.getString("profile_picture");
                    String gender = obj.getString("gender");
                    MyFunctions.setLoginType(this, ConstantsLocal.LOGIN_TYPE_GOOGLE);
                    MyFunctions.saveDataToPreference(this, id, name, phone, email, img, gender);
                    MyFunctions.showToast(this,"Successfully Logged in..");
                    launchHomeScreen();
                }else{
                    // not exist, new user, so go to update page from here with email and pic
                    signOutFromGoogle();
                    Intent intent = new Intent(this, PhoneNumber.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("name",name);
                    bundle.putString("email",email);
                    bundle.putString("img_url",img_url);
                    bundle.putString("PURPOSE","Google");
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }else {
                signOutFromGoogle();
                ac.showOkDialog("Some Error Occurred...","Registration failed");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void signOutFromGoogle() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {

            }
        });
    }

    private void launchHomeScreen(){
        Intent intent = new Intent(this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }



    private void checkLogin(String mobile_string, String pass_string, String url) {

        final Map<String, String> param = new HashMap<String, String>();
        param.put("phone", mobile_string);
        param.put("password", pass_string);

        new AsyncTask<String, String, String>(){

            String response;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(
                            client, params[0],
                            RequestServer.LoginBody(param));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return  response;
            }

            @Override
            protected void onPostExecute(String params) {
                super.onPostExecute(params);
                String response = params;
                if(response== null){
                    if(pc.isActive())
                        pc.stopProgress();
                    ac.showOkDialog("Something went wrong","");
                    return;
                }

                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(response);

                    JSONObject obj = jsonArray.getJSONObject(0);

                    String status = obj.get("status").toString();
                    Log.i("Status ",status);

                    if(status.equals("SUCCESS")){
                        String id = obj.getString("id");
                        String name = obj.getString("name");
                        String phone = obj.getString("phone");
                        String email = obj.getString("email");
                        String pic = obj.getString("profile_picture");
                        String gender = obj.getString("gender");

                        MyFunctions.saveDataToPreference(LoginActivity.this, id, name,phone,email,pic, gender);
                        launchHomeScreen();

                    }else{

                        String errorType = obj.getString("error");
                        Log.i("ErrorType ",errorType);
                        if(errorType.equals("PHONE_ERROR")){
                            ac.showOkDialog("Phone number is invalid","");
                        } else if(errorType.equals("PASSWORD_ERROR")){
                            ac.showOkDialog("Password is wrong...","");
                        } else if(errorType.equals("MYSQLI_ERROR")){
                            ac.showOkDialog("Some Error Occured","");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(pc.isActive())
                    pc.stopProgress();
            }
        }.execute(url);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
