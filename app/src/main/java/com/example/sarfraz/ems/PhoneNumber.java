package com.example.sarfraz.ems;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;

public class PhoneNumber extends AppCompatActivity {

    private ImageView back_phone_number;
    private ImageView next_phone_number;
    private EditText phone_number;
    private TextView phone_number_text, below_phone_number_text;
    private Typeface raleway;
    private OkHttpClient client;
    private ProgressClass pc;
    private AlertClass ac;
    String purpose;
    String name,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number);

        Bundle bundle = getIntent().getExtras();
        purpose = bundle.getString("PURPOSE");
        if(purpose.equalsIgnoreCase("Google")){
            name = bundle.getString("name");
            email = bundle.getString("email");
        }


        init();
    }

    private void init() {

        back_phone_number = (ImageView) findViewById(R.id.back_phone_number);
        next_phone_number = (ImageView) findViewById(R.id.next_phone_number);
        phone_number = (EditText) findViewById(R.id.phone_number);
        phone_number_text = (TextView) findViewById(R.id.phone_number_text);
        below_phone_number_text = (TextView) findViewById(R.id.below_phone_number_text);
        client = new OkHttpClient();
        pc = new ProgressClass(this);
        ac = new AlertClass(this);

        raleway = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        phone_number_text.setTypeface(raleway);
        below_phone_number_text.setTypeface(raleway);

        back_phone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        next_phone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String phone = phone_number.getText().toString();


                if(phone.trim().isEmpty()){
                    phone_number.setError("Phone number is required and will be verified");
                    phone_number.requestFocus();
                    return;
                }

                if(!validCellPhone(phone)){
                    phone_number.setError("Not a valid Mobile Number");
                    phone_number.requestFocus();
                    return;
                }

                if(purpose.equals("SIGN_UP") || purpose.equalsIgnoreCase("Google")){

                    pc = new ProgressClass(PhoneNumber.this);
                    pc.startProgress("Validating your phone number");

                    final Map<String, String> param = new HashMap<>();

                    param.put("phone", phone);
                    param.put("table", "users");

                    new AsyncTask<String, String, String>(){

                        String response;

                        @Override
                        protected String doInBackground(String... params) {
                            try {
                                response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                                //Log.i("response --  -- ", response);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return response;
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            //Log.i("server res ",s);

                            if(s != null) {

                                try {
                                    JSONObject obj = new JSONArray(s).getJSONObject(0);
                                    String status = obj.getString("status");
                                    if (status.equals("SUCCESS")) {
                                        if (obj.getString("flag").equals("DOES_NOT_EXIST")) {

                                            if (pc != null && pc.isActive())
                                                pc.stopProgress();

                                            pc = new ProgressClass(PhoneNumber.this);
                                            pc.startProgress("Sending OTP... !!");
                                            new SentOtp(phone).execute();


                                        } else {

                                            if (pc != null && pc.isActive())
                                                pc.stopProgress();
                                            ac.showOkDialog("Mobile Number already exists!", "");

                                        }
                                    } else {
                                        String error = obj.getString("error");
                                        if (pc != null && pc.isActive())
                                            pc.stopProgress();
                                        ac.showOkDialog("There was some error! ", error);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }.execute(ConstantsLocal.CHECK_PHONE_URL);

                    return;
                } else {

                    pc = new ProgressClass(PhoneNumber.this);
                    pc.startProgress("Sending OTP... !!");
                    new SentOtp(phone).execute();
                }

            }
        });

    }

    /*private boolean checkPhoneNumber(String phone) {



        return result[0];
    }*/

    public boolean validCellPhone(String number)
    {
        return android.util.Patterns.PHONE.matcher(number).matches();
    }


    public class SentOtp extends AsyncTask<String, String, String>{

        String response,phone;

        public SentOtp(String phone){

            this.phone = phone;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                response = RequestServer.GET(client, buildURL(phone));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(s != null) {


                Log.d("OTP Response - - - - -", response);

                try {
                    JSONObject obj = new JSONObject(response);
                    String status = obj.get("type").toString();
                    if (status.equals("success")) {
                        if (pc != null && pc.isActive())
                            pc.stopProgress();

                        Bundle bundle = new Bundle();
                        bundle.putString("PHONE_NUMBER", phone);
                        bundle.putString("PURPOSE", purpose);
                        if (purpose.equalsIgnoreCase("Google")) {
                            bundle.putString("name", name);
                            bundle.putString("email", email);
                        }

                        startActivity(new Intent(PhoneNumber.this, Otp.class).putExtras(bundle));
                    } else {
                        if (pc != null && pc.isActive())
                            pc.stopProgress();
                        ac.showOkDialog("Failed to send OTP", "");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public HttpUrl buildURL(String phone) {
        return new HttpUrl.Builder()
                .scheme("https") //http
                .host(ConstantsLocal.OTP_URL)
                .addPathSegment(ConstantsLocal.OTP_SEND_PATH)//adds "/pathSegment" at the end of hostname
                .addQueryParameter("authkey", ConstantsLocal.OTP_API_KEY) //add query parameters to the URL
                .addQueryParameter("mobile","91"+phone)
                .addQueryParameter("message",ConstantsLocal.OTP_DEFAULT_MSG)
                .addQueryParameter("sender",ConstantsLocal.OTP_SENDER_ID)
                .addQueryParameter("otp_expiry",ConstantsLocal.OTP_VALIDITY)
                .addQueryParameter("otp_length",ConstantsLocal.OTP_LENGTH)

                //.addEncodedQueryParameter("encodedName", "encodedValue")//add encoded query parameters to the URL
                .build();
        /**
         * The return URL:
         *  https://www.somehostname.com/pathSegment?param1=value1&encodedName=encodedValue
         */


    }

}
