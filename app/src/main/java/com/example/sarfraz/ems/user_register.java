package com.example.sarfraz.ems;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;

public class user_register extends AppCompatActivity {

    EditText full_name_et;
    EditText email_et;
    EditText password_et;
    EditText confirm_password_et;
    ImageView back_register;
    ImageView next_register;
    TextView register_text;
    Typeface raleway;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);
        init();



    }


    private void init() {

        Bundle bundle = getIntent().getExtras();


        full_name_et = (EditText) findViewById(R.id.user_full_name);
        email_et = (EditText) findViewById(R.id.user_email);

        if(bundle!=null){
            if(bundle.getString("PURPOSE").equalsIgnoreCase("Google")){
                full_name_et.setText(bundle.getString("name"));
                email_et.setText(bundle.getString("email"));
                full_name_et.setEnabled(false);
                email_et.setEnabled(false);
            }
        }

        password_et = (EditText) findViewById(R.id.user_password);
        confirm_password_et = (EditText) findViewById(R.id.user_confirm_password);
        register_text = (TextView) findViewById(R.id.register_text);

        raleway = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        register_text.setTypeface(raleway);

        back_register = (ImageView) findViewById(R.id.back_register);
        next_register = (ImageView) findViewById(R.id.next_register);

        back_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        next_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String full_name = full_name_et.getText().toString();
                String email = email_et.getText().toString();
                String password = password_et.getText().toString();
                String confirm_password = confirm_password_et.getText().toString();

                if(full_name.trim().isEmpty()){
                    full_name_et.setError("Name is required!");
                    full_name_et.requestFocus();
                    return;
                }

                if(email.trim().isEmpty()){
                    email_et.setError("Email is required!");
                    email_et.requestFocus();
                    return;
                }

                if(!isEmailValid(email)){
                    email_et.setError("Invalid email!");
                    email_et.requestFocus();
                    return;
                }


                if(password.trim().isEmpty()){
                    password_et.setError("Password is required!");
                    password_et.requestFocus();
                    return;
                }

                if(!password.trim().equals(confirm_password.trim())){
                    confirm_password_et.setError("password does not match!");
                    confirm_password_et.requestFocus();
                    return;
                }

                Bundle bundle = getIntent().getExtras();
                bundle.putString("NAME", full_name);
                bundle.putString("EMAIL", email);
                bundle.putString("PASSWORD", password);

                startActivity(new Intent(user_register.this, Gender.class).putExtras(bundle));

            }
        });
    }


    private void register() {



        /*pc.startProgress("Registering...");
        //Log.i("text - ", full_name + " " + phone + " " + email + " " + password + " " + confirm_password);

        final Map<String, String> param = new HashMap<String, String>();
        param.put("name", full_name);
        param.put("phone", phone);
        param.put("email", email);
        param.put("password", password);

        new AsyncTask<String, String, String>(){

            String response;

            @Override
            protected String doInBackground(String... params) {
                try {
                    response = RequestServer.POST(client, params[0], RequestServer.LoginBody(param));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.i("server res ",s);
                try {
                    JSONObject obj = new JSONArray(s).getJSONObject(0);
                    String status = obj.getString("status");
                    if(status.equals("SUCCESS")){
                        String id = obj.getString("id");
                        MyFunctions.saveDataToPreference(user_register.this, id, full_name, phone, email, "");
                        MyFunctions.setLoginType(user_register.this, ConstantsLocal.LOGIN_TYPE_NORMAL);
                        pc.stopProgress();
                        Toast.makeText(user_register.this, "Registered Successfully",Toast.LENGTH_SHORT);
                        Intent intent = new Intent(user_register.this, Home.class);
                        startActivity(intent);
                    }else {
                        String error = obj.getString("error");
                        if(error.equals("EXISTS")){
                            pc.stopProgress();
                            ac.showOkDialog("Mobile No. Already registered...","");
                        }else{
                            pc.stopProgress();
                            ac.showOkDialog("Some Error Occurred...","");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(ConstantsLocal.USER_REGISTER_URL);*/
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
}
