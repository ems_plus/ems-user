package com.example.sarfraz.ems;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;

public class Otp extends AppCompatActivity {

    private ImageView back_otp;
    private ImageView next_otp;
    private EditText otp;
    private TextView otp_text, below_otp_text;
    private Typeface raleway;
    private OkHttpClient client;
    private ProgressClass pc;
    private AlertClass ac;
    String phone_number, purpose;
    String name, email;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        init();
    }

    private void init() {

        back_otp = (ImageView) findViewById(R.id.back_otp);
        next_otp = (ImageView) findViewById(R.id.next_otp);
        otp = (EditText) findViewById(R.id.otp);
        otp_text = (TextView) findViewById(R.id.otp_text);
        below_otp_text = (TextView) findViewById(R.id.below_otp_text);

        raleway = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        otp_text.setTypeface(raleway);
        below_otp_text.setTypeface(raleway);

        client = new OkHttpClient();
        pc = new ProgressClass(this);
        ac = new AlertClass(this);

        bundle = getIntent().getExtras();
        phone_number = bundle.getString("PHONE_NUMBER");
        purpose = bundle.getString("PURPOSE");
        if(purpose.equalsIgnoreCase("Google")){
            name = bundle.getString("name");
            email = bundle.getString("email");
        }

        Log.i("phone number ...", phone_number);

        back_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                finish();
            }
        });

        next_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(purpose.equalsIgnoreCase("UPDATE")){

                    String id = bundle.getString("driver_id");
                    if(!MyFunctions.isInternetAvailable(Otp.this)){
                        ac.showConfirmDialog("internet");
                        return;
                    }

                    HashMap<String, String> param = new HashMap<String, String>();
                    param.put("phone", phone_number);
                    param.put("driver_id", id);

                    //ac.showConfirmDialog(param.toString());

                    new UpdateDriverPhone(param).execute();

                } else {

                    if(otp.getText().toString().trim().isEmpty()){
                        otp.setError("Please enter the OTP received..!!");
                    }else if(otp.getText().toString().trim().length() !=4 ){
                        otp.setError("OTP should be a 4-digit code");
                    }else{
                        pc.startProgress("Validating ... ");
                        new verifyOtp(phone_number,otp.getText().toString().trim()).execute();
                    }

                }



            }
        });

    }

    public class UpdateDriverPhone extends AsyncTask<String, String, String> {

        String res, phone;
        HashMap<String, String> param;
        String url = ConstantsLocal.HOME_URL+"update_user_phone_number.php";

        public UpdateDriverPhone(HashMap<String, String> param){
            this.param = param;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                res = RequestServer.POST(client, url, RequestServer.LoginBody(param));
            } catch (IOException e) {
                e.printStackTrace();
            }


            Log.d("accept response - - - ", res);

            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            //pc.stopProgress();

            if(s != null) {

                try {
                    Log.d("res", s);
                    JSONObject obj = new JSONObject(s);

                    String r = obj.getString("status");
                    if (r.equalsIgnoreCase("success")) {
                        MyFunctions.showToast(Otp.this, "Phone number updated successfully!");
                        Intent intent = new Intent(Otp.this, LoginActivity.class);
                        MyFunctions.clearPreferenceFile(Otp.this);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        MyFunctions.showToast(Otp.this, "There was some error!");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }




    public class verifyOtp extends AsyncTask<String, String, String>{

        String phone,otp,response;
        public verifyOtp(String phone, String otp){
            this.phone = phone;
            this.otp = otp;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                response = RequestServer.GET(client, buildURL(phone, otp));
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(s != null) {


                Log.d("OTP Verify - - - - - -", s);

                try {
                    JSONObject obj = new JSONObject(s);
                    String status = obj.get("type").toString();
                    if (status.equals("success")) {
                        if (pc != null && pc.isActive())
                            pc.stopProgress();
                        Bundle bundle = new Bundle();
                        bundle.putString("PHONE_NUMBER", phone_number);
                        if (purpose.equalsIgnoreCase("Google")) {
                            bundle.putString("name", name);
                            bundle.putString("email", email);
                            bundle.putString("PURPOSE", "Google");
                        } else if (purpose.equals("SIGN_UP") || purpose.equalsIgnoreCase("Google")) {
                            bundle.putString("PURPOSE", "PROFILE");
                            startActivity(new Intent(Otp.this, user_register.class).putExtras(bundle));
                        } else if (purpose.equals(("FORGOT_PASSWORD"))) {
                            startActivity(new Intent(Otp.this, ForgotPassword.class).putExtras(bundle));
                        }

                    } else {
                        if (pc != null && pc.isActive())
                            pc.stopProgress();
                        ac.showOkDialog("Failed to verify OTP", "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public HttpUrl buildURL(String phone, String otp) {
        return new HttpUrl.Builder()
                .scheme("https") //http
                .host(ConstantsLocal.OTP_URL)
                .addPathSegment(ConstantsLocal.OTP_VERIFY_PATH)//adds "/pathSegment" at the end of hostname
                .addQueryParameter("authkey", ConstantsLocal.OTP_API_KEY) //add query parameters to the URL
                .addQueryParameter("mobile","91"+phone)
                .addQueryParameter("otp",otp)

                //.addEncodedQueryParameter("encodedName", "encodedValue")//add encoded query parameters to the URL
                .build();
        /**
         * The return URL:
         *  https://www.somehostname.com/pathSegment?param1=value1&encodedName=encodedValue
         */

    }
}
