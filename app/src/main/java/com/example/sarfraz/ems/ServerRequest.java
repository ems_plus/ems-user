package com.example.sarfraz.ems;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

/**
 * Created by sarfraz on 25/3/17.
 */

public class ServerRequest {

    public  static String resp;
    public static String getData(String url, final Map<String, String> params, Context ctx){

        //final String[] resp = new String[1];
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("Server Resp : ", response);
                        //resp = response;
                        //LoginActivity.resp = response;
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error in volley ",error.toString());
            }
        }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.i("sending data ",params.toString());
                return params;
            }
        };
        MySingleton.getmInstance(ctx).addToRequestqueue(stringRequest);
        //Log.i("returning data", resp);
        return resp;
    }

}
