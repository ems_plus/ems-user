package com.example.sarfraz.ems;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;


public class TransactionHistory extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
    private Toolbar toolbar;
    private SharedPreferences sp;
    Bundle bundle;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);

        mRecyclerView = (RecyclerView) findViewById(R.id.transaction_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        TextView transaction_history_text = (TextView) findViewById(R.id.transaction_history_text);
        RelativeLayout transaction_history_layout = (RelativeLayout) findViewById(R.id.transaction_history_layout);
        transaction_history_layout.setVisibility(View.GONE);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bundle = getIntent().getExtras();
        String response = bundle.getString("RESPONSE");
        Log.d("rs --- ", response);

        final ArrayList results = new ArrayList<TransactionCardViewObject>();
        final int[] num_rows = new int[1];
        final String[] pid = new String[1];
        final String[] payment_status = new String[1];
        final String[] amount = new String[1];
        final String[] purpose = new String[1];
        final String[] time_stamp = new String[1];
        final JSONArray[] data_array = new JSONArray[1];

        try {
            JSONObject parentObject = new JSONObject(response);
            JSONObject obj = parentObject.getJSONObject("data");

            String status = obj.get("status").toString();

            if(status.equals("success")){


                num_rows[0] = Integer.parseInt(obj.get("num_rows").toString());
                if(num_rows[0] > 0){
                    data_array[0] = obj.getJSONArray("data_array");
                    for(int i=0; i<data_array[0].length(); i++){
                        //showToast(data_array[0].getJSONObject(i).toString());
                        JSONObject newParentObject = new JSONObject(data_array[0].getJSONObject(i).toString());
                        pid[0] = newParentObject.getString("pid").toString();
                        amount[0] = newParentObject.getString("amount").toString();
                        payment_status[0] = newParentObject.getString("status").toString();
                        purpose[0] = newParentObject.getString("vehicle_type").toString();
                        time_stamp[0] = newParentObject.getString("time_stamp").toString();

                        TransactionCardViewObject newObj =  new TransactionCardViewObject(purpose[0], "Rs. " + amount[0], payment_status[0], pid[0], time_stamp[0]);
                        results.add(i, newObj);
                    }

                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    transaction_history_layout.setVisibility(View.VISIBLE);
                    transaction_history_text.setText("No transactions found!");
                }


            } else {

                mRecyclerView.setVisibility(View.GONE);
                transaction_history_layout.setVisibility(View.VISIBLE);
                transaction_history_text.setText("There was some error. " + obj.get("error").toString());
                //showToast(obj.get("error").toString());
                return;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        mAdapter = new TransactionRecyclerViewAdapter(results);
        mRecyclerView.setAdapter(mAdapter);

        // Code to Add an item with default animation
        //((MyRecyclerViewAdapter) mAdapter).addItem(obj, index);

        // Code to remove an item with default animation
        //((MyRecyclerViewAdapter) mAdapter).deleteItem(index);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed(){
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(mAdapter != null)
            ((TransactionRecyclerViewAdapter) mAdapter).setOnItemClickListener(new TransactionRecyclerViewAdapter
                    .MyClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    //showToast(" Clicked on Item " + position);
                }
            });
    }

    public void showToast(String query){
        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
    }
}