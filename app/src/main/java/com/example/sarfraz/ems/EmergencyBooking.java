package com.example.sarfraz.ems;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class EmergencyBooking extends AppCompatActivity {

    private ImageView back_emergency_booking, next_emergency_booking;
    private RadioGroup emergency_menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_booking);

        init();
    }

    private void init() {

        back_emergency_booking = (ImageView) findViewById(R.id.back_emergency_booking);
        next_emergency_booking = (ImageView) findViewById(R.id.next_emergency_booking);

        //emergency menu

        emergency_menu = (RadioGroup) findViewById(R.id.emergenct_menu);

        RadioButton defaultCat = (RadioButton)findViewById(R.id.emergency_1);
        defaultCat.setChecked(true);

        int radioButtonID = emergency_menu.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton)emergency_menu.findViewById(radioButtonID);
        int selectedIndex = emergency_menu.indexOfChild(radioButton);

        String emergency_menu_value="NA";
        if(selectedIndex==0)  emergency_menu_value = "CARDIAC_ARREST";
        else if(selectedIndex==1) emergency_menu_value = "FIRE_EMERGENCY";
        else  if(selectedIndex==2) emergency_menu_value = "ACCIDENTAL_EMERGENCY";
        else  if(selectedIndex==3) emergency_menu_value = "OTHER";

        back_emergency_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        final String finalEmergency_menu_value = emergency_menu_value;

        next_emergency_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("emergency selected - ", finalEmergency_menu_value);

            }
        });

    }
}
