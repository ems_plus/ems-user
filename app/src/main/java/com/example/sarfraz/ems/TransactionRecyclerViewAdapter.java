package com.example.sarfraz.ems;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sarfraz.ems.TransactionCardViewObject;

import java.util.ArrayList;

public class TransactionRecyclerViewAdapter extends RecyclerView
        .Adapter<TransactionRecyclerViewAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<TransactionCardViewObject> mDataset;
    private static MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView transaction_time, transaction_vehicle, transaction_id, transaction_fare, transaction_status;
        ImageView transaction_icon, transaction_status_icon;

        public DataObjectHolder(View itemView) {
            super(itemView);
            transaction_time = (TextView) itemView.findViewById(R.id.transaction_time);
            transaction_vehicle = (TextView) itemView.findViewById(R.id.transaction_vehicle);
            transaction_id = (TextView) itemView.findViewById(R.id.transaction_id);
            transaction_fare = (TextView) itemView.findViewById(R.id.transaction_fare);
            transaction_status = (TextView) itemView.findViewById(R.id.transaction_status);
            transaction_icon = (ImageView) itemView.findViewById(R.id.transaction_icon);
            transaction_status_icon = (ImageView) itemView.findViewById(R.id.transaction_status_icon);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public TransactionRecyclerViewAdapter(ArrayList<TransactionCardViewObject> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_card_view, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.transaction_time.setText(mDataset.get(position).getTime());
        holder.transaction_vehicle.setText(mDataset.get(position).getVehicleType());
        holder.transaction_id.setText(mDataset.get(position).getOrderId());
        holder.transaction_fare.setText(mDataset.get(position).getTotalFare());
        holder.transaction_status.setText(mDataset.get(position).getStatus());
        String status = mDataset.get(position).getStatus();
        if(status.equals("unpaid") || status.equals("paid")){
            holder.transaction_icon.setImageResource(R.drawable.cash);
        } else {
            holder.transaction_icon.setImageResource(R.drawable.online);
        }

        if(status.equals("unpaid") || status.equals("failed")){
            holder.transaction_status_icon.setImageResource(R.drawable.failed);
        } else {
            holder.transaction_status_icon.setImageResource(R.drawable.checked);
        }

    }

    public void addItem(TransactionCardViewObject dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}