package com.example.sarfraz.ems;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class Profile extends AppCompatActivity {

    private TextView user_profile_name;
    private TextView user_profile_email;
    private  TextView user_profile_phone;
    private TextView toolbar_title;
    private TextView logout_text;
    private LinearLayout user_profile_logout;
    private Toolbar toolbar;
    private Typeface raleway_light, raleway_medium;
    Bundle bundle;
    RoundedImageView profile_picture;
    private ProgressDialog dialog = null;
    AlertClass ac;
    TextView edit_phone;

    static int PICK_IMAGE_REQUEST = 232;

    OkHttpClient client;

    GoogleApiClient googleApiClient;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        init();
    }



    private void init() {

        client = new OkHttpClient();
        ac = new AlertClass(this);

        raleway_light = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        raleway_medium = Typeface.createFromAsset(getAssets(), "Raleway-Medium.ttf");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bundle = getIntent().getExtras();

        user_profile_email = (TextView) findViewById(R.id.user_profile_email);
        user_profile_name = (TextView) findViewById(R.id.user_profile_name);
        user_profile_phone = (TextView) findViewById(R.id.user_profile_phone);
        user_profile_logout =  (LinearLayout) findViewById(R.id.user_profile_logout);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        logout_text = (TextView) findViewById(R.id.logout_text);
        profile_picture = (RoundedImageView) findViewById(R.id.profile_picture);

        //fonts

        user_profile_email.setTypeface(raleway_light);
        user_profile_name.setTypeface(raleway_medium);
        user_profile_phone.setTypeface(raleway_light);
        toolbar_title.setTypeface(raleway_medium);
        logout_text.setTypeface(raleway_light);


        edit_phone = (TextView) findViewById(R.id.edit_phone);


        sp = getSharedPreferences(ConstantsLocal.SHARED_PREFERENCE_NAME, MODE_PRIVATE);

        user_profile_name.setText(sp.getString(ConstantsLocal.USER_NAME, ""));
        user_profile_phone.setText(sp.getString(ConstantsLocal.USER_PHONE, ""));
        user_profile_email.setText(sp.getString(ConstantsLocal.USER_EMAIL, ""));

        String pic = MyFunctions.getUserPic(this);
        //ac.showOkDialog(pic,"");
        Log.d("user pic - - -",pic);
        if(pic.isEmpty() || pic.equalsIgnoreCase("path") || pic.equalsIgnoreCase("null")){
            if(sp.getString(ConstantsLocal.USER_GENDER,"").equals("MALE")){
                profile_picture.setImageResource(R.drawable.man);
            }else{
                profile_picture.setImageResource(R.drawable.woman);
            }
        }else{
            profile_picture.setImageBitmap(MyFunctions.Decodebitmap(MyFunctions.getUserPic(this)));
        }



        profile_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    showFileChooser();
            }
        });

        user_profile_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Profile.this);
                alertDialogBuilder.setMessage("Do you really want to logout?");
                alertDialogBuilder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                logOut();
                            }
                        });

                alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        edit_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(bundle == null)
                    bundle = new Bundle();

                bundle.putString("driver_id", sp.getString(ConstantsLocal.USER_ID, ""));
                startActivity(new Intent(Profile.this, UpdatePhoneNumber.class).putExtras(bundle));
            }
        });

    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    public String getStringImage(Bitmap bmp){

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 90, stream); //compress to which format you want.
        byte[] byte_arr = stream.toByteArray();
        String imageStr = Base64.encodeToString(byte_arr, Base64.DEFAULT);
        return imageStr;

    }

    public class UploadImage extends AsyncTask<String, String, String>{

        String bm,user_id,resp;
        Bitmap bitmap;
        public UploadImage(Bitmap bitmap){
            this.bm = getStringImage(bitmap);
            this.user_id = MyFunctions.getUserId(Profile.this);
            this.bitmap = bitmap;
        }

        @Override
        protected String doInBackground(String... params) {

            Log.d("bm -- ", bm);
            Map<String, String> map = new HashMap<String, String>();
            map.put("user_id",user_id);
            map.put("image",bm);
            map.put("user_type","user");

            try {
                resp = RequestServer.POST(client, ConstantsLocal.IMAGE_UPLOAD_URL, RequestServer.LoginBody(map));
            } catch (IOException e) {
                e.printStackTrace();
            }
           // Log.d("upload response ", resp);
            return resp;
        }

        @Override
        protected void onPostExecute(String s) {
           // Log.d("upload response ", s);
            if(dialog.isShowing()) dialog.dismiss();
            if(s != null) {

                //ac.showOkDialog(s,"");
                try {
                    JSONObject obj = new JSONObject(s);

                    if (obj.getString("status").equalsIgnoreCase("success")) {

                        profile_picture.setImageBitmap(bitmap);
                        MyFunctions.updateUserPic(Profile.this, bm);

                        MyFunctions.showToast(Profile.this, "Profile picture updated");

                    } else {
                        MyFunctions.showToast(Profile.this, "Uploading failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed(){
        finish();
    }

    private void logOut() {

        /*if(MyFunctions.getLoginType(this).equals(ConstantsLocal.LOGIN_TYPE_NORMAL)){

        }else if(MyFunctions.getLoginType(this).equals(ConstantsLocal.LOGIN_TYPE_GOOGLE)){
            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    Toast.makeText(Profile.this, "Successfully Logged Out",Toast.LENGTH_LONG).show();
                }
            });
        }else if(MyFunctions.getLoginType(this).equals(ConstantsLocal.LOGIN_TYPE_FACEBOOK)){

        }*/

        MyFunctions.clearPreferenceFile(this);

        Intent intent = new Intent(Profile.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    Bitmap bitmap;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView

                if(MyFunctions.isInternetAvailable(Profile.this)){
                    dialog = ProgressDialog.show(Profile.this, "", "Uploading profile picture...", true);

                    new UploadImage(bitmap).execute();
                }else{
                    ac.showOkDialog("Please connect to Internet","");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
