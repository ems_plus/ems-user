package com.example.sarfraz.ems;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class Rejected extends AppCompatActivity {

    TextView timeout_head, timeout_text;
    Typeface raleway_medium, raleway_light;
    ImageView cross_rejected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected);

        init();
    }

    private void init() {
        raleway_light = Typeface.createFromAsset(getAssets(), "Raleway-Light.ttf");
        raleway_medium = Typeface.createFromAsset(getAssets(), "Raleway-Medium.ttf");

        cross_rejected = (ImageView) findViewById(R.id.cross_rejected);

        timeout_head = (TextView) findViewById(R.id.timeout_head);
        timeout_text = (TextView) findViewById(R.id.timeout_text);

        timeout_text.setTypeface(raleway_light);
        timeout_head.setTypeface(raleway_medium);

        cross_rejected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Rejected.this, Home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

    }
}
